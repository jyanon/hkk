#include "stdafx.h"

#include<vector>
#include"Database.h"
#include"SpriteBattler.h"

const int System::LevelPassReward[System::MaxLevelCount] = { 44,45,46,47 };

Bitmap* iconSet;

Bitmap* titleBackground1;
Bitmap* titleBackground2;
Bitmap* titleBackground3;
Bitmap* titleText1;
Bitmap* titleText2; 

Bitmap* titleCommandBg;

Bitmap* menuBackground;
Bitmap* menuMapBackground;
Bitmap* menuMapLevelBackground;
Bitmap* menuItemBackground;
Bitmap* menuSkillBackground;
Bitmap* menuShopBackground;
Bitmap* menuSaveBackground;

Bitmap* menuCommandBg;
Bitmap* menuItemCommandBg;
Bitmap* menuEquipCommandBg;
Bitmap* menuSkillCommandBg;
Bitmap* menuFileCommandBg;

Bitmap* defaultFontFace;
Bitmap* defaultFontFaceSmall;
Bitmap* damageFontFace;

Bitmap* weaponBitmap;

Bitmap* hpDisplayBar;
Bitmap* hpDisplayBg;

Bitmap* hpBarBitmap;
Bitmap* mpBarBitmap;

Bitmap* battleHpBarBitmap;
Bitmap* battleMpBarBitmap;

Bitmap* battleStateUI;
Bitmap* battleSlotsUI;


int defaultFontYCount;

int AnimationData::BeingHurtAnimationId = 0;
int AnimationData::count = 0;
int Actor::count = 0;
int Enemy::count = 0;
int Item::count = 0;
int Weapon::count = 0;
int Armor::count = 0;
int Skill::count = 0;
int Battle::count = 0;

vector<AnimationData> animations;

vector<Actor> actors; 
vector<Enemy> enemies;

vector<Item> items;
vector<Weapon> weapons;
vector<Armor> armors;
vector<Skill> skills;

vector<Shop> shops;

vector<Battle> battles;

vector<OperatorData> normalTargetOper;
vector<OperatorData> criticalOper;
vector<OperatorData> missOper;

ShineData::ShineData(int _t, int _l, char _c, bool _s) :time(_t), last(_l), c(_c), screen(_s) {}
bool ShineData::operator<(const ShineData dt)const {return time < dt.time;}

BattleField::BattleField(string fn, string pfn) : fileName(fn), passableFileName(pfn) {}
BattleEnemy::BattleEnemy(int id, int count, Rect position) : enemyId(id), count(count), position(position) {}

EquipSlot::EquipSlot(System::EquipType et, Equip* eq) :eType(et), equip(eq) {}

UsingEffect::UsingEffect(Damage::DamageType type, int base,
	double rate[System::BaseParamCount],
	double targetRate[System::BaseParamCount],
	bool crit, int disp, bool hit) :
	damageType(type), baseValue(base), critable(crit), dispersed(disp), hit100(hit) {
	for (int i = 0; i < System::BaseParamCount; i++) {
		paramsRate[i] = rate[i];
		targetParamsRate[i] = targetRate[i];
	}
}
UsingEffect::UsingEffect(Damage::DamageType type, int base,
	bool crit, int disp, bool hit) :
	damageType(type), baseValue(base), critable(crit), dispersed(disp), hit100(hit) {
	for (int i = 0; i < System::BaseParamCount; i++) {
		paramsRate[i] = targetParamsRate[i] = 0;
	}
}

OperatorData::OperatorData(int t, OperatorType type) : timing(t), oType(type) {}
OperatorData::OperatorData(int t, OperatorType type, vector<int> p) : timing(t), oType(type), params(p) {}
bool OperatorData::operator<(const OperatorData od) const { return timing < od.timing; }
bool OperatorData::operator==(const OperatorData od) const { 
	return timing == od.timing && oType == od.oType && params == od.params;
}

AnimationData::AnimationData(){};
AnimationData::AnimationData(string key, string file, int fc, int xc, int yc, int spd) :
	index(AnimationData::count++), name(key), fileName(file), maxFrame(fc), xCount(xc), yCount(yc), speed(spd) {}

void AnimationData::addShineData(int time, int last, char c, bool screen) {
	addShineData(ShineData(time, last, c, screen));
}
void AnimationData::addShineData(ShineData dt) {
	shine.push_back(dt);
}

void Battler::addSkill(int id) { addSkill(&skills[id]); }
void Battler::addSkill(Skill* skill) { learnedSkills.push_back(skill); }
Battler::Battler(int id, string n, string fn) : index(id), name(n), fileName(fn) {}
Actor::Actor(string n, string fn, string bf) : Battler(Actor::count++, n, fn), bigFace(bf){
	for (int i = 0; i < System::EquipCount; i++) 
		initEquips.push_back(EquipSlot(System::EquipType(i)));
}
void Enemy::addSkill(int id, int rate) { addSkill(&skills[id], rate); }
void Enemy::addSkill(Skill* skill,int rate) {
	Battler::addSkill(skill); skillRate.push_back(rate);
}
Enemy::Enemy(string n, string fn, int gold) : Battler(Enemy::count++, n, fn), gold(gold) {}

ItemBase::ItemBase(int type,int id, string n, string fn ,int ic) :type(type), index(id),name(n), fileName(fn), iconId(ic) {}

UsableItem::UsableItem(int type, int id, string n, string fn, int ic, int cd, int dl) :
	ItemBase(type,id, n, fn, ic), coolDown(cd), delay(dl) {}
void UsableItem::addUsingEffect(Damage::DamageType type, int base,
	double rate[System::BaseParamCount],
	double targetRate[System::BaseParamCount],
	int disp, bool crit, bool hit) {
	usingEffects.push_back(UsingEffect(type, base,
		rate, targetRate, crit, disp, hit));
}
void UsableItem::addUsingEffect(Damage::DamageType type, int base,
	int disp, bool crit, bool hit) {
	usingEffects.push_back(UsingEffect(type, base, crit, disp, hit));
}

Item::Item(string n, string fn, int ic, int cd, int dl, System::ItemType type) : 
	UsableItem(0,Item::count++,n, fn, ic, cd, dl), iType(type) {}

Equip::Equip(int type, int id,string n, string fn, int ic, System::EquipType eType) :
	ItemBase(type,id,n, fn, ic), eType(eType) {
	for (int i = 0; i < System::BaseParamCount; i++) params[i] = 0;
}
Weapon::Weapon(string n, string fn, int ic, System::WeaponType type) :
	Equip(2,Weapon::count++,n, fn, ic, System::EquipType::Weapon), wType(type) {}

Armor::Armor(string n, string fn, int ic, System::EquipType etype, System::ArmorType atype) :
	Equip(3,Armor::count++, n, fn, ic, etype), aType(atype) {}

Skill::Skill(string n, string fn, int ic, int cost,int cd, int dl, System::SkillType type) :
	UsableItem(1,Skill::count++, n, fn, ic, cd, dl), mpCost(cost), sType(type) {
	targetOper = normalTargetOper;
}
OperatorData* Skill::addPreSkillOper(int timing, OperatorData::OperatorType type) {
	OperatorData oper(timing, type);
	preSkillOper.push_back(oper);
	sort(preSkillOper.begin(), preSkillOper.end());
	for (int i = 0; i < preSkillOper.size(); i++)
		if (preSkillOper[i] == oper) return &preSkillOper[i];
	return NULL;
}
OperatorData* Skill::addUsingOper(int timing, OperatorData::OperatorType type) {
	OperatorData oper(timing, type);
	usingOper.push_back(oper);
	sort(usingOper.begin(), usingOper.end());
	for (int i = 0; i < usingOper.size(); i++)
		if (usingOper[i] == oper) return &usingOper[i];
	return NULL;
}
OperatorData* Skill::addTargetOper(int timing, OperatorData::OperatorType type) {
	OperatorData oper(timing, type);
	targetOper.push_back(oper);
	OperatorData* res = &targetOper[targetOper.size() - 1];
	sort(targetOper.begin(), targetOper.end());
	for (int i = 0; i < targetOper.size(); i++)
		if (targetOper[i] == oper) return &targetOper[i];
	return NULL;
}

Shop::Shop(string name) :name(name) {}
void Shop::addSellItem(ItemBase* item) { list.push_back(item); }

void Battle::addEnemy(int id, int cnt, Rect pos) {
	battleEnemies.push_back(BattleEnemy(id,cnt,pos));
}
void Battle::addEnemy(Enemy* enemy, int cnt, Rect pos) { addEnemy(enemy->index, cnt, pos); }

Battle::Battle(string n, string bfn, string bfpn,double difficluty) :
	index(Battle::count++), name(n), battleField(BattleField(bfn, bfpn)), difficluty(difficluty){}


void initDatabase() {
	initSystem();
	initItems();
	initSkills();
	initEquips();
	initActors();
	initEnemies();
	initAnimations();
	initShops();
	initBattles();
}
void initSystem() {
	iconSet = Bitmap::getBitmap("../HKK/ASCII-IconSet.txt");

	titleBackground1 = Bitmap::getBitmap("../HKK/ASCII-TitleImg1.txt");
	titleBackground2 = Bitmap::getBitmap("../HKK/ASCII-TitleImg2.txt");
	titleBackground3 = Bitmap::getBitmap("../HKK/ASCII-TitleImg2.5.txt");
	titleText1 = Bitmap::getBitmap("../HKK/ASCII-TitleText1.txt");
	titleText2 = Bitmap::getBitmap("../HKK/ASCII-TitleText2.txt");

	titleCommandBg = Bitmap::getBitmap("../HKK/ASCII-TitleCommandBg.txt");

	menuBackground = Bitmap::getBitmap("../HKK/ASCII-MenuBackground.txt");
	menuMapBackground = Bitmap::getBitmap("../HKK/ASCII-MenuMapBg.txt");
	menuMapLevelBackground = Bitmap::getBitmap("../HKK/ASCII-MenuMapLevelBg.txt");
	menuItemBackground = Bitmap::getBitmap("../HKK/ASCII-MenuItemBg.txt");
	menuSkillBackground = Bitmap::getBitmap("../HKK/ASCII-MenuSkillBg.txt");
	menuShopBackground = Bitmap::getBitmap("../HKK/ASCII-MenuShopBg.txt");
	menuSaveBackground = Bitmap::getBitmap("../HKK/ASCII-MenuSaveBg.txt");

	menuCommandBg = Bitmap::getBitmap("../HKK/ASCII-MenuCommandBg.txt");
	menuItemCommandBg = Bitmap::getBitmap("../HKK/ASCII-ItemFrame.txt");
	menuEquipCommandBg = Bitmap::getBitmap("../HKK/ASCII-EquipFrame.txt");
	menuSkillCommandBg = Bitmap::getBitmap("../HKK/ASCII-SkillFrame.txt");
	menuFileCommandBg = Bitmap::getBitmap("../HKK/ASCII-FileFrame.txt");

	defaultFontFace = Bitmap::getBitmap("../HKK/ASCII-NormalText.txt");
	defaultFontFaceSmall = Bitmap::getBitmap("../HKK/ASCII-NormalText_small.txt");
	damageFontFace = Bitmap::getBitmap("../HKK/ASCII-DamageText.txt");

	weaponBitmap = Bitmap::getBitmap("../HKK/ASCII-Weapons1.txt");

	hpDisplayBg = Bitmap::getBitmap("../HKK/ASCII-HPBg.txt");
	hpDisplayBar = Bitmap::getBitmap("../HKK/ASCII-HPBar.txt");

	hpBarBitmap = Bitmap::getBitmap("../HKK/ASCII-ActorHPDisplay.txt");
	mpBarBitmap = Bitmap::getBitmap("../HKK/ASCII-ActorMPDisplay.txt");

	battleHpBarBitmap = Bitmap::getBitmap("../HKK/ASCII-BattleActorHPDisplay.txt");
	battleMpBarBitmap = Bitmap::getBitmap("../HKK/ASCII-BattleActorMPDisplay.txt");

	battleStateUI = Bitmap::getBitmap("../HKK/ASCII-BattleUI_top.txt");
	battleSlotsUI = Bitmap::getBitmap("../HKK/ASCII-BattleUI_bottom.txt");

	defaultFontYCount = 8;

	normalTargetOper.push_back(OperatorData(0, OperatorData::OperatorType::Freeze));
	normalTargetOper.push_back(OperatorData(0, OperatorData::OperatorType::Delay));
	normalTargetOper.push_back(OperatorData(0, OperatorData::OperatorType::ChangeMotion));
	normalTargetOper.push_back(OperatorData(0, OperatorData::OperatorType::PlayAnimation));
	normalTargetOper.push_back(OperatorData(10, OperatorData::OperatorType::Unfreeze));
	normalTargetOper[1].params.push_back(10);
	normalTargetOper[2].params.push_back(SpriteBattler::MotionType::BeingHurt);
	normalTargetOper[2].params.push_back(10);
	normalTargetOper[3].params.push_back(AnimationData::BeingHurtAnimationId);
	normalTargetOper[3].params.push_back(false);

	missOper.push_back(OperatorData(0, OperatorData::OperatorType::Delay));
	missOper.push_back(OperatorData(0, OperatorData::OperatorType::ChangeMotion));
	missOper[0].params.push_back(5);
	missOper[1].params.push_back(SpriteBattler::MotionType::Missing);
	missOper[1].params.push_back(6);

	criticalOper.push_back(OperatorData(0, OperatorData::OperatorType::Freeze));
	criticalOper.push_back(OperatorData(0, OperatorData::OperatorType::Delay));
	criticalOper.push_back(OperatorData(0, OperatorData::OperatorType::ChangeMotion));
	criticalOper.push_back(OperatorData(0, OperatorData::OperatorType::PlayAnimation));
	criticalOper.push_back(OperatorData(0, OperatorData::OperatorType::ScreenShine));
	criticalOper.push_back(OperatorData(20, OperatorData::OperatorType::Unfreeze));
	criticalOper[1].params.push_back(20);
	criticalOper[2].params.push_back(SpriteBattler::MotionType::BeingHurt);
	criticalOper[2].params.push_back(20);
	criticalOper[3].params.push_back(AnimationData::BeingHurtAnimationId);
	criticalOper[3].params.push_back(false);
	criticalOper[4].params.push_back('$');
	criticalOper[4].params.push_back(2);
}

void initActors() {
	Actor* actor;
	{
		int params[System::BaseParamCount] = { 625,100,15,8,5,6,8,10 };
		double paramsGrowth[System::BaseParamCount] = { 0.12,0.08,0.14,0.11,0.03,0.08,0.09,0.09 };
		actors.push_back(Actor("Poword", "../HKK/ASCII-Actor1_bt.txt", "../HKK/ASCII-Actor1.txt"));
		actor = &actors[actors.size() - 1];
		for (int i = 0; i < System::BaseParamCount; i++) {
			actor->params[i] = params[i];
			actor->paramsGrowth[i] = paramsGrowth[i];
		}
		actor->initEquips[0].equip = &weapons[1];
		actor->initEquips[1].equip = &armors[0];
		actor->initEquips[2].equip = &armors[1];
		actor->initEquips[3].equip = &armors[2];
		actor->initEquips[6].equip = &armors[49];
		actor->addSkill(3);
		actor->addSkill(4);
		actor->addSkill(5);
		actor->addSkill(6);
		actor->addSkill(7);
		actor->addSkill(8);
		actor->addSkill(9);
		actor->addSkill(10);
		actor->addSkill(11);
	}
	{
		int params[System::BaseParamCount] = { 520,375,6,4,18,10,6,7 };
		double paramsGrowth[System::BaseParamCount] = { 0.08,0.10,0.05,0.04,0.15,0.11,0.07,0.08 };
		actors.push_back(Actor("Mogician", "../HKK/ASCII-Actor2_bt.txt", "../HKK/ASCII-Actor2.txt"));
		actor = &actors[actors.size() - 1];
		for (int i = 0; i < System::BaseParamCount; i++) {
			actor->params[i] = params[i];
			actor->paramsGrowth[i] = paramsGrowth[i];
		}
		actor->initEquips[0].equip = &weapons[2];
		actor->initEquips[1].equip = &armors[0];
		actor->initEquips[2].equip = &armors[1];
		actor->initEquips[3].equip = &armors[2];
		actor->initEquips[6].equip = &armors[49];
		actor->addSkill(19);
		actor->addSkill(20);
		actor->addSkill(21);
		actor->addSkill(22);
		actor->addSkill(23);
		actor->addSkill(24);
		actor->addSkill(25);
		actor->addSkill(26);
		actor->addSkill(27);
		actor->addSkill(28);
	}
	{
		int params[System::BaseParamCount] = { 660,75,17,6,5,5,12,4 };
		double paramsGrowth[System::BaseParamCount] = { 0.11,0.07,0.13,0.08,0.04,0.07,0.10,0.06 };
		actors.push_back(Actor("Werof", "../HKK/ASCII-Actor3_bt.txt", "../HKK/ASCII-Actor3.txt"));
		actor = &actors[actors.size() - 1];
		for (int i = 0; i < System::BaseParamCount; i++) {
			actor->params[i] = params[i];
			actor->paramsGrowth[i] = paramsGrowth[i];
		}
		actor->initEquips[0].equip = &weapons[0];
		actor->initEquips[1].equip = &armors[0];
		actor->initEquips[2].equip = &armors[1];
		actor->initEquips[3].equip = &armors[2];
		actor->initEquips[6].equip = &armors[49];
		actor->addSkill(3);
		actor->addSkill(4);
		actor->addSkill(5);
		actor->addSkill(6);
		actor->addSkill(7);
		actor->addSkill(8);
		actor->addSkill(9);
		actor->addSkill(10);
		actor->addSkill(11);
	}
	{
		int params[System::BaseParamCount] = { 580,300,14,7,13,6,9,4 };
		double paramsGrowth[System::BaseParamCount] = { 0.10,0.10,0.10,0.07,0.10,0.08,0.08,0.05 };
		actors.push_back(Actor("Chivalry", "../HKK/ASCII-Actor4_bt.txt", "../HKK/ASCII-Actor4.txt"));
		actor = &actors[actors.size() - 1];
		for (int i = 0; i < System::BaseParamCount; i++) {
			actor->params[i] = params[i];
			actor->paramsGrowth[i] = paramsGrowth[i];
		}
		actor->initEquips[0].equip = &weapons[0];
		actor->initEquips[1].equip = &armors[0];
		actor->initEquips[2].equip = &armors[1];
		actor->initEquips[3].equip = &armors[2];
		actor->initEquips[6].equip = &armors[49];
		actor->addSkill(4);
		actor->addSkill(12);
		actor->addSkill(13);
		actor->addSkill(14);
		actor->addSkill(15);
		actor->addSkill(16);
		actor->addSkill(17);
		actor->addSkill(18);
	}
}
void initItems() {
	Item* item;
	//Сҩƿ  Ч����HP+100
	{
		items.push_back(Item("Сҩƿ", "../HKK/ASCII-ItemInfo1.txt", 212));
		item = &items[items.size() - 1];

		item->price = 35;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::HPRecover, 100, 0, false, true);
	}
	//��ҩƿ  Ч����HP+400
	{
		items.push_back(Item("��ҩƿ", "../HKK/ASCII-ItemInfo2.txt", 212));
		item = &items[items.size() - 1];

		item->price = 200;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::HPRecover, 400, 0, false, true);
	}
	//��ҩƿ  Ч����HP+1500
	{
		items.push_back(Item("��ҩƿ", "../HKK/ASCII-ItemInfo3.txt", 212));
		item = &items[items.size() - 1];

		item->price = 1000;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::HPRecover, 1500, 0, false, true);
	}
	//Сħƿ  Ч����MP+100
	{
		items.push_back(Item("Сħƿ", "../HKK/ASCII-ItemInfo4.txt", 213));
		item = &items[items.size() - 1];

		item->price = 50;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::MPRecover, 100, 0, false, true);
	}
	//��ħƿ  Ч����MP+300
	{
		items.push_back(Item("��ħƿ", "../HKK/ASCII-ItemInfo5.txt", 213));
		item = &items[items.size() - 1];

		item->price = 200;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::MPRecover, 300, 0, false, true);
	}
	//��ħƿ  Ч����MP+1000
	{
		items.push_back(Item("��ħƿ", "../HKK/ASCII-ItemInfo6.txt", 213));
		item = &items[items.size() - 1];

		item->price = 1000;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		item->addUsingEffect(Damage::DamageType::MPRecover, 1000, 0, false, true);
	}
	//ʥҩˮ  Ч����HP+50%
	{
		items.push_back(Item("ʥҩˮ", "../HKK/ASCII-ItemInfo7.txt", 192));
		item = &items[items.size() - 1];

		item->price = 700;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		double rate[System::BaseParamCount] = { 0.5,0,0,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,0,0,0 };
		item->addUsingEffect(Damage::DamageType::HPRecover, 0, rate, rate2, 0, false, true);
	}
	//��ʥҩˮ  Ч����HP+100%
	{
		items.push_back(Item("��ʥҩˮ", "../HKK/ASCII-ItemInfo8.txt", 197));
		item = &items[items.size() - 1];

		item->price = 1750;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		double rate[System::BaseParamCount] = { 1,0,0,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,0,0,0 };
		item->addUsingEffect(Damage::DamageType::HPRecover, 0, rate, rate2, 0, false, true);
	}
	//ħҩˮ  Ч����MP+50%
	{
		items.push_back(Item("ħҩˮ", "../HKK/ASCII-ItemInfo9.txt", 193));
		item = &items[items.size() - 1];

		item->price = 1000;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		double rate[System::BaseParamCount] = { 0,0.5,0,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,0,0,0 };
		item->addUsingEffect(Damage::DamageType::MPRecover, 0, rate, rate2, 0, false, true);
	}
	//��ħҩˮ  Ч����MP+100%
	{
		items.push_back(Item("��ħҩˮ", "../HKK/ASCII-ItemInfo10.txt", 211));
		item = &items[items.size() - 1];

		item->price = 2500;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		double rate[System::BaseParamCount] = { 0,1,0,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,0,0,0 };
		item->addUsingEffect(Damage::DamageType::MPRecover, 0, rate, rate2, 0, false, true);
	}
	//ʥˮ  Ч����HP/MP+100%
	{
		items.push_back(Item("ʥˮ", "../HKK/ASCII-ItemInfo11.txt", 367));
		item = &items[items.size() - 1];

		item->price = 5000;
		item->occasion = System::OccasionType::Any;
		item->target = System::TargetType::Self;
		item->usingAnimationId = 6;
		double rate1[System::BaseParamCount] = { 1,0,0,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,1,0,0,0,0,0 };
		double rate3[System::BaseParamCount] = { 0,0,0,0,0,0,0,0 };
		item->addUsingEffect(Damage::DamageType::HPRecover, 0, rate1, rate3, 0, false, true);
		item->addUsingEffect(Damage::DamageType::MPRecover, 0, rate2, rate3, 0, false, true);
	}

	// �ȼ���*  Ч��������1��
	{
		items.push_back(Item("�ȼ���*", "../HKK/ASCII-ItemInfo12.txt", 352));
		item = &items[items.size() - 1];

		item->price = 1250;
		item->occasion = System::OccasionType::Menu;
		item->target = System::TargetType::Self;
		item->iType = System::ItemType::LevelBall;
		item->minLevel = 1; item->maxLevel = 20;
	}
	// �ȼ���**  Ч��������1��
	{
		items.push_back(Item("�ȼ���**", "../HKK/ASCII-ItemInfo13.txt", 354));
		item = &items[items.size() - 1];

		item->price = 8000;
		item->occasion = System::OccasionType::Menu;
		item->target = System::TargetType::Self;
		item->iType = System::ItemType::LevelBall;
		item->minLevel = 21; item->maxLevel = 50;
	}
	// �ȼ���***  Ч��������1��
	{
		items.push_back(Item("�ȼ���***", "../HKK/ASCII-ItemInfo14.txt", 357));
		item = &items[items.size() - 1];

		item->price = 23333;
		item->occasion = System::OccasionType::Menu;
		item->target = System::TargetType::Self;
		item->iType = System::ItemType::LevelBall;
		item->minLevel = 51; item->maxLevel = 99;
	}
}
void initSkills() {
	OperatorData* operPtr;
	Skill* skill;
	{
		skills.push_back(Skill("ͽ�ֹ���", "", 0, 0));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking1);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->range.dist = 50;
	}{
		skills.push_back(Skill("��ͨ����", "", 0, 0));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 20);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->range.dist = 75;
	}{
		skills.push_back(Skill("��Ծ����", "", 0, 0, 20, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1.05,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 20);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeZ);
		operPtr->params.push_back(0);
		operPtr->params.push_back(5);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(7);
		operPtr->params.push_back(0);
		operPtr->params.push_back(32);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->range.fullAngle = true;
		skill->range.dist = 100;
	} 
	
	// ��/�����ػ�  ��ע�����Ŷ���
	{
		skills.push_back(Skill("�� / �����ػ�", "../HKK/ASCII-SkillInfo1.txt", 115, 10, 30, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1.6,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 15);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(8);
		operPtr->params.push_back(-18);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		// Other Setting
		skill->range.dist = 80;
	}
	// ��/��������ն  ��ע������ת��
	{
		skills.push_back(Skill("�� / ��������ն", "../HKK/ASCII-SkillInfo2.txt", 113, 15, 75, 20));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0.8,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 20);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(30);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(9);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::ChangeDir);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(60);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeDir);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::ChangeDir);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(80);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::ChangeDir);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(35);
		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(10);
		operPtr->params.push_back(3);
		// Other Setting		
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		skill->range.fullAngle = true;
		skill->range.zRange = 35;
		skill->range.dist = 125;
		skill->learnedLevel = 5;
	}
	// �������  ��ע����ǰ���
	{
		skills.push_back(Skill("�������", "../HKK/ASCII-SkillInfo3.txt", 119, 40, 85, 20));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 15);		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);

		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Freeze);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(12);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeSpeed);
		operPtr->params.push_back(25);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(110);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(95);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(85);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(65);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::ResetSpeed);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(1, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(2, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(3, OperatorData::OperatorType::MoveBackward);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 60;		
		skill->learnedLevel = 15;
	}
	// ����ս����  ��ע��������
	{
		skills.push_back(Skill("����ս����", "../HKK/ASCII-SkillInfo4.txt", 102, 65, 250, 30));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1.85,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.75,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 30);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);

		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Freeze);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(17);
		operPtr->params.push_back(-112);
		operPtr->params.push_back(0);
		//operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::ChangeSpeed);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::ResetSpeed);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(14, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(25, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(25, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(1, OperatorData::OperatorType::MoveBackward);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.wholeLine = true;
		skill->range.dist = 35;
		skill->learnedLevel = 25;
	}
	// ������Ѫն  ��ע����Ѫ
	{
		skills.push_back(Skill("������Ѫն", "../HKK/ASCII-SkillInfo5.txt", 2, 30, 150, 20));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1.25,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPAbsorb, 0, rate, rate2, 20);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(18);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		// Other Setting
		skill->range.ballRange = true;
		skill->range.dist = 100;
		skill->learnedLevel = 15;
	}
	// �����������  ��ע����׶
	{
		skills.push_back(Skill("�����������", "../HKK/ASCII-SkillInfo6.txt", 100, 60, 240, 30));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,3,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);		
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(150);
		operPtr->params.push_back(8);
		operPtr = skill->addPreSkillOper(6, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addPreSkillOper(8, OperatorData::OperatorType::ScreenShake);
		operPtr->params.push_back(15);
		operPtr->params.push_back(8);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(19);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-6);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::PlayAnimationAtTarget);
		operPtr->params.push_back(19);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-6);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(40);
		operPtr->params.push_back(10);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		skill->range.fullAngle = true;
		skill->range.dist = 200;
		skill->learnedLevel = 25;
	}
	// ��/��������ն  ��ע����
	{
		skills.push_back(Skill("�� / ��������ն", "../HKK/ASCII-SkillInfo7.txt", 9, 75, 225, 25)); // 75 255
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,4,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.8,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(20);
		operPtr->params.push_back(-24);
		operPtr->params.push_back(18);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(25);
		operPtr->params.push_back(8);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(40);
		operPtr->params.push_back(10);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		skill->range.dist = 80;
		skill->learnedLevel = 30;
	}
	// �������塤����  ��ע������ǰ������
	{
		skills.push_back(Skill("�������塤����", "../HKK/ASCII-SkillInfo8.txt", 8, 200, 600, 40)); // 200 600
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,4,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);		
		
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(200);
		operPtr->params.push_back(8);
		operPtr = skill->addPreSkillOper(4, OperatorData::OperatorType::TransferToTarget);
		operPtr = skill->addPreSkillOper(6, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addPreSkillOper(8, OperatorData::OperatorType::ScreenShake);
		operPtr->params.push_back(15);
		operPtr->params.push_back(8);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(21);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-6);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(30);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(21);
		operPtr->params.push_back(-18);
		operPtr->params.push_back(14);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(60);
		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(18, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(21);
		operPtr->params.push_back(7);
		operPtr->params.push_back(-25);
		operPtr = skill->addUsingOper(18, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(35);
		operPtr = skill->addUsingOper(20, OperatorData::OperatorType::AutoDir);
		operPtr = skill->addUsingOper(20, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(35);
		operPtr = skill->addUsingOper(25, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(30);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.fullAngle = true;
		skill->range.dist = 250;
		skill->learnedLevel = 50;
	}
	// �������塤��ɨǧ��  ��ע��ǰ��ȫ����������ι���
	{
		skills.push_back(Skill("�������塤��ɨǧ��", "../HKK/ASCII-SkillInfo9.txt", 14, 185, 600, 20)); // 185 600
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,3,0,0,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-1,0,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 30);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(10);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(22);
		operPtr->params.push_back(-192);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::ScreenShake);
		operPtr->params.push_back(25);
		operPtr->params.push_back(5);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('$');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(150);
		operPtr = skill->addUsingOper(14, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(17, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Axe);
		skill->range.wholeLine = true;
		skill->range.dist = 80;
		skill->learnedLevel = 50;
	}
	// ������ն��  ��ע����
	{
		skills.push_back(Skill("������ն��", "../HKK/ASCII-SkillInfo10.txt", 131, 15, 20, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0.6,0,0.5,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.5,0,-0.5,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(23);
		operPtr->params.push_back(-18);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 80;
		skill->learnedLevel = 5;
	}
	// ������ն��  ��ע����
	{
		skills.push_back(Skill("������ն��", "../HKK/ASCII-SkillInfo11.txt", 131, 15, 20, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0.45,0,0.65,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.5,0,-0.5,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(24);
		operPtr->params.push_back(-18);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 80;
		skill->learnedLevel = 5;
	}
	// ������ն��  ��ע����
	{
		skills.push_back(Skill("������ն��", "../HKK/ASCII-SkillInfo12.txt", 131, 15, 20, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0.5,0,0.7,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.5,0,-0.5,0,-0.5 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(5);
		operPtr->params.push_back(5);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(25);
		operPtr->params.push_back(-18);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 80;
		skill->learnedLevel = 5;
	}
	// �����׹�  ��ע����
	{
		skills.push_back(Skill("�����׹�", "../HKK/ASCII-SkillInfo13.txt", 8, 45, 30, 20));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0.85,0,1,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.4,0,-0.5,0,-0.2 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 25);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);

		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Freeze);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(13);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeSpeed);
		operPtr->params.push_back(25);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(80);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(120);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(120);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(80);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::ResetSpeed);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(1, OperatorData::OperatorType::MoveBackward);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 50;
		skill->learnedLevel = 15;
	}
	// �������Ƴ��  ��ע����̣���ι���
	{
		skills.push_back(Skill("�������Ƴ��", "../HKK/ASCII-SkillInfo15.txt", 115, 100, 175, 25));// 100 175
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,1.8,0,2,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.4,0,-0.4,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 50);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);

		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Freeze);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(26);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeSpeed);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(25);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::ResetSpeed);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::MoveForward);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::MoveBackward);
		operPtr = skill->addTargetOper(1, OperatorData::OperatorType::MoveBackward);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.dist = 50;
		skill->learnedLevel = 30;
	}
	// �������塤����  ��ע�������磬���ƶ�
	{
		skills.push_back(Skill("�������塤����", "../HKK/ASCII-SkillInfo16.txt", 101, 385, 750, 5));// 385 750
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,3,0,3,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.5,0,-0.5,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 15);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(27);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-20);
		//operPtr = skill->addUsingOper(1, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(1, OperatorData::OperatorType::ChangeSpeed);
		operPtr->params.push_back(6);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::ForceMove);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::Freeze);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(70);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(90);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(17, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(19, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(21, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(22, OperatorData::OperatorType::ResetSpeed);
		operPtr = skill->addUsingOper(23, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(90);
		operPtr = skill->addUsingOper(25, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(70);
		operPtr = skill->addUsingOper(27, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(29, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(30);
		operPtr = skill->addUsingOper(31, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		//operPtr = skill->addUsingOper(32, OperatorData::OperatorType::AutoDir);
		operPtr = skill->addUsingOper(34, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(34, OperatorData::OperatorType::ForceMove);
		operPtr->params.push_back(false);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(20);
		operPtr->params.push_back(6);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.fullAngle = true;
		skill->range.zRange = 1000;
		skill->range.dist = 75;
		skill->learnedLevel = 50;
	}
	// �������塤ħ������  ��ע������
	{
		skills.push_back(Skill("�������塤ħ������", "../HKK/ASCII-SkillInfo17.txt", 121, 425, 800, 15));//425 800
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,4,0,7.5,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,-0.4,0,-0.6,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 30);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::Shake);
		operPtr->params.push_back(10);
		operPtr->params.push_back(15);
		operPtr->params.push_back(false);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(28);
		operPtr->params.push_back(0);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('$');
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(28);
		operPtr->params.push_back(0);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(28);
		operPtr->params.push_back(0);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Attacking2);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Sword);
		skill->range.fullAngle = true;
		skill->range.zRange = 1000;
		skill->range.dist = 375;
		skill->learnedLevel = 50;
	}
	// �ȡ��㾧  ��ע����
	{
		skills.push_back(Skill("�ȡ��㾧", "../HKK/ASCII-SkillInfo18.txt", 97, 25, 15, 10));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,1.15,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(29);
		operPtr->params.push_back(-48);
		operPtr->params.push_back(0);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.dist = 75;
	}
	// �ȡ�����  ��ע��ǰ������
	{
		skills.push_back(Skill("�ȡ�����", "../HKK/ASCII-SkillInfo19.txt", 8, 28, 20, 10));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,1.35,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,-1 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 40);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(30);
		operPtr->params.push_back(-50);
		operPtr->params.push_back(-28);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.pos[0] = -50;
		skill->range.fullAngle = true;
		skill->range.dist = 60;
		skill->learnedLevel = 5;
	}
	// �ȡ�����  ��ע��ǰ������
	{
		skills.push_back(Skill("�ȡ�����", "../HKK/ASCII-SkillInfo20.txt", 97, 65, 50, 15));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,1.25,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-0.5,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 15);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(31);
		operPtr->params.push_back(-100);
		operPtr->params.push_back(4);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.pos[0] = -100;
		skill->range.fullAngle = true;
		skill->range.dist = 80;
		skill->learnedLevel = 5;
	}
	// �ȡ�����  ��ע��ʮ���˺�
	{
		skills.push_back(Skill("�ȡ�����", "../HKK/ASCII-SkillInfo21.txt", 99, 150, 200, 20));// 150 200
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,0.8,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(32);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(10);

		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('&');
		operPtr->params.push_back(1);
		operPtr = skill->addUsingOper(4, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(1);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('&');
		operPtr->params.push_back(1);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(1);
		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(60);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(40);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('&');
		operPtr->params.push_back(1);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(75);
		operPtr = skill->addUsingOper(14, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		operPtr = skill->addUsingOper(16, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.wholeScreen = true;
		skill->learnedLevel = 20;
	}
	// �ȡ�Զ��֮��  ��ע����
	{
		skills.push_back(Skill("�ȡ�Զ��֮��", "../HKK/ASCII-SkillInfo22.txt", 96, 135, 80, 35));
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,2,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 30);		
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Calling);
		operPtr->params.push_back(15);
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(10);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-12);
		operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(15);
		operPtr = skill->addUsingOper(18, OperatorData::OperatorType::PlayAnimationAtTarget);
		operPtr->params.push_back(11);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-12);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(30, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(40);
		operPtr->params.push_back(10);
		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.wholeLine = true;
		skill->range.dist = 40;
		skill->learnedLevel = 16;

	}
	// �ȡ����׵���  ��ע����ϵ����ǿ
	{
		skills.push_back(Skill("�ȡ����׵���", "../HKK/ASCII-SkillInfo23.txt", 96, 200, 350, 30));// 200 350
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,3,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 30);		
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Calling);
		operPtr->params.push_back(6);
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(33);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(6);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(34);
		operPtr->params.push_back(-200);
		operPtr->params.push_back(-15);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(35);
		operPtr->params.push_back(-275);
		operPtr->params.push_back(-50);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(10);
		operPtr = skill->addUsingOper(16, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(60);
		operPtr = skill->addUsingOper(17, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(35);
		operPtr->params.push_back(-128);
		operPtr->params.push_back(27);
		operPtr = skill->addUsingOper(17, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(10);
		operPtr = skill->addUsingOper(24, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(35);
		operPtr->params.push_back(-178);
		operPtr->params.push_back(-60);
		operPtr = skill->addUsingOper(24, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(10);
		operPtr = skill->addUsingOper(28, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(35);
		operPtr->params.push_back(-249);
		operPtr->params.push_back(37);
		operPtr = skill->addUsingOper(28, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(10);
		operPtr = skill->addUsingOper(29, OperatorData::OperatorType::AutoDir);

		operPtr = skill->addTargetOper(0, OperatorData::OperatorType::Jump);
		operPtr->params.push_back(50);
		operPtr->params.push_back(10);

		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.pos[0] = -200;
		skill->range.fullAngle = true;
		skill->range.zRange = 100;
		skill->range.dist = 200;
		skill->learnedLevel = 30;
	}
	// �ȡ��ڶ�  ��ע����������
	{
		skills.push_back(Skill("�ȡ��ڶ�", "../HKK/ASCII-SkillInfo24.txt", 120, 400, 600, 50));// 400 600
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,4,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,-1,0,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 5);
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Calling);
		operPtr->params.push_back(12);
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(36);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(6);
		operPtr = skill->addUsingOper(14, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(37);
		operPtr->params.push_back(-200);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(24, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(24, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back(' ');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(28, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(28, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back(' ');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(32, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(32, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back(' ');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(36, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(36, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back(' ');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(40, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(20);
		operPtr = skill->addUsingOper(40, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back(' ');
		operPtr->params.push_back(1);

		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.pos[0] = -200;
		skill->range.fullAngle = true;
		skill->range.dist = 200;
		skill->learnedLevel = 40;

	}
	// �ȡ����塤��֮��Ϣ  ��ע�����Ŷ�������Χ�˺�������ι���
	{
		skills.push_back(Skill("�ȡ����塤��֮��Ϣ", "../HKK/ASCII-SkillInfo25.txt", 9, 750, 1200, 5)); // 750 1000
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,1,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,-0.15 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);
		// Oper Setting
		operPtr = skill->addUsingOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(38);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-20);
		//operPtr = skill->addUsingOper(1, OperatorData::OperatorType::FixedDir);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::ForceMove);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(2, OperatorData::OperatorType::Freeze);
		operPtr = skill->addUsingOper(3, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(7, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(13, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(17, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(19, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(21, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(23, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(25, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(27, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(29, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(31, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(33, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(35, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(37, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(39, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(41, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(43, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(45, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(47, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(49, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(51, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(53, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(55, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(57, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(59, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(61, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		operPtr = skill->addUsingOper(63, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);
		//operPtr = skill->addUsingOper(32, OperatorData::OperatorType::AutoDir);
		operPtr = skill->addUsingOper(64, OperatorData::OperatorType::Unfreeze);
		operPtr = skill->addUsingOper(64, OperatorData::OperatorType::ForceMove);
		operPtr->params.push_back(false);

		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.fullAngle = true;
		skill->range.zRange = 1000;
		skill->range.dist = 80;
		skill->learnedLevel = 50;
	}
	// �ȡ����塤����  ��ע�������˺�
	{
		skills.push_back(Skill("�ȡ����塤����", "../HKK/ASCII-SkillInfo26.txt", 98, 750, 900, 15));// 750 1000
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,8.5,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,-2 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 75);
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Calling);
		operPtr->params.push_back(6);
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(39);
		operPtr->params.push_back(0);
		operPtr->params.push_back(-10);
		operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(6, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(3);
		operPtr = skill->addUsingOper(8, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(40);
		operPtr->params.push_back(-125);
		operPtr->params.push_back(-40);
		operPtr = skill->addUsingOper(9, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(2);

		operPtr = skill->addUsingOper(10, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(100);

		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.pos[0] = -125;
		skill->range.fullAngle = true;
		skill->range.dist = 100;
		skill->learnedLevel = 50;
	}
	// �ȡ����塤�⻪  ��ע��ȫ���˺�
	{
		skills.push_back(Skill("�ȡ����塤�⻪", "../HKK/ASCII-SkillInfo27.txt", 112, 750, 1000, 30));// 750 1000
		skill = &skills[skills.size() - 1];
		// Effect Setting
		double rate[System::BaseParamCount] = { 0,0,0,0,5,0,0,0 };
		double rate2[System::BaseParamCount] = { 0,0,0,0,0,-1,0,0 };
		skill->addUsingEffect(Damage::DamageType::HPDamage, 0, rate, rate2, 10);
		// Oper Setting
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Calling);
		operPtr->params.push_back(6);
		operPtr = skill->addPreSkillOper(0, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(41);
		operPtr->params.push_back(true);

		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::ChangeMotion);
		operPtr->params.push_back(SpriteBattler::MotionType::Singing);
		operPtr->params.push_back(6);
		operPtr = skill->addUsingOper(5, OperatorData::OperatorType::PlayAnimation);
		operPtr->params.push_back(42);
		operPtr->params.push_back(true);
		operPtr = skill->addUsingOper(11, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(12, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(50);

		operPtr = skill->addUsingOper(15, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(16, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(80);

		operPtr = skill->addUsingOper(19, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(20, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(30);

		operPtr = skill->addUsingOper(23, OperatorData::OperatorType::ScreenShine);
		operPtr->params.push_back('#');
		operPtr->params.push_back(2);
		operPtr = skill->addUsingOper(24, OperatorData::OperatorType::Damage);
		operPtr->params.push_back(10);

		// Other Setting
		skill->weaponRequirements.push_back(System::WeaponType::Staff);
		skill->range.wholeScreen = true;
		skill->learnedLevel = 50;
	}

}
void initEquips() {
	initWeapons(); initArmors();
}
void initWeapons() {
	Weapon* weapon;
	// �̽�
	{
		int params[System::BaseParamCount] = { 0,0,30,0,0,0,0,0 };
		weapons.push_back(Weapon("�̽�", "../HKK/ASCII-WeaponInfo1.txt", 386, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 75;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// �ָ�
	{
		int params[System::BaseParamCount] = { 0,0,36,0,0,0,-2,0 };
		weapons.push_back(Weapon("�ָ�", "../HKK/ASCII-WeaponInfo2.txt", 144, System::WeaponType::Axe));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 100;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ľ��
	{
		int params[System::BaseParamCount] = { 0,0,9,0,33,0,0,0 };
		weapons.push_back(Weapon("ľ��", "../HKK/ASCII-WeaponInfo3.txt", 152, System::WeaponType::Staff));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 90;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 0,0,55,0,0,0,1,0 };
		weapons.push_back(Weapon("����", "../HKK/ASCII-WeaponInfo4.txt", 147, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 500;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 0,0,66,1,0,0,-3,0 };
		weapons.push_back(Weapon("����", "../HKK/ASCII-WeaponInfo5.txt", 144, System::WeaponType::Axe));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 625;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// Ĥ����
	{
		int params[System::BaseParamCount] = { 0,50,15,0,62,0,0,0 };
		weapons.push_back(Weapon("Ĥ����", "../HKK/ASCII-WeaponInfo6.txt", 413, System::WeaponType::Staff));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 600;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 0,0,104,0,0,0,3,2 };
		weapons.push_back(Weapon("����", "../HKK/ASCII-WeaponInfo7.txt", 387, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 1750;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ���ܽ�
	{
		int params[System::BaseParamCount] = { 0,0,98,0,89,0,1,1 };
		weapons.push_back(Weapon("���ܽ�", "../HKK/ASCII-WeaponInfo8.txt", 394, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 1880;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��ս��
	{
		int params[System::BaseParamCount] = { 25,0,112,2,0,0,-6,0 };
		weapons.push_back(Weapon("��ս��", "../HKK/ASCII-WeaponInfo9.txt", 400, System::WeaponType::Axe));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 1955;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��ʯ����
	{
		int params[System::BaseParamCount] = { 0,100,28,0,108,2,0,0 };
		weapons.push_back(Weapon("��ʯ����", "../HKK/ASCII-WeaponInfo10.txt", 416, System::WeaponType::Staff));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 1900;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��֮��
	{
		int params[System::BaseParamCount] = { 125,0,365,2,0,0,5,10 };
		weapons.push_back(Weapon("��֮��", "../HKK/ASCII-WeaponInfo11.txt", 392, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 8000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��֮��
	{
		int params[System::BaseParamCount] = { 0,100,315,0,344,2,8,4 };
		weapons.push_back(Weapon("��֮��", "../HKK/ASCII-WeaponInfo12.txt", 397, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 8000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}	
	// ج��ս��
	{
		int params[System::BaseParamCount] = { 200,0,399,6,0,0,-15,0 };
		weapons.push_back(Weapon("ج��ս��", "../HKK/ASCII-WeaponInfo13.txt", 403, System::WeaponType::Axe));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 8200;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}	
	// ������
	{
		int params[System::BaseParamCount] = { 0,300,56,0,375,10,0,0 };
		weapons.push_back(Weapon("������", "../HKK/ASCII-WeaponInfo14.txt", 423, System::WeaponType::Staff));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 8175;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ����ն��
	{
		int params[System::BaseParamCount] = { 750,0,760,0,0,0,20,15 };
		weapons.push_back(Weapon("����ն��", "../HKK/ASCII-WeaponInfo15.txt", 393, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 50000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��������
	{ 
		int params[System::BaseParamCount] = { 500,400,625,0,747,0,15,8 };
		weapons.push_back(Weapon("��������", "../HKK/ASCII-WeaponInfo16.txt", 399, System::WeaponType::Sword));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 50000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ���ƹ�����
	{
		int params[System::BaseParamCount] = { 1000,0,825,25,0,0,-20,5 };
		weapons.push_back(Weapon("���ƹ�����", "../HKK/ASCII-WeaponInfo17.txt", 402, System::WeaponType::Axe));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 50000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
	// ��ʹħ����
	{
		int params[System::BaseParamCount] = { 0,1500,150,0,800,25,0,0 };
		weapons.push_back(Weapon("��ʹħ����", "../HKK/ASCII-WeaponInfo18.txt", 420, System::WeaponType::Staff));
		weapon = &weapons[weapons.size() - 1];
		weapon->price = 50000;
		for (int i = 0; i < System::BaseParamCount; i++)
			weapon->params[i] = params[i];
	}
}
void initArmors() {
	Armor* armor;
	// ��ñ
	{
		int params[System::BaseParamCount] = { 5,0,0,3,0,1,0,0 };
		armors.push_back(Armor("��ñ", "../HKK/ASCII-ArmorInfo1.txt", 162, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 30;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 15,0,0,8,0,4,0,0 };
		armors.push_back(Armor("����", "../HKK/ASCII-ArmorInfo2.txt", 168, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 80;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��Ь
	{
		int params[System::BaseParamCount] = { 0,0,0,2,0,2,1,0 };
		armors.push_back(Armor("��Ь", "../HKK/ASCII-ArmorInfo3.txt", 470, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 35;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}


	// ��ͭͷ��
	{
		int params[System::BaseParamCount] = { 15,0,0,11,0,4,0,0 };
		armors.push_back(Armor("��ͭͷ��", "../HKK/ASCII-ArmorInfo4.txt", 163, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 375;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ս����
	{
		int params[System::BaseParamCount] = { 35,0,0,22,0,15,0,0 };
		armors.push_back(Armor("ս����", "../HKK/ASCII-ArmorInfo5.txt", 169, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 515;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �˶�Ь
	{
		int params[System::BaseParamCount] = { 0,0,0,8,0,6,3,0 };
		armors.push_back(Armor("�˶�Ь", "../HKK/ASCII-ArmorInfo6.txt", 471, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 380;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��ñ
	{
		int params[System::BaseParamCount] = { 5,5,0,6,0,12,0,0 };
		armors.push_back(Armor("ħ��ñ", "../HKK/ASCII-ArmorInfo7.txt", 165, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 400;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��ս����
	{
		int params[System::BaseParamCount] = { 15,15,0,14,0,25,0,0 };
		armors.push_back(Armor("ħ��ս����", "../HKK/ASCII-ArmorInfo8.txt", 171, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 520;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �鶯Ь
	{
		int params[System::BaseParamCount] = { 0,5,0,5,1,10,1,0 };
		armors.push_back(Armor("�鶯Ь", "../HKK/ASCII-ArmorInfo9.txt", 472, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 400;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}

	// ʥ��ͷ��
	{
		int params[System::BaseParamCount] = { 50,0,0,45,0,22,0,0 };
		armors.push_back(Armor("ʥ��ͷ��", "../HKK/ASCII-ArmorInfo10.txt", 164, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 3800;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ʥ������
	{
		int params[System::BaseParamCount] = { 125,0,0,66,0,44,0,0 };
		armors.push_back(Armor("ʥ������", "../HKK/ASCII-ArmorInfo12.txt", 436, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 4300;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ʥ��սѥ
	{
		int params[System::BaseParamCount] = { 0,0,0,40,0,28,5,0 };
		armors.push_back(Armor("ʥ��սѥ", "../HKK/ASCII-ArmorInfo13.txt", 469, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 3700;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ����ñ
	{
		int params[System::BaseParamCount] = { 35,35,0,25,0,50,0,0 };
		armors.push_back(Armor("ħ����ñ", "../HKK/ASCII-ArmorInfo14.txt", 165, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 3970;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��ս��
	{
		int params[System::BaseParamCount] = { 50,200,0,40,0,72,0,0 };
		armors.push_back(Armor("ħ��ս��", "../HKK/ASCII-ArmorInfo15.txt", 171, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 4300;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��սѥ
	{
		int params[System::BaseParamCount] = { 0,25,0,25,2,42,2,0 };
		armors.push_back(Armor("ħ��սѥ", "../HKK/ASCII-ArmorInfo16.txt", 472, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 4000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}

	// ��������
	{
		int params[System::BaseParamCount] = { 0,0,12,0,0,0,0,0 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorBInfo1.txt", 517, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 200;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 15,0,35,5,0,0,0,0 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorBInfo2.txt", 517, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 1000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 100,0,85,20,0,0,0,5 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorBInfo3.txt", 517, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 4800;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ����׹
	{
		int params[System::BaseParamCount] = { 0,15,0,0,10,0,0,0 };
		armors.push_back(Armor("ħ����׹", "../HKK/ASCII-ArmorBInfo4.txt", 516, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 200;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ������׹
	{
		int params[System::BaseParamCount] = { 0,50,0,0,34,5,0,0 };
		armors.push_back(Armor("������׹", "../HKK/ASCII-ArmorBInfo5.txt", 516, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 1000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��ʹ��׹
	{
		int params[System::BaseParamCount] = { 0,150,0,0,100,15,0,5 };
		armors.push_back(Armor("��ʹ��׹", "../HKK/ASCII-ArmorBInfo6.txt", 516, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 5000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 200,200,75,25,75,25,0,2 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorBInfo7.txt", 518, System::EquipType::Earbob));
		armor = &armors[armors.size() - 1];
		armor->price = 6000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �ٶ�֮��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,0,5,0 };
		armors.push_back(Armor("�ٶ�֮��", "../HKK/ASCII-ArmorBInfo8.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����֮��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,0,0,5 };
		armors.push_back(Armor("����֮��", "../HKK/ASCII-ArmorBInfo9.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����֮��
	{
		int params[System::BaseParamCount] = { 0,0,10,0,0,0,0,0 };
		armors.push_back(Armor("����֮��", "../HKK/ASCII-ArmorBInfo10.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��֮��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,10,0,0,0 };
		armors.push_back(Armor("ħ��֮��", "../HKK/ASCII-ArmorBInfo11.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �ȹ�֮��
	{
		int params[System::BaseParamCount] = { 0,0,0,10,0,0,0,0 };
		armors.push_back(Armor("�ȹ�֮��", "../HKK/ASCII-ArmorBInfo12.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ֪ʶ֮��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,10,0,0 };
		armors.push_back(Armor("֪ʶ֮��", "../HKK/ASCII-ArmorBInfo13.txt", 513, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 125;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����ָ��
	{
		int params[System::BaseParamCount] = { 0,0,25,24,25,24,10,10 };
		armors.push_back(Armor("����ָ��", "../HKK/ASCII-ArmorBInfo14.txt", 512, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 1500;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����ָ��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,0,40,25 };
		armors.push_back(Armor("����ָ��", "../HKK/ASCII-ArmorBInfo15.txt", 512, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 1500;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����ָ��
	{
		int params[System::BaseParamCount] = { 0,0,0,0,66,30,0,0 };
		armors.push_back(Armor("����ָ��", "../HKK/ASCII-ArmorBInfo16.txt", 512, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 1500;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �ƻ�ָ��
	{
		int params[System::BaseParamCount] = { 0,0,66,30,0,0,0,0 };
		armors.push_back(Armor("�ƻ�ָ��", "../HKK/ASCII-ArmorBInfo17.txt", 512, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 1500;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��Ӱ
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,0,200,0 };
		armors.push_back(Armor("��Ӱ", "../HKK/ASCII-ArmorBInfo18.txt", 103, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 36000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ���
	{
		int params[System::BaseParamCount] = { 0,0,0,0,0,0,0,200 };
		armors.push_back(Armor("���", "../HKK/ASCII-ArmorBInfo19.txt", 176, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 36000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 200,0,255,75,0,0,0,0 };
		armors.push_back(Armor("����", "../HKK/ASCII-ArmorBInfo20.txt", 409, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 40000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 0,250,0,0,255,75,0,0 };
		armors.push_back(Armor("����", "../HKK/ASCII-ArmorBInfo21.txt", 143, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 40000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 4000,0,0,0,0,0,0,0 };
		armors.push_back(Armor("����", "../HKK/ASCII-ArmorBInfo22.txt", 179, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 38000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��Ȫ
	{
		int params[System::BaseParamCount] = { 0,5000,0,0,0,0,0,0 };
		armors.push_back(Armor("��Ȫ", "../HKK/ASCII-ArmorBInfo23.txt", 179, System::EquipType::Ring));
		armor = &armors[armors.size() - 1];
		armor->price = 38000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��ͷ��
	{
		int params[System::BaseParamCount] = { 250,0,0,166,0,123,0,0 };
		armors.push_back(Armor("��ͷ��", "../HKK/ASCII-ArmorInfo17.txt", 458, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 40000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��֮����
	{
		int params[System::BaseParamCount] = { 750,0,0,249,0,178,0,0 };
		armors.push_back(Armor("��֮����", "../HKK/ASCII-ArmorInfo18.txt", 437, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 55000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ����սЬ
	{
		int params[System::BaseParamCount] = { 0,0,0,159,0,145,10,0 };
		armors.push_back(Armor("����սЬ", "../HKK/ASCII-ArmorInfo19.txt", 469, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 42000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��ͷ��
	{
		int params[System::BaseParamCount] = { 50,250,0,118,0,160,0,0 };
		armors.push_back(Armor("ħ��ͷ��", "../HKK/ASCII-ArmorInfo20.txt", 459, System::EquipType::Helmet));
		armor = &armors[armors.size() - 1];
		armor->price = 40000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ֮����
	{
		int params[System::BaseParamCount] = { 200,500,0,188,0,250,0,0 };
		armors.push_back(Armor("ħ֮����", "../HKK/ASCII-ArmorInfo21.txt", 438, System::EquipType::Suit));
		armor = &armors[armors.size() - 1];
		armor->price = 55000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ħ��սЬ
	{
		int params[System::BaseParamCount] = { 0,50,0,100,5,179,4,0 };
		armors.push_back(Armor("ħ��սЬ", "../HKK/ASCII-ArmorInfo22.txt", 479, System::EquipType::Shoe));
		armor = &armors[armors.size() - 1];
		armor->price = 42000;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 100,75,30,30,30,30,20,20 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorCInfo1.txt", 520, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �м�����
	{
		int params[System::BaseParamCount] = { 800,500,175,175,175,175,125,125 };
		armors.push_back(Armor("�м�����", "../HKK/ASCII-ArmorCInfo2.txt", 520, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// �߼�����
	{
		int params[System::BaseParamCount] = { 2000,1500,666,666,666,666,500,500 };
		armors.push_back(Armor("�߼�����", "../HKK/ASCII-ArmorCInfo3.txt", 519, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 10086,6666,1024,1024,1024,1024,666,666 };
		armors.push_back(Armor("��������", "../HKK/ASCII-ArmorCInfo4.txt", 519, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// 666����
	{
		int params[System::BaseParamCount] = { 6666,6666,666,666,666,666,666,666 };
		armors.push_back(Armor("666����", "../HKK/ASCII-ArmorCInfo5.txt", 520, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// ���ֿ�
	{
		int params[System::BaseParamCount] = { 25,5,3,3,3,3,1,1 };
		armors.push_back(Armor("���ֿ�", "../HKK/ASCII-ArmorCInfo5.txt", 235, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
	// VIP��
	{
		int params[System::BaseParamCount] = { 2333,1000,200,200,200,200,150,150 };
		armors.push_back(Armor("VIP��", "../HKK/ASCII-ArmorCInfo5.txt", 228, System::EquipType::Others));
		armor = &armors[armors.size() - 1];
		armor->price = 0;
		for (int i = 0; i < System::BaseParamCount; i++)
			armor->params[i] = params[i];
	}
}
void initEnemies() {

	Enemy* enemy;
	// С��
	{
		int params[System::BaseParamCount] = { 275,100,25,12,0,7,5,7 };
		enemies.push_back(Enemy("С��", "../HKK/ASCII-Enemy1.txt",75));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 1;
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(35);
		enemy->aiParams.push_back(175);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// С��*
	{
		int params[System::BaseParamCount] = { 375,120,32,16,0,10,9,12 };
		enemies.push_back(Enemy("С��*", "../HKK/ASCII-Enemy2.txt",100));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 3;
		enemy->addSkill(3, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(40);
		enemy->aiParams.push_back(175);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ľ��
	{
		int params[System::BaseParamCount] = { 500,80,26,30,0,22,3,6 };
		enemies.push_back(Enemy("ľ��", "../HKK/ASCII-Enemy3.txt",85));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 4;
		enemy->aiType = Enemy::AIType::Random;
		enemy->addSkill(3, 2);
		enemy->aiParams.push_back(40);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// Ѳ�߱�
	{
		int params[System::BaseParamCount] = { 440,130,36,20,0,15,9,10 };
		enemies.push_back(Enemy("Ѳ�߱�", "../HKK/ASCII-Enemy4.txt",90));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 6;
		enemy->addSkill(3, 3);
		enemy->addSkill(7, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(50);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// Ѳ�߻���
	{
		int params[System::BaseParamCount] = { 500,125,40,25,0,20,11,13 };
		enemies.push_back(Enemy("Ѳ�߻���", "../HKK/ASCII-Enemy5.txt",120));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 7;
		enemy->addSkill(3, 4);
		enemy->addSkill(4, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(75);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// СħŮ
	{
		int params[System::BaseParamCount] = { 335,350,15,15,50,26,9,7 };
		enemies.push_back(Enemy("СħŮ", "../HKK/ASCII-Enemy6.txt",175));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 8;
		enemy->normalAttackRate = 1;
		enemy->addSkill(19, 8);
		enemy->addSkill(20, 3);
		enemy->addSkill(21, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(150);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// Ѳ�߻���*
	{
		int params[System::BaseParamCount] = { 505,155,46,28,0,18,10,8 };
		enemies.push_back(Enemy("Ѳ�߻���*", "../HKK/ASCII-Enemy7.txt",133));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 8;
		enemy->addSkill(3, 5);
		enemy->addSkill(4, 5);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(85);
		enemy->aiParams.push_back(400);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ˫Ŀսʿ
	{
		int params[System::BaseParamCount] = { 520,315,55,30,52,26,8,6 };
		enemies.push_back(Enemy("˫Ŀսʿ", "../HKK/ASCII-Enemy8.txt",180));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 10;
		enemy->normalAttackRate = 3;
		enemy->addSkill(12, 5);
		enemy->addSkill(13, 5);
		enemy->addSkill(14, 5);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(100);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ����С��
	{
		int params[System::BaseParamCount] = { 400,115,75,20,0,16,25,21 };
		enemies.push_back(Enemy("����С��", "../HKK/ASCII-Enemy9.txt",400));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 12;
		enemy->addSkill(3, 5);
		enemy->addSkill(4, 4);
		enemy->addSkill(5, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(120);
		enemy->aiParams.push_back(400);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ����ס��
	{
		int params[System::BaseParamCount] = { 2500,1000,200,80,175,75,26,19 };
		enemies.push_back(Enemy("����ס��", "../HKK/ASCII-Enemy10.txt",5000));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 20;
		enemy->addSkill(3, 4);
		enemy->addSkill(4, 4);
		enemy->addSkill(5, 3);
		enemy->addSkill(19, 5);
		enemy->addSkill(22, 1);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(275);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ���С��
	{
		int params[System::BaseParamCount] = { 700,200,82,39,0,35,13,9 };
		enemies.push_back(Enemy("���С��", "../HKK/ASCII-Enemy11.txt",350));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 13;
		enemy->addSkill(3, 4);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(75);
		enemy->aiParams.push_back(100);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ˫Ŀ����
	{
		int params[System::BaseParamCount] = { 666,400,85,35,90,36,11,11 };
		enemies.push_back(Enemy("˫Ŀ����", "../HKK/ASCII-Enemy12.txt",300));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 15;
		enemy->normalAttackRate = 2;
		enemy->addSkill(12, 5);
		enemy->addSkill(13, 5);
		enemy->addSkill(14, 5);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(70);
		enemy->aiParams.push_back(125);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ˫Ŀ����*
	{
		int params[System::BaseParamCount] = { 700,500,88,45,88,48,12,11 };
		enemies.push_back(Enemy("˫Ŀ����*", "../HKK/ASCII-Enemy13.txt",375));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 18;
		enemy->normalAttackRate = 2;
		enemy->addSkill(12, 5);
		enemy->addSkill(13, 5);
		enemy->addSkill(14, 5);
		enemy->addSkill(15, 4);
		enemy->addSkill(16, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(100);
		enemy->aiParams.push_back(150);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// �����ͽ
	{
		int params[System::BaseParamCount] = { 590,150,100,46,0,42,15,15 };
		enemies.push_back(Enemy("�����ͽ", "../HKK/ASCII-Enemy14.txt",500));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 20;
		enemy->addSkill(3, 5);
		enemy->addSkill(4, 5);
		enemy->addSkill(7, 4);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(85);
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ľ��
	{
		int params[System::BaseParamCount] = { 800,125,80,52,0,55,13,18 };
		enemies.push_back(Enemy("ľ��", "../HKK/ASCII-Enemy15.txt",400));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 20;
		enemy->addSkill(3, 5);
		enemy->addSkill(5, 4);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(100);
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// �������
	{
		int params[System::BaseParamCount] = { 3333,2000,275,100,295,102,25,25 };
		enemies.push_back(Enemy("�������", "../HKK/ASCII-Enemy16.txt",8000));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 30;
		enemy->addSkill(7, 4);
		enemy->addSkill(15, 4);
		enemy->addSkill(19, 5);
		enemy->addSkill(20, 4);
		enemy->addSkill(21, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(150);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ��ľʦ
	{
		int params[System::BaseParamCount] = { 1000,500,120,62,0,50,15,15 };
		enemies.push_back(Enemy("��ľʦ", "../HKK/ASCII-Enemy17.txt",385));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 21;
		enemy->addSkill(3, 4);
		enemy->addSkill(5, 5);
		enemy->addSkill(9, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(125);
		enemy->aiParams.push_back(250);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ˫ĿħŮ
	{
		int params[System::BaseParamCount] = { 1000,800,118,60,133,72,18,20 };
		enemies.push_back(Enemy("˫ĿħŮ", "../HKK/ASCII-Enemy18.txt",500));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 24;
		enemy->normalAttackRate = 1;
		enemy->addSkill(12, 5);
		enemy->addSkill(13, 5);
		enemy->addSkill(14, 5);
		enemy->addSkill(19, 5);
		enemy->addSkill(20, 5);
		enemy->addSkill(21, 5);
		enemy->addSkill(23, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(175);
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ����
	{
		int params[System::BaseParamCount] = { 750,1000,100,85,130,90,15,20 };
		enemies.push_back(Enemy("����", "../HKK/ASCII-Enemy19.txt",460));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 25;
		enemy->normalAttackRate = 2;
		enemy->addSkill(20, 5);
		enemy->addSkill(22, 3);
		enemy->addSkill(23, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(145);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ľ��*
	{
		int params[System::BaseParamCount] = { 1250,250,145,100,0,69,20,25 };
		enemies.push_back(Enemy("ľ��*", "../HKK/ASCII-Enemy20.txt",475));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 27;
		enemy->addSkill(3, 5);
		enemy->addSkill(5, 4);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(140);
		enemy->aiParams.push_back(250);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ��������
	{
		int params[System::BaseParamCount] = { 1275,300,150,105,0,66,23,24 };
		enemies.push_back(Enemy("��������", "../HKK/ASCII-Enemy21.txt",800));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 28;
		enemy->addSkill(3, 5);
		enemy->addSkill(4, 5);
		enemy->addSkill(5, 4);
		enemy->addSkill(6, 2);
		enemy->addSkill(8, 3);
		enemy->addSkill(9, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(120);
		enemy->aiParams.push_back(400);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ������
	{
		int params[System::BaseParamCount] = { 1000,750,160,92,168,90,35,40 };
		enemies.push_back(Enemy("������", "../HKK/ASCII-Enemy22.txt",600));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 30;
		enemy->addSkill(15, 4);
		enemy->addSkill(17, 1);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(150);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ������
	{
		int params[System::BaseParamCount] = { 5000,2500,666,275,300,250,30,50 };
		enemies.push_back(Enemy("������", "../HKK/ASCII-Enemy23.txt",12500));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 40;
		enemy->addSkill(4, 5);
		enemy->addSkill(5, 5);
		enemy->addSkill(6, 2);
		enemy->addSkill(7, 4);
		enemy->addSkill(8, 3);
		enemy->addSkill(9, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(175);
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ����*
	{
		int params[System::BaseParamCount] = { 1500,1000,188,125,200,120,24,32 };
		enemies.push_back(Enemy("����*", "../HKK/ASCII-Enemy24.txt",555));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 35;
		enemy->normalAttackRate = 1;
		enemy->addSkill(20, 5);
		enemy->addSkill(22, 2);
		enemy->addSkill(23, 4);
		enemy->addSkill(24, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(150);
		enemy->aiParams.push_back(350);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ���λ�����
	{
		int params[System::BaseParamCount] = { 250,499,0,999,175,5000,29,20 };
		enemies.push_back(Enemy("���λ�����", "../HKK/ASCII-Enemy25.txt",700));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 35;
		enemy->normalAttackRate = 0;
		enemy->addSkill(20, 5);
		enemy->addSkill(25, 5);
		enemy->addSkill(27, 3);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(500);
		enemy->aiParams.push_back(1);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}
	// ٳ����
	{
		int params[System::BaseParamCount] = { 7000,4000,520,315,680,360,42,30 };
		enemies.push_back(Enemy("ٳ����", "../HKK/ASCII-Enemy26.txt",20000));
		enemy = &enemies[enemies.size() - 1];
		enemy->level = 50;
		enemy->normalAttackRate = 2;
		enemy->addSkill(10, 2);
		enemy->addSkill(17, 3);
		enemy->addSkill(22, 3);
		enemy->addSkill(20, 5);
		enemy->addSkill(23, 4);
		enemy->addSkill(24, 2);
		enemy->addSkill(26, 3);
		enemy->addSkill(27, 2);
		enemy->aiType = Enemy::AIType::Closer;
		enemy->aiParams.push_back(200);
		enemy->aiParams.push_back(300);
		enemy->aiParams.push_back(2);
		for (int i = 0; i < System::BaseParamCount; i++)
			enemy->params[i] = params[i];
	}

}
void initAnimations() {
	animations.push_back(AnimationData("NormalHurt", "../HKK/ASCII-Stick.txt", 8, 5, 2, 2));
	animations.push_back(AnimationData("TitleSelectorAni", "../HKK/ASCII-TitleSelectorAni.txt", 4, 1, 4, 3));
	animations.push_back(AnimationData("MenuSelectorAni", "../HKK/ASCII-MenuSelectorAni.txt", 6, 1, 6, 2));
	animations.push_back(AnimationData("MenuItemSelectorAni", "../HKK/ASCII-ItemFrameSelectAni.txt", 4, 1, 4, 3));
	animations.push_back(AnimationData("MenuEquipSelectorAni", "../HKK/ASCII-EquipFrameSelectAni.txt", 4, 1, 4, 3));
	animations.push_back(AnimationData("MenuSkillSelectorAni", "../HKK/ASCII-SkillFrameSelectAni.txt", 4, 1, 4, 3));

	animations.push_back(AnimationData("NormalHeal", "../HKK/ASCII-Heal.txt", 12, 5, 3, 2));
	animations.push_back(AnimationData("GroundHitAni", "../HKK/ASCII-GroundHitAni.txt", 8, 4, 2, 2));
	animations.push_back(AnimationData("HeavyHitAni", "../HKK/ASCII-HeavyHit.txt", 7, 5, 2, 2));
	
	animations.push_back(AnimationData("RotateHitAni", "../HKK/ASCII-RotateHit.txt", 8, 2, 4, 2));

	animations.push_back(AnimationData("FireCall", "../HKK/ASCII-FireCall.txt", 23, 5, 5, 2));
	animations.push_back(AnimationData("FireAttack", "../HKK/ASCII-FireAttack.txt", 12, 5, 3, 2));
	animations.push_back(AnimationData("Sprint", "../HKK/ASCII-Sprint.txt", 9, 1, 9, 2));
	animations.push_back(AnimationData("ElectricSprint", "../HKK/ASCII-ElectricSprint.txt", 4, 1, 4, 3));

	animations.push_back(AnimationData("MenuMapSelectorAni", "../HKK/ASCII-MapSelectAni.txt", 2, 1, 2, 3));
	animations.push_back(AnimationData("MenuFileSelectorAni", "../HKK/ASCII-FileFrameSelectAni.txt", 4, 1, 4, 3));

	animations.push_back(AnimationData("Wind", "../HKK/ASCII-Wind.txt", 13, 2, 7, 2));
	animations.push_back(AnimationData("Ares'sBiff", "../HKK/ASCII-Ares'sBiff.txt", 16, 5, 4, 2));
	animations.push_back(AnimationData("AbsorbKiri", "../HKK/ASCII-AbsorbKiri.txt", 5, 5, 1, 2));
	animations.push_back(AnimationData("EarthRoar", "../HKK/ASCII-EarthRoar.txt", 13, 5, 3, 1));
	animations.push_back(AnimationData("UpKiri", "../HKK/ASCII-UpKiri.txt", 9, 2, 5, 2));
	animations.push_back(AnimationData("SwordRain", "../HKK/ASCII-SwordRain.txt", 8, 2, 4, 3));
	animations.push_back(AnimationData("1000Kill", "../HKK/ASCII-1000Kill.txt", 8, 4, 2, 3));
	animations.push_back(AnimationData("FireSword", "../HKK/ASCII-FireSword.txt", 14, 5, 3, 1));
	animations.push_back(AnimationData("IceSword", "../HKK/ASCII-IceSword.txt", 14, 5, 3, 1));
	animations.push_back(AnimationData("ThunderSword", "../HKK/ASCII-ThunderSword.txt", 14, 5, 3, 1));
	animations.push_back(AnimationData("FireDash", "../HKK/ASCII-FireDash.txt", 6, 3, 2, 2));
	animations.push_back(AnimationData("Wind", "../HKK/ASCII-Wind.txt", 13, 2, 7, 3));
	animations.push_back(AnimationData("MagicGas", "../HKK/ASCII-MagicGas.txt", 3, 3, 1, 3));
	animations.push_back(AnimationData("IceAttack", "../HKK/ASCII-IceAttack.txt", 15, 5, 3, 1));
	animations.push_back(AnimationData("ThunderAttack", "../HKK/ASCII-ThunderAttack.txt", 7, 4, 2, 1));
	animations.push_back(AnimationData("IceLock", "../HKK/ASCII-IceLock.txt", 12, 4, 3, 1));
	animations.push_back(AnimationData("Water", "../HKK/ASCII-Water.txt", 19, 5, 4, 1));
	animations.push_back(AnimationData("RedFirePre", "../HKK/ASCII-RedFirePre.txt", 12, 5, 3, 1));
	animations.push_back(AnimationData("RedFire1", "../HKK/ASCII-RedFire1.txt", 15, 5, 3, 2));
	animations.push_back(AnimationData("RedFire2", "../HKK/ASCII-RedFire2.txt", 11, 5, 3, 1));
	animations.push_back(AnimationData("MagicZhin", "../HKK/ASCII-MagicZhin.txt", 20, 5, 4, 1));
	animations.push_back(AnimationData("BlackHole", "../HKK/ASCII-BlackHole.txt", 29, 5, 6, 1));
	animations.push_back(AnimationData("Special", "../HKK/ASCII-SpecialSkill.txt", 35, 7, 5, 2));
	animations.push_back(AnimationData("KiRinPre", "../HKK/ASCII-KiRinPre.txt", 12, 5, 3, 1));
	animations.push_back(AnimationData("KiRin", "../HKK/ASCII-KiRin.txt", 5, 5, 1, 1));
	animations.push_back(AnimationData("PreLight", "../HKK/ASCII-PreLight.txt", 25, 5, 5, 1));
	animations.push_back(AnimationData("Light", "../HKK/ASCII-Light.txt", 19, 5, 4, 2));

	animations[0].addShineData(4, 2, ' ');

	animations[6].addShineData(3, 1, '.');
	animations[6].addShineData(4, 2, ':');
	animations[6].addShineData(6, 1, '1');
	animations[6].addShineData(7, 2, ':');
	animations[6].addShineData(9, 1, '.');

	animations[7].addShineData(1, 1, '#');//,true);

	animations[9].addShineData(2, 1, '#', true);
	animations[9].addShineData(4, 1, '#', true);
	animations[9].addShineData(6, 1, '#', true);

	animations[10].addShineData(16, 3, '#', true);

	animations[11].addShineData(6, 5, '#');

	animations[17].addShineData(6, 3, '#');

	animations[19].addShineData(3, 1, '#');
	animations[19].addShineData(5, 1, '#');

	animations[21].addShineData(3, 1, '#', true);
	animations[21].addShineData(6, 1, '#', true);

	animations[26].addShineData(0, 1, '#', true);
	animations[26].addShineData(1, 1, '#', true);

	animations[27].addShineData(7, 1, '#', true);

	animations[30].addShineData(1, 1, '#', true);
	animations[31].addShineData(3, 1, '#', true);

	animations[34].addShineData(3, 2, '#', true);
	animations[35].addShineData(1, 1, '#', true);
}
void initShops() {
	
	shops.push_back(Shop("NormalShop"));
	shops[0].addSellItem(&items[0]);
	shops[0].addSellItem(&items[3]);
	shops[0].addSellItem(&items[11]);
	shops[0].addSellItem(&weapons[0]);
	shops[0].addSellItem(&weapons[1]);
	shops[0].addSellItem(&weapons[2]);
	shops[0].addSellItem(&weapons[3]);
	shops[0].addSellItem(&weapons[4]);
	shops[0].addSellItem(&weapons[5]);
	shops[0].addSellItem(&armors[0]);
	shops[0].addSellItem(&armors[1]);
	shops[0].addSellItem(&armors[2]);
	shops[0].addSellItem(&armors[3]);
	shops[0].addSellItem(&armors[4]);
	shops[0].addSellItem(&armors[5]);
	shops[0].addSellItem(&armors[6]);
	shops[0].addSellItem(&armors[7]);
	shops[0].addSellItem(&armors[8]);
	/*
	shops[0].addSellItem(&armors[0]);
	shops[0].addSellItem(&armors[1]);
	shops[0].addSellItem(&armors[2]);
	shops[0].addSellItem(&armors[3]);
	shops[0].addSellItem(&armors[4]);
	shops[0].addSellItem(&armors[5]);
	shops[0].addSellItem(&armors[6]);
	shops[0].addSellItem(&armors[7]);
	shops[0].addSellItem(&armors[8]);
	*/

	shops.push_back(Shop("MiddleShop"));
	shops[1].addSellItem(&items[0]);
	shops[1].addSellItem(&items[1]);
	shops[1].addSellItem(&items[3]);
	shops[1].addSellItem(&items[4]);
	shops[1].addSellItem(&items[6]);
	shops[1].addSellItem(&items[8]);
	shops[1].addSellItem(&items[12]);
	shops[1].addSellItem(&weapons[6]);
	shops[1].addSellItem(&weapons[7]);
	shops[1].addSellItem(&weapons[8]);
	shops[1].addSellItem(&weapons[9]);
	shops[1].addSellItem(&armors[3]);
	shops[1].addSellItem(&armors[4]);
	shops[1].addSellItem(&armors[5]);
	shops[1].addSellItem(&armors[6]);
	shops[1].addSellItem(&armors[7]);
	shops[1].addSellItem(&armors[8]);
	shops[1].addSellItem(&armors[15]);
	shops[1].addSellItem(&armors[16]);
	shops[1].addSellItem(&armors[18]);
	shops[1].addSellItem(&armors[19]);
	shops[1].addSellItem(&armors[22]);
	shops[1].addSellItem(&armors[23]);
	shops[1].addSellItem(&armors[24]);
	shops[1].addSellItem(&armors[25]);
	shops[1].addSellItem(&armors[26]);
	shops[1].addSellItem(&armors[27]);

	shops.push_back(Shop("Middle-LastShop"));
	shops[2].addSellItem(&items[0]);
	shops[2].addSellItem(&items[1]);
	shops[2].addSellItem(&items[2]);
	shops[2].addSellItem(&items[3]);
	shops[2].addSellItem(&items[4]);
	shops[2].addSellItem(&items[5]);
	shops[2].addSellItem(&items[6]);
	shops[2].addSellItem(&items[7]);
	shops[2].addSellItem(&items[8]);
	shops[2].addSellItem(&items[9]);
	shops[2].addSellItem(&items[12]);
	shops[2].addSellItem(&items[13]);
	shops[2].addSellItem(&weapons[6]);
	shops[2].addSellItem(&weapons[7]);
	shops[2].addSellItem(&weapons[8]);
	shops[2].addSellItem(&weapons[9]);
	shops[2].addSellItem(&weapons[10]);
	shops[2].addSellItem(&weapons[11]);
	shops[2].addSellItem(&weapons[12]);
	shops[2].addSellItem(&weapons[13]);
	shops[2].addSellItem(&armors[9]);
	shops[2].addSellItem(&armors[10]);
	shops[2].addSellItem(&armors[11]);
	shops[2].addSellItem(&armors[12]);
	shops[2].addSellItem(&armors[13]);
	shops[2].addSellItem(&armors[14]);
	shops[2].addSellItem(&armors[16]);
	shops[2].addSellItem(&armors[17]);
	shops[2].addSellItem(&armors[19]);
	shops[2].addSellItem(&armors[20]);
	shops[2].addSellItem(&armors[21]);
	shops[2].addSellItem(&armors[28]);
	shops[2].addSellItem(&armors[29]);
	shops[2].addSellItem(&armors[30]);
	shops[2].addSellItem(&armors[31]);

	shops.push_back(Shop("LastShop"));
	shops[3].addSellItem(&items[0]);
	shops[3].addSellItem(&items[1]);
	shops[3].addSellItem(&items[2]);
	shops[3].addSellItem(&items[3]);
	shops[3].addSellItem(&items[4]);
	shops[3].addSellItem(&items[5]);
	shops[3].addSellItem(&items[6]);
	shops[3].addSellItem(&items[7]);
	shops[3].addSellItem(&items[8]);
	shops[3].addSellItem(&items[9]);
	shops[3].addSellItem(&items[10]);
	shops[3].addSellItem(&items[13]);
	shops[3].addSellItem(&weapons[10]);
	shops[3].addSellItem(&weapons[11]);
	shops[3].addSellItem(&weapons[12]);
	shops[3].addSellItem(&weapons[13]);
	shops[3].addSellItem(&armors[17]);
	shops[3].addSellItem(&armors[20]);
	shops[3].addSellItem(&armors[21]);
	shops[3].addSellItem(&armors[28]);
	shops[3].addSellItem(&armors[29]);
	shops[3].addSellItem(&armors[30]);
	shops[3].addSellItem(&armors[31]);

	shops.push_back(Shop("EndShop"));
	shops[4].addSellItem(&items[0]);
	shops[4].addSellItem(&items[1]);
	shops[4].addSellItem(&items[2]);
	shops[4].addSellItem(&items[3]);
	shops[4].addSellItem(&items[4]);
	shops[4].addSellItem(&items[5]);
	shops[4].addSellItem(&items[6]);
	shops[4].addSellItem(&items[7]);
	shops[4].addSellItem(&items[8]);
	shops[4].addSellItem(&items[9]);
	shops[4].addSellItem(&items[10]);
	shops[4].addSellItem(&weapons[14]);
	shops[4].addSellItem(&weapons[15]);
	shops[4].addSellItem(&weapons[16]);
	shops[4].addSellItem(&weapons[17]);
	shops[4].addSellItem(&armors[17]);
	shops[4].addSellItem(&armors[20]);
	shops[4].addSellItem(&armors[21]);
	shops[4].addSellItem(&armors[28]);
	shops[4].addSellItem(&armors[29]);
	shops[4].addSellItem(&armors[30]);
	shops[4].addSellItem(&armors[31]);

	shops.push_back(Shop("GodShop"));
	shops[5].addSellItem(&weapons[14]);
	shops[5].addSellItem(&weapons[15]);
	shops[5].addSellItem(&weapons[16]);
	shops[5].addSellItem(&weapons[17]);
	shops[5].addSellItem(&armors[32]);
	shops[5].addSellItem(&armors[33]);
	shops[5].addSellItem(&armors[34]);
	shops[5].addSellItem(&armors[35]);
	shops[5].addSellItem(&armors[36]);
	shops[5].addSellItem(&armors[37]);
	shops[5].addSellItem(&armors[38]);
	shops[5].addSellItem(&armors[39]);
	shops[5].addSellItem(&armors[40]);
	shops[5].addSellItem(&armors[41]);
	shops[5].addSellItem(&armors[42]);
	shops[5].addSellItem(&armors[43]);

	shops.push_back(Shop("TestShop"));
	for (int i = 0; i < items.size(); i++) shops[6].addSellItem(&items[i]);
	for (int i = 0; i < weapons.size(); i++) shops[6].addSellItem(&weapons[i]);
	for (int i = 0; i < armors.size(); i++) shops[6].addSellItem(&armors[i]);

}
void initBattles() {
	battles.push_back(Battle("Test", "../HKK/ASCII-Battle1_r.txt",
		"../HKK/ASCII-Battle1_passable.txt",0.8));
	battles[0].battleField.enemyCreateBlock = Rect(350, 270, 300, 100);
	battles[0].battleField.playerCreatePos[0] = 870;
	battles[0].battleField.playerCreatePos[1] = 290;
	battles[0].addEnemy(0, 6, Rect(720, 235, 96, 105));
	battles[0].addEnemy(2, 3, Rect(523, 280, 96, 40));
	battles[0].addEnemy(3, 4, Rect(500, 225, 30, 120));
	battles[0].addEnemy(7, 3, Rect(400, 230, 145, 100));
	battles[0].addEnemy(8, 1, Rect(335, 240, 70, 35));
	battles[0].addEnemy(8, 1, Rect(265, 275, 70, 35));
	battles[0].addEnemy(8, 1, Rect(335, 310, 70, 35));
	battles[0].addEnemy(8, 1, Rect(405, 275, 70, 35));
	battles[0].addEnemy(9, 1, Rect(335, 275, 70, 35));

	battles.push_back(Battle("Test", "../HKK/ASCII-Battle2_r.txt", 
		"../HKK/ASCII-Battle2_passable.txt", 1.0));
	battles[1].battleField.enemyCreateBlock = Rect(350, 270, 300, 100);
	battles[1].battleField.playerCreatePos[0] = 400;
	battles[1].battleField.playerCreatePos[1] = 350;
	battles[1].addEnemy(1, 2, Rect(238, 335, 100, 20));
	battles[1].addEnemy(1, 1, Rect(410, 300, 50, 20));
	battles[1].addEnemy(1, 2, Rect(480, 275, 10, 90));
	battles[1].addEnemy(4, 3, Rect(480, 275, 10, 90));
	battles[1].addEnemy(5, 3, Rect(520, 275, 10, 90));
	battles[1].addEnemy(10, 5, Rect(690, 215, 350, 65));
	battles[1].addEnemy(11, 2, Rect(690, 215, 350, 65));
	battles[1].addEnemy(13, 2, Rect(690, 215, 350, 65));
	battles[1].addEnemy(14, 1, Rect(1000, 300, 50, 65));
	battles[1].addEnemy(15, 1, Rect(1000, 300, 50, 65));

	battles.push_back(Battle("Test", "../HKK/ASCII-Battle3_r.txt", 
		"../HKK/ASCII-Battle3_passable.txt", 1.25));
	battles[2].battleField.enemyCreateBlock = Rect(350, 270, 300, 100);
	battles[2].battleField.playerCreatePos[0] = 240;
	battles[2].battleField.playerCreatePos[1] = 340;
	battles[2].addEnemy(6, 4, Rect(360, 256, 116, 112));
	battles[2].addEnemy(12, 3, Rect(800, 190, 233, 86));
	battles[2].addEnemy(16, 4, Rect(456, 262, 378, 97));
	battles[2].addEnemy(20, 4, Rect(360, 256, 116, 112));
	battles[2].addEnemy(17, 1, Rect(414, 351, 2, 2));
	battles[2].addEnemy(17, 1, Rect(516, 359, 2, 2));
	battles[2].addEnemy(17, 1, Rect(740, 364, 2, 2));
	battles[2].addEnemy(17, 1, Rect(880, 364, 2, 2));
	battles[2].addEnemy(18, 3, Rect(950, 333, 117, 62));
	battles[2].addEnemy(22, 1, Rect(640, 315, 2, 2));

	battles.push_back(Battle("Test", "../HKK/ASCII-Battle4_r.txt", 
		"../HKK/ASCII-Battle4_passable.txt", 1.5));
	battles[3].battleField.enemyCreateBlock = Rect(350, 270, 300, 100);
	battles[3].battleField.playerCreatePos[0] = 785;
	battles[3].battleField.playerCreatePos[1] = 330;
	battles[3].addEnemy(19, 10, Rect(460, 170, 360, 78));
	battles[3].addEnemy(21, 4, Rect(340, 190, 120, 165));
	battles[3].addEnemy(23, 5, Rect(472, 270, 160, 80));
	battles[3].addEnemy(24, 1, Rect(422, 182, 55, 27));
	battles[3].addEnemy(24, 1, Rect(800, 175, 55, 27));
	battles[3].addEnemy(24, 1, Rect(1014, 260, 50, 24));
	battles[3].addEnemy(25, 1, Rect(100, 158, 250, 115));
	/*
	battles[3].addEnemy(&enemies[0]);
	battles[3].addEnemy(&enemies[1]);
	battles[3].addEnemy(&enemies[1]);
	battles[3].addEnemy(&enemies[2]);
	battles[3].addEnemy(&enemies[2]);
	battles[3].addEnemy(&enemies[0]);*/
}