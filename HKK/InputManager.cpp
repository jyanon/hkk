
#include "stdafx.h"

#include "InputManager.h"
char InputManager::hit;
int InputManager::keyCd;
//const int InputManager::StandardKeyCoolDown = 5;

bool InputManager::isAnyKeyPress() {
	bool res = _kbhit();
	if (res) hit = _getch();
	else hit = '\0';
	return res;
}/*
bool InputManager::isKeyPress(char key) {
	return getInputKey() == key || getInputKeyInUppercase() == key;
}*/
bool InputManager::isKeyPress(char key) {
	bool res = KEY_DOWN(key);
	if(res) keyCd = StandardKeyCoolDown;
	return res;
}
bool InputManager::isKeyPressWithCD(char key) {
	if (keyCd > 0) return false;
	return isKeyPress(key);
}
char InputManager::getInputKey() { return hit; }

char InputManager::getInputKeyInUppercase() {
	char key = getInputKey();
	if ('a' <= key && key <= 'z') key -= 32;
	return key;
}
int InputManager::dir8() {
	int x = 0, y = 0;
	x = KEY_DOWN(Right) - KEY_DOWN(Left);
	y = KEY_DOWN(Down) - KEY_DOWN(Up);
	if (x == 0 && y == 0)   return -1;
	if (x == 1 && y == 0)   return 0;
	if (x == 1 && y == 1)   return 1;
	if (x == 0 && y == 1)   return 2;
	if (x == -1 && y == 1)  return 3;
	if (x == -1 && y == 0)  return 4;
	if (x == -1 && y == -1) return 5;
	if (x == 0 && y == -1)  return 6;
	if (x == 1 && y == -1)  return 7;
}
int InputManager::UD() {
	if (keyCd > 0) return 0;
	int res = KEY_DOWN(Down) - KEY_DOWN(Up);
	if (res != 0)keyCd = StandardKeyCoolDown;
	return res;
}
int InputManager::LR() {
	if (keyCd > 0) return 0;
	int res = KEY_DOWN(Right) - KEY_DOWN(Left);
	if (res != 0)keyCd = StandardKeyCoolDown;
	return res;
	/*
	if (keyCd > 0) return 0;
	keyCd = StandardKeyCoolDown;
	return KEY_DOWN(Right) - KEY_DOWN(Left);*/
}

void InputManager::update() {
	if (keyCd > StandardKeyCoolDown) keyCd = 0;
	if (keyCd > 0) keyCd--;
}