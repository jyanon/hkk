#pragma once

#include "stdafx.h"

#include "ScreenManager.h"

#include <cstdlib>
#include <string>
using namespace std;

struct CONSOLE_FONT
{
	DWORD index;
	COORD dim;
};

//双缓冲处理显示

typedef BOOL(WINAPI *PROCSETCONSOLEFONT)(HANDLE, DWORD);
typedef BOOL(WINAPI *PROCGETCONSOLEFONTINFO)(HANDLE, BOOL, DWORD, CONSOLE_FONT*);
typedef COORD(WINAPI *PROCGETCONSOLEFONTSIZE)(HANDLE, DWORD);
typedef DWORD(WINAPI *PROCGETNUMBEROFCONSOLEFONTS)();
typedef BOOL(WINAPI *PROCGETCURRENTCONSOLEFONT)(HANDLE, BOOL, CONSOLE_FONT*);

PROCSETCONSOLEFONT SetConsoleFont;
PROCGETCONSOLEFONTINFO GetConsoleFontInfo;
PROCGETCONSOLEFONTSIZE GConsoleFontSize;
PROCGETNUMBEROFCONSOLEFONTS GetNumberOfConsoleFonts;
PROCGETCURRENTCONSOLEFONT GCurrentConsoleFont;

char** ScreenManager::data;// [GHeight][GWidth];
HANDLE ScreenManager::hOutput, ScreenManager::hOutBuf;

DWORD ScreenManager::bytes = 0;
COORD ScreenManager::coord = { 0,0 };

stack<Scene*> ScreenManager::scenes;
bool ScreenManager::bufferSelector = false;
Sprite* ScreenManager::screenSprite;

const double ScreenManager::FontSize = GFontSize;
const int ScreenManager::Width = GWidth;
const int ScreenManager::Height = GHeight;

void ScreenManager::setup() {
	createData();
	setupScreenSize();
	setupFontSize();
	setupScreenBuffers();
	setupScreenSprite();
}
void ScreenManager::createData() {
	data = new char *[Height];
	for (int y = 0; y <Height; y++)
		data[y] = new char[Width];
	clearData();
}
void ScreenManager::clearData() {
	for (int y = 0; y < Height; y++)
		for (int x = 0; x < Width; x++)
			data[y][x] = ' ';// 32 + (x + y) % 64;
}

void ScreenManager::setupScreenSize() {
	char setting[30];
	sprintf_s(setting, "mode con:cols=%d lines=%d", Width, Height);
	system(setting);
}

void ScreenManager::setupFontSize() {
	int nNumFont;

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	HMODULE hKernel32 = GetModuleHandle(L"kernel32");

	SetConsoleFont = (PROCSETCONSOLEFONT)GetProcAddress(hKernel32, "SetConsoleFont");
	GetConsoleFontInfo = (PROCGETCONSOLEFONTINFO)GetProcAddress(hKernel32, "GetConsoleFontInfo");
	GConsoleFontSize = (PROCGETCONSOLEFONTSIZE)GetProcAddress(hKernel32, "GetConsoleFontSize");
	GetNumberOfConsoleFonts = (PROCGETNUMBEROFCONSOLEFONTS)GetProcAddress(hKernel32, "GetNumberOfConsoleFonts");
	GCurrentConsoleFont = (PROCGETCURRENTCONSOLEFONT)GetProcAddress(hKernel32, "GetCurrentConsoleFont");

	nNumFont = GetNumberOfConsoleFonts();
	CONSOLE_FONT *pFonts = new CONSOLE_FONT[nNumFont];
	GetConsoleFontInfo(hConsole, 0, nNumFont, pFonts);

	SetConsoleFont(hConsole, FontSize);
}

void ScreenManager::setupScreenBuffers() {

	//创建新的控制台缓冲区
	hOutBuf = CreateConsoleScreenBuffer(
		GENERIC_WRITE,//定义进程可以往缓冲区写数据
		FILE_SHARE_WRITE,//定义缓冲区可共享写权限
		NULL,
		CONSOLE_TEXTMODE_BUFFER,
		NULL
	);
	hOutput = CreateConsoleScreenBuffer(
		GENERIC_WRITE,//定义进程可以往缓冲区写数据
		FILE_SHARE_WRITE,//定义缓冲区可共享写权限
		NULL,
		CONSOLE_TEXTMODE_BUFFER,
		NULL
	);
	//隐藏两个缓冲区的光标
	CONSOLE_CURSOR_INFO cci;
	cci.bVisible = 0;
	cci.dwSize = 1;
	SetConsoleCursorInfo(hOutput, &cci);
	SetConsoleCursorInfo(hOutBuf, &cci);
}

void ScreenManager::setupScreenSprite(){
	screenSprite = new Sprite(Width,Height);
	screenSprite->z = 100000;
}

void ScreenManager::showScreen(HANDLE buf) {
	for (int y = 0; y<Height; y++) {
		coord.Y = y;
		WriteConsoleOutputCharacterA(buf, data[y], Width, coord, &bytes);
	}
	SetConsoleActiveScreenBuffer(buf);

}

Scene* ScreenManager::currentScene() {
	return scenes.empty() ? NULL : scenes.top();
}
void ScreenManager::popScene() {
	if (scenes.empty()) return;
	if (currentScene()!=NULL) 
		delete currentScene();
	scenes.pop();
}
void ScreenManager::clearScene() {
	while (!scenes.empty()) popScene();
}
void ScreenManager::pushScene(Scene* scene) {
	scene->addChild(screenSprite);
	scenes.push(scene);
}
void ScreenManager::gotoScene(Scene* scene) {
	scene->addChild(screenSprite);
	popScene(); scenes.push(scene);
}
void ScreenManager::resetToScene(Scene* scene) {
	scene->addChild(screenSprite);
	clearScene(); scenes.push(scene);
}
void ScreenManager::update() {
	InputManager::update();
	Scene* scene = currentScene();
	if(scene != NULL) scene->update();
}
void ScreenManager::render() {
	Scene* scene = currentScene();
	if (scene == NULL) clearData();
	else {
		clearData();
		scene->quickRender(data);

		ScreenManager::showScreen(bufferSelector ? hOutBuf : hOutput);
		bufferSelector = !bufferSelector;
	}
}