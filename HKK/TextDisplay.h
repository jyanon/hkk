#pragma once

#include"Sprite.h"
#include"BattleManager.h"

class CharDisplay : public Sprite {
public:
	static const int FontXCount;

	CharDisplay(int yCount);
	CharDisplay(string fileName, int yCount);
	CharDisplay(Bitmap* bitmap,int yCount);
	void setup(int index,int length);
	void setIndex(int index);
	void setLength(int length);

	int getFontWidth();
	int getFontHeight();

	void refresh();
private:

	int getFrameRectWidth();
	int getFrameRectHeight();
	int getFrameRectX();
	int getFrameRectY();

	void clearFrame();
	void refreshFrame();

	int index,length;
	int fontYCount;
};

class TextDisplay : public Sprite {
public:

	TextDisplay();
	TextDisplay(string fileName);
	TextDisplay(Bitmap* bitmap);
	~TextDisplay();
	void setLineHeightRate(double rate);
	virtual void setText(string text);
	virtual string getText();

	virtual int getWidth();
	virtual int getHeight();

	virtual void refresh();

protected:
	virtual void processText();
	virtual void processChar(int &lineHeight, char c);

	virtual int getCharIndex(char c);
	virtual int getCharLength(char c);

	virtual CharDisplay* addCharDisplay(char c);
	void clearCharDisplays();

	int textX, textY;
	int textWidth, textHeight;
	double lineHeightRate;

	Bitmap* fontFace;
	vector<CharDisplay*> charDisplays;
	string text;
};

class NumberDisplay : public TextDisplay {
public:

	NumberDisplay();
	NumberDisplay(string fileName);
	NumberDisplay(Bitmap* bitmap);

	virtual void setNum(int num);
	virtual int getNum();
protected:
	virtual void processText();
	virtual void processValue(int &lineHeight, int c);

	virtual int getValueIndex(int c);

	virtual CharDisplay* addCharDisplay(int c);

	int num;
};

class TypableNumberDisplay : public NumberDisplay {
public:

	TypableNumberDisplay(int nmax = 100, int nmin = 0);
	TypableNumberDisplay(string fileName, int nmax = 100, int nmin = 0);
	TypableNumberDisplay(Bitmap* bitmap, int nmax = 100, int nmin = 0);

	virtual void setNum(int num);

	void activate(); void deactivate();
	bool isActive();

	bool isChanged();
protected:

	void updateOthers();
	void updateTypingEffect();
	void updateTyping();

	bool active;
	bool changed;
	int keyCd;
	int nmax, nmin;
	int count;
};

class DamageDisplay : public TextDisplay {
public:
	static const int UpingSpeed;
	static const int ShowCount;

	DamageDisplay(Damage damage);

	void setDamageData(Damage damage);

protected:

	void updateOthers();
	void updateUping();

	void processText();
	void processDamageData();
	void processValue(int &lineHeight, int value, Damage::DamageType type);
	void processFlag(int &lineHeight);

	CharDisplay* addCharDisplay();

	int getValueIndex(int c, Damage::DamageType type);
	int getFlagIndex();
	int getFlagLength();

	int count;
	Damage damageData;
};