#pragma once

#include<map>
#include "InputManager.h"
#include "database.h"
class ObjectBattler;
class ObjectActor;/*
struct ItemBase;
struct Item;
struct Skill;
struct System;
*/
vector<string> split(const string& str, const string& delim);

static class GameManager {
public:
	static const int InitialGold;
	static const int SkillSlotCount = 5;
	static const int ItemSlotCount = 3;
	static const int BattleCount = System::MaxMapCount;
	static const int LevelCount = System::MaxLevelCount;
	static const InputManager::KeyMap skillSlotKeys[SkillSlotCount];
	static const InputManager::KeyMap itemSlotKeys[ItemSlotCount];

	static void setupGame(int id);
	static void setdownGame();
	static void clearSkillSlots();
	static void clearItemSlots();

	static Skill* getSkillSlot(int id);
	//static pair<Item*, int> getItemSlot(int id);
	static Item* getItemSlot(int id);

	static void setSkillSlot(Skill* skill, int id);
	static void switchSkillSlot(Skill* skill, int id);
	static void setItemSlot(Item* item, int id);
	static void switchItemSlot(Item* item, int id);

	static void useItemSlot(int count, int id);

	static void clearSkillSlot(int id);
	static void clearItemSlot(int id);

	static void gainItem(ItemBase* item, int count, bool newEquip=true);
	static void loseItem(ItemBase* item, int count);
	static bool hasItem(ItemBase* item, bool include = true);
	static int itemNum(ItemBase* item);

	static void gainGold(int count);
	static void loseGold(int count);

	static int getBattleScoreRecord(int id, int level);
	static int getBattleScoreJudge(int id, int level);
	static void setBattleScoreRecord(int id, int level, int record, int judge);
	static bool isBattleLevelPassed(int level);

	static int getGold();
	static vector<ItemBase*> getItems();

	static int getLeaderId();

	static ObjectActor* getLeader();
	static ObjectActor* getActor(int id);

	static bool inBattle;
	static bool newSkill;
	static bool newEquip;

	static string makeSaveData();
	static void loadSaveData(string data);
private:
	static void initializeItems();
	static void initializeRecords();

	static int leaderId;
	static int gold;
	static int battleRecord[BattleCount][LevelCount][2];
	static Skill* skillSlots[SkillSlotCount];
	//static pair<Item*,int> itemSlots[itemSlotCount];
	static Item* itemSlots[ItemSlotCount];

	static map<ItemBase*,int> items;

	static ObjectActor* leader;

	static map<int,ObjectActor> actors;
};