
#include "stdafx.h"

#include<fstream>
#include<ctime>
#include "ObjectBattler.h"
#include "DataManager.h"

const char* DataManager::GameSaveFormat = "../HKK/data_%d.gs";
const string DataManager::SaveFileDataPath = "../HKK/SaveFile.sf";

SaveFileData DataManager::fileDatas[DataManager::MaxSaveFileCount];

string SaveFileData::dataToString() {
	if (empty) return ";";
	char data_c[50];
	sprintf_s(data_c, "%d %d %d ",leaderId, level, gold);
	return leaderName + "|" + data_c + "|" + time + ";";
}
void SaveFileData::stringToData(string str) {
	vector<string> dataV = split(str, "|");
	empty = dataV.size()<3;
	if(!empty){
		empty = false;
		leaderName = dataV[0]; time = dataV[2];
		sscanf_s(dataV[1].c_str(), "%d %d %d ", &leaderId, &level, &gold);
	}
}

void DataManager::saveGame(int id) {
	string fileData = getSaveGameData();
	char name_c[50]; fstream file;
	sprintf_s(name_c, GameSaveFormat, id);
	file.open(name_c, ios::out); // 打开文件 逐字输入
	for (int i = 0; i < fileData.size(); i++)
		file << fileData[i];
	file.close();
	updateGameFile(id);
}
void DataManager::loadGame(int id) {
	string fileData = "";
	char name_c[50]; fstream file;
	sprintf_s(name_c, GameSaveFormat, id);
	file.open(name_c, ios::in); // 打开文件 逐字读取
	char c; file >> noskipws;
	while (!file.eof()) {
		file >> c; fileData += c;
	}
	file.close();
	GameManager::loadSaveData(fileData);
}
void DataManager::deleteGame(int id) {
	char name_c[50]; fstream file;
	sprintf_s(name_c, GameSaveFormat, id);
	deleteGameFile(id);
	remove(name_c);
}

void DataManager::loadSaveFiles() {
	string fileData = ""; fstream file;
	file.open(SaveFileDataPath, ios::in); // 打开文件 逐字读取
	char c; file >> noskipws;
	int line = 0;
	while (!file.eof()) {
		file >> c;
		if (c == ';') {
			fileDatas[line++].stringToData(fileData);
			fileData = "";
		}
		else fileData += c;
	}
	file.close();
}
void DataManager::saveSaveFiles() {
	string fileData = getSaveGameFileData();
	fstream file;
	file.open(SaveFileDataPath, ios::out); // 打开文件 逐字输入
	for (int i = 0; i < fileData.size(); i++)
		file << fileData[i];
	file.close();
}
SaveFileData* DataManager::getFileData(int id) { return &fileDatas[id]; }
string DataManager::getSaveGameData() {
	return GameManager::makeSaveData();
}
string DataManager::getSaveGameFileData() {
	string data = "";
	for (int i = 0; i < MaxSaveFileCount; i++)
		data += fileDatas[i].dataToString();
	return data;
}

void DataManager::updateGameFile(int id) {
	time_t current = time(NULL);
	tm* t = localtime(&current);
	char time_c[50];
	sprintf_s(time_c, "%d-%02d-%02d\n      %02d:%02d:%02d",
		t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
		t->tm_hour, t->tm_min, t->tm_sec
	);
	fileDatas[id].empty = false;
	fileDatas[id].leaderId = GameManager::getLeaderId();
	fileDatas[id].leaderName = GameManager::getLeader()->getName();
	fileDatas[id].level = GameManager::getLeader()->getLevel();
	fileDatas[id].gold = GameManager::getGold();
	fileDatas[id].time = time_c;
	saveSaveFiles();
}
void DataManager::deleteGameFile(int id) {
	fileDatas[id].empty = true;
	saveSaveFiles();
}