#pragma once
#include<vector>

#include "Rect.h"
#include "Stage.h"
using namespace std;
class Stage;

class DisplayObject {
public:
	void update();
	virtual Rect getObjectRect();

	virtual vector<vector<char> > getDisplayData();

	void render(Stage* stage, int lx, int ly);
	void render(char** map, int lx, int ly);
	virtual void quickRender(char** &map, int lx, int ly);

	void addChild(DisplayObject* obj);
	void removeChild(DisplayObject* obj);
	void clearChildren();

	void addBackChild(DisplayObject* obj);
	void removeBackChild(DisplayObject* obj);
	void clearBackChildren();

	//void setParent(DisplayObject* obj);

	virtual int getWidth();
	virtual int getHeight();

	bool visible;
	int x, y, z;

private:
	void updateChildren();

protected:
	virtual void updateOthers();

	DisplayObject* parent;
	vector<DisplayObject*> children;
	vector<DisplayObject*> backChildren;
	int width, height;
};
bool DisplayObjectComp(const DisplayObject* a, const DisplayObject* b);