
#include "stdafx.h"

#include "Scene.h"
#include "BattleManager.h"
#include "ScreenManager.h"
#include "DataManager.h"

Scene::Scene() : Stage() {
	requestPop = false; 
	requestScene = NULL;
	initialiazeEffect();
	createSprites();
}
void Scene::initialiazeEffect() {
	shineChar = 'T'; shakePower = 0;
	shineDuration = shakeDuration = 0;
}
void Scene::createSprites() {}

void Scene::shake(int power, int duration) {
	shakePower = power; shakeDuration = duration;
}
void Scene::shine(char c = 'T', int duration = 0) {
	shineChar = c; shineDuration = duration;
}
void Scene::requestPopScene() { requestPop = true; }
bool Scene::isRequestPopScene() { return requestPop; }

void Scene::requestNextScene(Scene* scene) { requestScene = scene; }
Scene* Scene::getRequestNextScene() { return requestScene; }

void Scene::update() {
	updateChildren();
	updateOthers(); 
	updatePopScene();
}
void Scene::updateChildren() {
	for (int i = 0; i < children.size(); i++) {
		DisplayObject* obj = children[i];
		obj->update();
	}
}
void Scene::updatePopScene() {
	Scene* next = getRequestNextScene();
	if (next != NULL) ScreenManager::gotoScene(next);
	else if (isRequestPopScene()) ScreenManager::popScene();
}
void Scene::updateOthers() {
	updateEffect();
}
void Scene::updateEffect() {
	updateShake();
	updateShine();
}
void Scene::updateShake() {
	if (shakeDuration > 0) {
		x = shakePower * sin(shakeDuration);
		if (--shineDuration <= 0) { x = 0; y = 0; }
	}
}
void Scene::updateShine() {
	if (shineDuration > 0) { shineDuration--; }
	if (shineDuration <= 0) shineChar = 'T';
}
void Scene::render() {
	if (shineDuration > 0) {
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				data[y][x] = shineChar;
	}else Stage::render();
}
void Scene::render(char** data) {
	if (shineDuration > 0) {
		for (int y = this->y; y < height; y++)
			for (int x = this->x; x < width; x++)
				if(y >= 0 && y < GHeight && x >= 0 && x < GWidth)
					data[y][x] = shineChar;
	}
	else Stage::render(data);
}
void Scene::quickRender(char** &data) {
	if (shineDuration > 0) {
		for (int y = this->y; y < height; y++)
			for (int x = this->x; x < width; x++)
				if (y >= 0 && y < GHeight && x >= 0 && x < GWidth)
					data[y][x] = shineChar;
	}
	else Stage::quickRender(data);
}
/*
SceneTest::SceneTest() : Scene() { createSprites(); };
void SceneTest::createSprites() {
	cnt = 0;
	createTestSprite();
}
void SceneTest::createTestSprite() {

	test1 = new SpriteExtend("../HKK/ASCII-Package1_1.txt");
	test2 = new Sprite("../HKK/ASCII-Speak1.txt");
	background1 = new Sprite("../HKK/ASCII-Grassland.txt");
	background2 = new Sprite("../HKK/ASCII-Forest.txt");

	actor1 = new SpriteActor(0);

	//testAni1 = new Animation(0);
	testAni2 = new Animation(1);
	//testAni3 = new Animation(0);

	background1->z = -10;
	background2->z = -5;
	test1->x = -100; test1->setMirror(true);
	test1->z = 5;
	test2->x = 200 - test2->getWidth(); test2->y = 15;
	test2->setFrameRect(test2->getWidth(), 0, 0, test2->getHeight());
	testAni2->x = 200; testAni2->y = 100;
	testAni2->z = 0;
	testAni2->setLoop(true);

	actor1->x = 100; actor1->y = 100;
	actor1->z = 100;
	test1->addChild(test2);
	//test1->addAnimation(testAni1);
	//test1->addAnimation(testAni2);
	//test1->addAnimation(testAni3);

	addChild(background1);
	addChild(background2);
	addChild(testAni2);
	addChild(test1);
	addChild(actor1);
}
void SceneTest::updateOthers() {
	cnt++;
	//if (cnt % 30==0) test1->shine(32+rand()%64, 5);
	test1->x += (200 - test1->x) / 10;
	if (test1->x >= 180) {
		//testAni1->play();
		test2->visible = true; test1->setMirror(false);
		Rect r = test2->getFrameRect();
		if (r.x > 0) {
			test2->x = 200 - test2->getWidth() + r.x;
			r.x -= 8; r.width += 8;
			test2->setFrameRect(r);
			if (r.x <= 0) test2->setFrameRect();
		}
	}
	//if (test1->x >= 35) testAni2->play();
	//if (test1->x >= 170) testAni3->play();
}
*/

SceneTitle::SceneTitle() : Scene() { createSprites(); };
void SceneTitle::createSprites() {
	cnt = 0; 
	createBackgroundSprites();
	createCommandSprites();
	createLoadWindow();
}
void SceneTitle::createLoadWindow() {
	loadWindow = new WindowFile(true);
	loadWindow->x = -292;
	loadWindow->z = 1000;
	loadWindow->visible = false;
	addChild(loadWindow);
}
void SceneTitle::createBackgroundSprites() {
	background1 = new Sprite(titleBackground1);
	background2 = new Sprite(titleBackground2);
	background3 = new Sprite(titleBackground3);
	title1 = new Sprite(titleText1);
	title2 = new Sprite(titleText2);

	background1->z = 0;
	background2->z = -50;
	background3->z = -50;
	background3->visible = false;
	title1->x = -300; title1->y = 10; title1->z = -10;
	title2->x = 800; title2->y = 56; title2->z = -10;

	addChild(background1);
	addChild(background2);
	addChild(background3);
	addChild(title1);
	addChild(title2);
}
void SceneTitle::createCommandSprites() {
	commandGroup = new SpriteCommandGroup(titleCommandBg,1,0);
	SpriteCommand* cmd;

	commandGroup->z = -10;
	commandGroup->x = 275;
	commandGroup->y = 100;

	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-TitleCommand1.txt"));
	cmd->x = -256; cmd->y = 0;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-TitleCommand2.txt"));
	cmd->x = 512; cmd->y = 24;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-TitleCommand3.txt"));
	cmd->x = -768; cmd->y = 48;

	commandGroup->deactivate();

	addChild(commandGroup);
}
bool SceneTitle::okEnable() {
	return commandGroup->isActive();
}
void SceneTitle::updateOthers() {
	Scene::updateOthers();
	updateControl();
	updateSprites();
}
void SceneTitle::updateControl() {
	if (okEnable() && InputManager::isKeyPressWithCD(InputManager::OK))
		processOK(commandGroup->getIndex());
}
void SceneTitle::processOK(int index) {
	commandGroup->deactivate();
	switch (index) {
	case 0: ScreenManager::pushScene(new SceneStart()); break;
	case 1: loadWindow->activate(); loadWindow->visible = true; break;
	case 2: exit(0); break;
	}
}
void SceneTitle::updateSprites() {
	if (cnt++ >= 20 && title1->x<286) title1->x += (290 - title1->x) / 4;
	if (title1->x >= 200) title2->x += (260 - title2->x) / 5;
	if (title1->x >= 286 && rand() % 100 == 0) title1->shake(5, 5);
	if (title2->x <= 286 && rand() % 100 == 0) title2->shine('#', 5,false);
	if (title2->x <= 286) {
		commandGroup->getCommand(0)->x /= 2;
		commandGroup->getCommand(1)->x /= 2;
		commandGroup->getCommand(2)->x /= 2;
	}
	if (!loadWindow->isActive() && !commandGroup->isActive() &&
		commandGroup->getCommand(1)->x == 0) commandGroup->activate();

	if (rand() % 100 == 0) {
		background2->shake(5, 5); background3->shake(5, 5);
		background2->visible = !background2->visible;
		background3->visible = !background3->visible;
	}

	if (loadWindow->isActive() && loadWindow->x < 0) loadWindow->x += -loadWindow->x / 2;
	if (!loadWindow->isActive() && loadWindow->x > -292) loadWindow->x += (-292 - loadWindow->x) / 2;
	if (loadWindow->x <= -290) loadWindow->visible = false;
}

SceneStart::SceneStart() : Scene() { 
	createSprites(); select(0);
}

void SceneStart::createSprites() {
	createGroundSprites();
	createArrowsSprites();
	createBigFaceSprites();
	createParamsSprites();
}
int SceneStart::getCount() { return actors.size(); }


void SceneStart::select(int id) { index = id; setActor(id); }
void SceneStart::setActor(int id) { setActor(&actors[id]); }
void SceneStart::setActor(Actor* actor) {
	this->actor = actor; refresh();
}
Actor* SceneStart::getActor() { return actor; }

void SceneStart::refresh() {
	actorBigFace->setBitmap(Bitmap::getBitmap(actor->bigFace));
	actorBigFace->x = -250;
	drawActorParams();
}
void SceneStart::drawActorParams() { drawParams(actor); }

void SceneStart::drawParams(Actor* actor) {
	char info_c[1000];
	string info;
	/*
	string fromat = "Name    %s   \n";
		  fromat += "LEVEL   1    \n";
		  fromat += "MHP     %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "MMP     %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "ATTACK  %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "DEFENSE %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "M. ATK  %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "M. DEF  %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "AGILITY %d   \n";
		  fromat += "             (*%.2f)\n";
		  fromat += "LUCKY   %d   \n";
		  fromat += "             (*%.2f)\n";
	*/
	info = "- Select Actor -\n";
	info += "Name    " + actor->name;
	sprintf_s(info_c, "\nLEVEL   1\nMHP     %d\n           (*%.2f)\nMMP     %d\n           (*%.2f)\nATTACK  %d\n           (*%.2f)\nDEFENSE %d\n           (*%.2f)\nM. ATK  %d\n           (*%.2f)\nM. DEF  %d\n           (*%.2f)\nAGILITY %d\n           (*%.2f)\nLUCKY   %d\n           (*%.2f)\n",
		actor->params[0], actor->paramsGrowth[0],
		actor->params[1], actor->paramsGrowth[1],
		actor->params[2], actor->paramsGrowth[2],
		actor->params[3], actor->paramsGrowth[3],
		actor->params[4], actor->paramsGrowth[4],
		actor->params[5], actor->paramsGrowth[5],
		actor->params[6], actor->paramsGrowth[6],
		actor->params[7], actor->paramsGrowth[7]
	);
	textDisplay->setText(info += info_c);
}

void SceneStart::createGroundSprites() {
	foreground = new Sprite(titleBackground1);
	background = new Sprite(titleBackground3);
	foreground->z = 20; background->z = -5;
	addChild(foreground); addChild(background);
}
void SceneStart::createArrowsSprites() {
	Bitmap* arrow = Bitmap::getBitmap("../HKK/ASCII-Arrow.txt");
	rightArrow = new Sprite(arrow);
	rightArrow->x = SWidth - rightArrow->getSpriteWidth();
	rightArrow->y = (SHeight - rightArrow->getSpriteHeight()) / 2;
	rightArrow->z = 50;

	leftArrow = new Sprite(arrow);
	leftArrow->setMirror(true);
	leftArrow->x = 0; leftArrow->z = 50;
	leftArrow->y = (SHeight - leftArrow->getSpriteHeight()) / 2;

	addChild(leftArrow);
	addChild(rightArrow);
}
void SceneStart::createBigFaceSprites() {
	actorBigFace = new Sprite();
	addChild(actorBigFace);
}
void SceneStart::createParamsSprites() {
	paramsDisplay = new Sprite(Bitmap::getBitmap("../HKK/ASCII-ParamsDisplay.txt"));
	paramsDisplay->setX(84); paramsDisplay->z = 100;
	paramsDisplay->setY((SHeight- paramsDisplay->getSpriteHeight())/2);

	textDisplay = new TextDisplay(defaultFontFaceSmall);
	textDisplay->x = 14; textDisplay->y = 5;
	//textDisplay->setLineHeightRate(1.2);
	paramsDisplay->addChild(textDisplay);
	addChild(paramsDisplay);
}

void SceneStart::scrollLeft() {
	int id = (index - 1 + getCount()) % getCount();
	select(id); leftArrow->shake(5, 4);
	leftArrow->shine(' ', 5);
}
void SceneStart::scrollRight() {
	int id = ((index + 1) % getCount());
	select(id); rightArrow->shake(5, 4);
	rightArrow->shine(' ', 5);
}
void SceneStart::setupGame() {
	GameManager::setupGame(index);
}
void SceneStart::updateOthers() {
	Scene::updateOthers();
	updateSprites();
	updateControl();
}
void SceneStart::updateSprites() {
	actorBigFace->x += (128 - actorBigFace->x) / 5;
	if (rand() % 150 == 0) paramsDisplay->shake(5, 5);
	if (rand() % 100 == 0) background->shine('.',5);
}
void SceneStart::updateControl() {
	//if (--keyCd > 0) return;
	if (InputManager::isKeyPressWithCD(InputManager::OK)) {
		setupGame(); requestNextScene(new SceneMenu());
	}
	else {
		int lr = InputManager::LR();
		if (lr == 0) return; keyCd = 5;
		lr > 0 ? scrollRight() : scrollLeft();
	}
}

SceneMenu::SceneMenu() : Scene() {
	createSprites();
}
void SceneMenu::createSprites() {
	createBackgroundSprites();
	createBigFaceSprites();
	createCommandSprites();
	createWindowsSprites();
	createNewPromptsSprites();
}

void SceneMenu::createBackgroundSprites() {
	background = new Sprite(menuBackground);
	background->z = -5;
	addChild(background);
}
void SceneMenu::createBigFaceSprites() {
	actorBigFace = new Sprite(GameManager::getLeader()->getBigFaceName());
	actorBigFace->x = -364;
	addChild(actorBigFace);
}
void SceneMenu::createCommandSprites() {
	commandGroup = new SpriteCommandGroup(menuCommandBg, 2, 0);
	SpriteCommand* cmd;

	commandGroup->z = 50;
	commandGroup->y = 25;

	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand1.txt"));
	cmd->x = 128; cmd->y = 0;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand2.txt"));
	cmd->x = 256; cmd->y = 20;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand3.txt"));
	cmd->x = 384; cmd->y = 40;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand4.txt"));
	cmd->x = 512; cmd->y = 60;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand5.txt"));
	cmd->x = 640; cmd->y = 80;
	cmd = commandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-MenuCommand6.txt"));
	cmd->x = 768; cmd->y = 100;

	commandGroup->setActiveShake(false);
	commandGroup->setActiveShineChar('*');
	commandGroup->deactivate();
	commandGroup->x = SWidth - commandGroup->getWidth();

	addChild(commandGroup);
}
void SceneMenu::createWindowsSprites() {
	createMapWindow();
	createItemWindow();
	createSkillWindow();
	createShopWindow();
	createSaveWindow();
}
void SceneMenu::createNewPromptsSprites() {
	newEquip = new Sprite(Bitmap::getBitmap("../HKK/ASCII-NewEquipPrompt.txt"));
	newSkill = new Sprite(Bitmap::getBitmap("../HKK/ASCII-NewSkillPrompt.txt"));
	newer = new Sprite(Bitmap::getBitmap("../HKK/ASCII-NewerPrompt.txt"));

	newEquip->x = 0; newEquip->y = SHeight - newEquip->getSpriteHeight();
	newSkill->x = 0; newSkill->y = SHeight - newEquip->getSpriteHeight();
	newer->x = newer->y = 0;
	newEquip->z = newSkill->z = newer->z = 500;
	newEquip->visible = newSkill->visible = newSkill->visible = false;
	addChild(newEquip);
	addChild(newSkill);
	addChild(newer);
}
void SceneMenu::createMapWindow() {
	mapWindow = new WindowMap();
	mapWindow->x = -292;
	mapWindow->z = 1000;
	mapWindow->visible = false;
	addChild(mapWindow);
}
void SceneMenu::createItemWindow() {
	itemWindow = new WindowItem();
	itemWindow->x = -292;
	itemWindow->z = 1000;
	itemWindow->visible = false;
	addChild(itemWindow);
}
void SceneMenu::createSkillWindow() {
	skillWindow = new WindowSkill();
	skillWindow->x = -292;
	skillWindow->z = 1000;
	skillWindow->visible = false;
	addChild(skillWindow);
}
void SceneMenu::createShopWindow() {
	shopWindow = new WindowShop();
	shopWindow->x = -292;
	shopWindow->z = 1000;
	shopWindow->visible = false;
	addChild(shopWindow);
}
void SceneMenu::createSaveWindow() {
	saveWindow = new WindowFile();
	saveWindow->x = -292;
	saveWindow->z = 1000;
	saveWindow->visible = false;
	addChild(saveWindow);
}
bool SceneMenu::okEnable() {
	return commandGroup->isActive();
}
bool SceneMenu::anyWindowActive() {
	return mapWindow->isActive() ||
		itemWindow->isActive() ||
		skillWindow->isActive() ||
		shopWindow->isActive() ||
		saveWindow->isActive();
}
SpriteWindow* SceneMenu::getActiveWindow() {
	if (mapWindow->isActive()) return mapWindow;
	if (itemWindow->isActive()) return itemWindow;
	if (skillWindow->isActive()) return skillWindow;
	if (shopWindow->isActive()) return shopWindow;
	if (saveWindow->isActive()) return saveWindow;
	return NULL;
}
void SceneMenu::updateOthers() {
	Scene::updateOthers();
	updateControl();
	updateSprites();
}
void SceneMenu::updateControl() {
	if (okEnable() && InputManager::isKeyPressWithCD(InputManager::OK))
		processOK(commandGroup->getIndex());
}
void SceneMenu::processOK(int index) {
	commandGroup->deactivate();
	switch (index) {
	case 0: mapWindow->activate(); mapWindow->visible = true; break;
	case 1: itemWindow->activate(); itemWindow->visible = true; break;
	case 2: skillWindow->activate(); skillWindow->visible = true; break;
	case 3: shopWindow->activate(); shopWindow->visible = true; break;
	case 4: saveWindow->activate(); saveWindow->visible = true; break;
	//case 4: DataManager::saveGame(0); break;
	case 5: GameManager::setdownGame(); requestPopScene(); break;
	}
}
void SceneMenu::updateSprites() {
	newEquip->visible = GameManager::newEquip;
	newSkill->visible = GameManager::newSkill;
	newer->visible = GameManager::getLeader()->getLevel()<=15;

	if (actorBigFace->x < -128)
		actorBigFace->x += (-128-actorBigFace->x) / 4;
	else actorBigFace->x = -128;
	if (actorBigFace->x > -200) {
		commandGroup->getCommand(0)->x /= 2;
		commandGroup->getCommand(1)->x /= 2;
		commandGroup->getCommand(2)->x /= 2;
		commandGroup->getCommand(3)->x /= 2;
		commandGroup->getCommand(4)->x /= 2;
		commandGroup->getCommand(5)->x /= 2;
	}
	if (!anyWindowActive() && !commandGroup->isActive() && commandGroup->getCommand(4)->x == 0)
		commandGroup->activate();

	if (mapWindow->isActive() && mapWindow->x < 0) mapWindow->x += -mapWindow->x / 2;
	if (!mapWindow->isActive() && mapWindow->x > -292) mapWindow->x += (-292 - mapWindow->x) / 2;
	if (mapWindow->x <= -290) mapWindow->visible = false;

	if (itemWindow->isActive() && itemWindow->x < 0) itemWindow->x += -itemWindow->x / 2;
	if (!itemWindow->isActive() && itemWindow->x > -292) itemWindow->x += (-292 - itemWindow->x) / 2;
	if (itemWindow->x <= -290) itemWindow->visible = false;

	if (skillWindow->isActive() && skillWindow->x < 0) skillWindow->x += -skillWindow->x / 2;
	if (!skillWindow->isActive() && skillWindow->x > -292) skillWindow->x += (-292 - skillWindow->x) / 2;
	if (skillWindow->x <= -290) skillWindow->visible = false;

	if (shopWindow->isActive() && shopWindow->x < 0) shopWindow->x += -shopWindow->x / 2;
	if (!shopWindow->isActive() && shopWindow->x > -292) shopWindow->x += (-292 - shopWindow->x) / 2;
	if (shopWindow->x <= -290) shopWindow->visible = false;

	if (saveWindow->isActive() && saveWindow->x < 0) saveWindow->x += -saveWindow->x / 2;
	if (!saveWindow->isActive() && saveWindow->x > -292) saveWindow->x += (-292 - saveWindow->x) / 2;
	if (saveWindow->x <= -290) saveWindow->visible = false;
}


SceneBattle::SceneBattle(int id, int level) : Scene() {
	BattleManager::setupBattle(id, level);
	battle = BattleManager::getBattle(); 
	createSprites();
	keyCd = pause = 0;
};
void SceneBattle::update() {
	updateBattlePause();
	if (pause) return;
	Scene::update();
}
void SceneBattle::updateOthers() {
	Scene::updateOthers();
	updateBattleProcess();
}
void SceneBattle::updateBattleProcess() {
	BattleManager::update();
	updateBattleResultAni();
	updateBattleResult();
}
void SceneBattle::updateBattleResult() {
	if (!battleResult->visible) setupBattleResult(BattleManager::battleResult());
	if (battleResultWindow->popSceneRequest()) battleEnd();
}
void SceneBattle::updateBattlePause() {
	if (GameManager::inBattle && InputManager::isKeyPressWithCD(InputManager::Pause)) {
		keyCd = 5; pause = !pause;
		battlePause->visible = pause;
	}
	if (pause) {
		if (InputManager::isKeyPressWithCD(InputManager::PauseVisable)) {
			keyCd = 5; battlePause->visible = !battlePause->visible;
		}
		if (InputManager::isKeyPressWithCD(InputManager::Back)) {
			BattleManager::endBattle();
			ScreenManager::popScene();
		}
	}
}
void SceneBattle::updateBattleResultAni() {
	if (battleResult->visible) {
		battleResult->y += (13 - battleResult->y) / 4;
	}
	if (battleResultWindow->visible && battleResult->y>=0) {
		battleResultWindow->y += (40 - battleResultWindow->y) / 4;
		if (!battleResultWindow->isActive() && battleResultWindow->y <= 45) battleResultWindow->activate();
	}
}
void SceneBattle::setupBattleResult(BattleManager::BattleResult result) {
	//background = new Sprite(battle->battleField.fileName);
	//addChild(background);
	if (result == BattleManager::None) return;
	if (result == BattleManager::Win) 
		actorObject->requestMotion(SpriteBattler::MotionType::Victory, -1);

	BattleManager::endBattle();
	battleResult->setBitmap(Bitmap::getBitmap(
		result == BattleManager::BattleResult::Win ?
		"../HKK/ASCII-BattleWin.txt" :
		"../HKK/ASCII-BattleLose.txt"
	));
	battleResult->x = (SWidth - battleResult->getSpriteWidth()) / 2;
	battleResult->y = -200;
	battleResult->visible = true;

	battleResultWindow->y = 200;
	battleResultWindow->setup();
	battleResultWindow->visible = true;
}
void SceneBattle::battleEnd() {
	GameManager::gainGold(BattleManager::getGold()); 
	BattleManager::updateRecord();
	if (BattleManager::battleResult() == BattleManager::Lose)
		actorObject->setHp(1);
	if (actorObject->getLevel() <= 15) actorObject->recoverAll();
	requestPopScene();
}
void SceneBattle::createSprites() {
	Scene::createSprites();
	createBattleField();
	createBattlers();
	createBattleUI();
	createBattleResult();
	createBattlePause();
}
void SceneBattle::createBattleResult() {
	//background = new Sprite(battle->battleField.fileName);
	//addChild(background);
	battleResult = new Sprite();
	battleResult->z = 50;

	battleResultWindow = new WindowBattleResult();
	battleResultWindow->x = (SWidth - 
		battleResultWindow->getSpriteWidth()) / 2;
	battleResultWindow->z = 40;
	battleResultWindow->deactivate();
	battleResult->visible = battleResultWindow->visible = false;
	addChild(battleResult);
	addChild(battleResultWindow);
}
void SceneBattle::createBattleField() {
	//background = new Sprite(battle->battleField.fileName);
	//addChild(background);
	battleField = new SpriteMap(&battle->battleField);
	addChild(battleField);
}
void SceneBattle::createBattlers() {
	createActor(); createEnemies();
}
void SceneBattle::createBattleUI() {
	battleState = new BattleState();
	battleState->x = battleState->y = 0;
	battleState->z = 100;
	battleSlots = new BattleSlots();
	battleSlots->x = SWidth - battleSlots->getSpriteWidth();
	battleSlots->y = SHeight - battleSlots->getSpriteHeight();
	battleSlots->z = 100;
	addChild(battleState);
	addChild(battleSlots);
}
void SceneBattle::createBattlePause() {
	battlePause = new Sprite(Bitmap::getBitmap("../HKK/ASCII-Pause.txt"));
	battlePause->x = (SWidth - battlePause->getSpriteWidth())/2;
	battlePause->y = (SHeight - battlePause->getSpriteHeight())/2;
	battlePause->z = 200;
	battlePause->visible = false;
	addChild(battlePause);
}
void SceneBattle::createActor() {
	actorObject = BattleManager::getActor();
	actorSprite = new SpriteActor(actorObject);
	//addChild(actorSprite);
	battleField->addBattler(actorSprite);
}
void SceneBattle::createEnemies() {
	int cnt = BattleManager::getEnemyCount();
	for (int i = 0; i < cnt; i++) {
		addEnemy(BattleManager::getEnemy(i));
	}
}
void SceneBattle::addEnemy(ObjectEnemy* enemy) {
	SpriteEnemy* sprite = new SpriteEnemy(enemy);
	enemyObjects.push_back(enemy);
	enemySprites.push_back(sprite);
	//addChild(sprite);
	battleField->addBattler(sprite);
}