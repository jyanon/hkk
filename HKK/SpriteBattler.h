#pragma once


#include "Sprite.h"
#include "SpriteVarBar.h"
#include "TextDisplay.h"
#include "Database.h"
struct Weapon;
struct System;

class SpriteWeapon : public Sprite {
public:
	static const int typeFrame = 3;
	static const int typeXCount = 1;
	static const int typeYCount = 6;
	static const int aniSpeed = 4;

	SpriteWeapon();
	SpriteWeapon(int id);
	SpriteWeapon(Weapon* weapon);

	void setType(System::WeaponType tpe);
	void setSpeed(int spd);
	void setLoop(bool l);

	void setWeapon(Weapon* wea);

	void activate();
	void deactivate();
	bool isActive();

private:
	int getTypeRectWidth();
	int getTypeRectHeight();
	int getTypeRectX();
	int getTypeRectY();

	int getFrameRectWidth();
	int getFrameRectHeight();
	int getFrameRectX();
	int getFrameRectY();

	bool active;
protected:
	virtual void updateOthers();
	virtual void updateFrame();
	virtual void updateFrameRect();

	int type, frame, speed, cnt;
	bool loop;
	Weapon* weapon;
};

class SpriteBattler : public SpriteExtend {
public:
	static const int motionFrame = 3;
	static const int motionXCount = 2;
	static const int motionYCount = 6;
	static const int aniSpeed = 4;

	static enum MotionType {
		NoneMotion = -1,
		Idle, Walking, Defending, BeingHurt, Missing, Victory,
		Attacking1, Attacking2, Singing, Calling, UsingItem, Dead
		// atk1: �����ࡢ����࣬atk2: �����ࡢ������
	};
	static enum EffectType {
		NoneEffect = -1,
		Shine, ScreenShine, Shake, ScreenShake, SubSprite
	};

	SpriteBattler();
	SpriteBattler(ObjectBattler* battler);
	~SpriteBattler();

	virtual void setup(bool active = true);

	virtual void addAnimation(Animation ani, int ox = 0, int oy = 0, bool atGround = false);
	virtual void addAnimation(AnimationData* ani, int ox = 0, int oy = 0, bool atGround = false);
	virtual void addAnimation(Animation* ani, int ox = 0, int oy = 0, bool atGround = false);
	virtual void addAnimation(AnimationRequestData* data);

	virtual void removeAnimation(Animation* ani);

	MotionType getMotion();
	virtual void setMotion(MotionType mot);
	virtual void setupMotion(MotionType mot, bool loop);
	virtual void setupMotion(MotionType mot, int duration);
	void setSpeed(int spd);

	bool isMoved();

	int getFrameRectWidth() const;
	int getFrameRectHeight() const;

	void activate();
	void deactivate();
	bool isActive();

	int getBattlerX() const;
	int getBattlerY() const;
	int getBattlerZ() const;

private:
	int motionDuration;
	int lastZ,lastY;
	bool moved;

protected:
	int getMotionRectWidth() const;
	int getMotionRectHeight() const;
	int getMotionRectX() const;
	int getMotionRectY() const;

	int getFrameRectX() const;
	int getFrameRectY() const;

	void addPromptSprite(Damage damage);
	void removePromptSprite(vector<DamageDisplay*>::iterator dit);

	virtual void updateOthers();
	virtual void updatePosition();
	virtual void updateRequestAnimations();
	virtual void updateMirror();
	virtual void updateEffect();
	virtual void updateMotion();
	virtual void updatePrompt();
	virtual void updateHPDisplay();
	virtual void updateFrame();
	virtual void updateFrameRect();
	virtual void updateRequestPrompt();
	virtual void updatePromptSprites();

	bool active, motionLock;
	int motion, frame, speed, cnt;
	vector<DamageDisplay*> damagePrompts;
	ObjectBattler* battler;
	SpriteVarBar* varBar;
};

class SpriteActor : public SpriteBattler {
public:
	static const int WeaponOffsetX;
	static const int WeaponOffsetY;

	SpriteActor();
	SpriteActor(int id);
	SpriteActor(ObjectActor* actor);
	~SpriteActor();

	virtual void setMirror(bool mir);
	virtual void setMotion(MotionType mot);

private:
	void createWeapon();

protected:
	virtual void updateOthers();
	virtual void updateWeapon();
	virtual void updateHPDisplay() {};

	Weapon* weapon;
	ObjectActor* actor;
	SpriteWeapon* weaponSprite;
};
class SpriteEnemy : public SpriteBattler {
public:
	SpriteEnemy();
	SpriteEnemy(int id);
	SpriteEnemy(ObjectEnemy* enemy);
private:

protected:
	virtual void updateOthers();
	virtual void updateVisible();

	ObjectEnemy* enemy;
};
