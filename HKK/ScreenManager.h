#pragma once
#include "stdafx.h"

#include<stack>

#include<Windows.h>

#include "Scene.h"

#define GFontSize  0
#define GHeight  500
#define GWidth  800
#define SHeight  170
#define SWidth  640

const char EMPTY_CHAR = ' ';
const char TRANSPARENT_CHAR = 'T';

static class ScreenManager {
public:
	static void setup();
	static void clearData();
	static void createData();

	static void showScreen(HANDLE buf);

	static void render();
	static void update();

	static void popScene();
	static void clearScene();
	static void pushScene(Scene* scene);
	static void gotoScene(Scene* scene);
	static void resetToScene(Scene* scene);
	static Scene* currentScene();

	//static void setScreenCursorPosition(int x, int y);
	
	static HANDLE hOutput, hOutBuf;//����̨��Ļ���������
	static DWORD bytes;

	static const int Height;
	static const int Width;

	static char** data;// [GHeight][GWidth];

	static Sprite* screenSprite;

private:
	static bool bufferSelector;
	static void setupScreenSize();
	static void setupFontSize();
	static void setupScreenBuffers();
	static void setupScreenSprite();

	static COORD coord;

	static stack<Scene*> scenes;

	static const double FontSize;
};