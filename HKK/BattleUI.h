#pragma once
#include "Sprite.h"
#include "TextDisplay.h"
#include "SpriteVarBar.h"
#include "ObjectBattler.h"
struct ItemBase;

class IconExtend : public Icon {
public:
	IconExtend();
	IconExtend(int id);
	IconExtend(ItemBase* item);

	void setCoolDown(double cd);
	void setItem(ItemBase* item);

	ItemBase* getItem();

private:
	double coolDown;
	ItemBase* item;

protected:
	void refresh();
};

class BattleState :public Sprite {
public:
	static const Rect ScoreRect;
	static const Rect GoldRect;
	static const Rect HPRect;
	static const Rect MPRect;

	BattleState();
	~BattleState();
private:
	ObjectActor* actor;

	NumberDisplay *scoreText, *goldText;
	SpriteVarBar *hpBar, *mpBar;
protected:
	void updateOthers();
	void updateData();
	void updateTextsPosition();
};

class BattleSlots :public Sprite {
public:
	static const int SlotY;
	static const int SlotX1;
	static const int SlotX2;
	static const int SlotWidth;
	static const int SlotSpacing;

	BattleSlots();
	~BattleSlots();
private:
	int getSlotX(int index);
	int getSlotY(int index);

	ObjectActor* actor;
	vector<IconExtend*> slots;
protected:
	void updateOthers();
	void updateSlots();
	void updateSlotItems();
	void updateSlotCoolDowns();
};
