
#include "stdafx.h"

#include<cmath>
#include<fstream>

#include"Database.h"

#include"Sprite.h"
#include"ScreenManager.h"


int Sprite::getSpriteWidth() const { return frameRect.width; }
int Sprite::getSpriteHeight() const { return frameRect.height; }

Sprite::Sprite() {
	this->Sprite::Sprite(new Bitmap());
}
Sprite::Sprite(string fileName) {
	this->Sprite::Sprite(Bitmap::getBitmap(fileName));
}
Sprite::Sprite(int width, int height) {
	this->Sprite::Sprite(new Bitmap(width, height));
}
Sprite::Sprite(Bitmap* bitmap) {
	setBitmap(bitmap);
	initialiaze();
}

Rect Sprite::getObjectRect() {
	return Rect(x, y, frameRect.width, frameRect.height);
}
Rect Sprite::getFrameRect() {
	return frameRect;
}
void Sprite::initialiaze() {
	initialiazeEffect();
	initialiazePosition();
}
void Sprite::initialiazeEffect() {
	count = 0;
	shineChar = 'T'; shakePower = 0;
	shineDuration = shakeDuration = 0;
}
void Sprite::initialiazePosition() {
	x = y = z = 0; rx = x; ry = y; 
	visible = true; mirror = false;
}
void Sprite::initialiazeFrameRect() {
	width = bitmap->getWidth();
	height = bitmap->getHeight();
	setFrameRect();
}
void Sprite::setBitmap(Bitmap* bitmap) { 
	this->bitmap = bitmap;
	initialiazeFrameRect();
}
Bitmap* Sprite::getBitmap() { return bitmap; }

void Sprite::setFrameRect() {
	setFrameRect(Rect(0, 0, width, height));
}
void Sprite::setFrameRect(int x, int y, int width, int height) {
	setFrameRect(Rect(x, y, width, height));
}
void Sprite::setFrameRect(Rect rect) {
	frameRect = rect;
}

void Sprite::shake(int power, int duration, double speed, bool yShake) {
	shakePower = power; shakeDuration = duration; shakeSpeed = speed; shakeY = yShake;
}
void Sprite::shine(char c, int duration, bool transparent) {
	shineChar = c; shineDuration = duration; shineTransparent = transparent;
}
void Sprite::setMirror(bool mir) {mirror = mir;}
bool Sprite::getMirror() { return mirror; }
void Sprite::updateOthers() {
	updateEffect();
}
void Sprite::updateEffect() {
	updateShake();
	updateShine();
}
void Sprite::updateShake() {
	if (shakeDuration == 0) { rx = x; ry = y; }
	if (shakeDuration > 0) {
		processShake(shakePower,shakeDuration*shakeSpeed);
		if (--shakeDuration <= 0) { x = rx; y = ry; }
	}
	if (shakeDuration < -1){
		processShake(shakePower, --shakeDuration*shakeSpeed);
	}
}
void Sprite::processShake(int power,double dur) {
	if (shakeY)
		y = ry + power * sin(dur);
	else
		x = rx + power * sin(dur);
}
void Sprite::updateShine() {
	if (shineDuration > 0) { 
		if (--shineDuration <= 0)
			shineChar = 'T';
	}
}

vector<vector<char> > Sprite::getDisplayData() {
	//vector<vector<char> > data = bitmap.getData();
	char** data = bitmap->getData();
	int width = bitmap->getWidth();
	int height = bitmap->getHeight();

	vector<vector<char> > res;
	vector<char> line;
	for (int y = 0; y < frameRect.height; y++) {
		for (int x = 0; x < frameRect.width; x++) {
			int dx = x + frameRect.x;
			int dy = y + frameRect.y;
			if (dy >= 0 && dy < height && dx >= 0 && dx < width)
				if(shineDuration>0 || shineDuration==-1)
					line.push_back(shineTransparent || data[dy][dx]!='T'? shineChar :'T');
				else
					line.push_back(data[dy][dx]);
		}
		res.push_back(line);
		line.clear();
	}
	return res;
};

void Sprite::quickRender(char** &map, int lx, int ly) {
	if (!visible) return;

	// 渲染后面的内容
	for (int i = 0; i < backChildren.size(); i++) {
		DisplayObject* obj = backChildren[i];
		int ox = obj->x; int oy = obj->y;
		obj->quickRender(map, lx + ox, ly + oy);
	}

	// 执行渲染，首先获取Sprite上的Bitmap中的渲染数据（字符数据）
	char** data = bitmap->getData();
	int width = bitmap->getWidth();
	int height = bitmap->getHeight();

	// frameRect 为当前Sprite所占的空间矩形
	for (int y = 0; y < frameRect.height; y++) {
		for (int x = 0; x < frameRect.width; x++) {
			int dx = x + frameRect.x; int dy = y + frameRect.y;
			if (mirror) // 处理镜像
				dx = frameRect.x + frameRect.width - x;

			int sx = x + lx; int sy = y + ly;
			if (dy >= 0 && dy < height && dx >= 0 && dx < width &&
				sy >= 0 && sy < GHeight && sx >= 0 && sx < GWidth)
				// 处理透明情况，T代表透明
				if (data[dy][dx] != 'T') map[sy][sx] = 
					(shineDuration>0 || shineDuration==-1) ? shineChar : data[dy][dx];
				else if ((shineDuration > 0 || shineDuration==-1) && shineTransparent)
					map[sy][sx] = shineChar; // 处理闪烁效果
		}
	}

	// 渲染前面的内容
	for (int i = 0; i < children.size(); i++) {
		DisplayObject* obj = children[i];
		int ox = obj->x; int oy = obj->y;
		obj->quickRender(map, lx + ox, ly + oy);
	}
}
Icon::Icon() { this->Icon::Icon(-1); }
Icon::Icon(int id) :Sprite(iconSet) { setIndex(id); }

void Icon::setIndex(int index) { this->index = index; refresh(); }
int Icon::getIndex() { return index; }

int Icon::getFrameRectX() {
	return (index % System::IconSetXCount)*getFrameRectWidth();
}
int Icon::getFrameRectY() {
	return (index / System::IconSetXCount)*getFrameRectHeight();
}
int Icon::getFrameRectWidth() {
	return width / System::IconSetXCount;
}
int Icon::getFrameRectHeight() {
	return height / System::IconSetYCount;
}
void Icon::refresh() {
	if (index < 0) setFrameRect(0, 0, 0, 0);
	else setFrameRect(Rect(getFrameRectX(), getFrameRectY(), getFrameRectWidth(), getFrameRectHeight()));
}

Animation::Animation(int id) {
	this->Animation::Animation(&animations[id]);
}
// Sprite(data->fileName)
// 读取的是对应文件的Bitmap
Animation::Animation(AnimationData* data) :Sprite(data->fileName) {
	count = frame = 0;
	aData = data; // 动画数据
	makeShineData(); // 设置闪烁效果
	setSpeed(data->speed); // 设置播放速度
	width = bitmap->getWidth();
	height = bitmap->getHeight();
	updateFrameRect();
	setAutoRotate(true);
	setReverse(false);
	setLoop(false);
	play();
}
void Animation::makeShineData() {
	for (int i = 0; i < aData->maxFrame; i++) {
		shineData.push_back(NULL);
	}
	for (int i = 0; i < aData->shine.size(); i++) {
		shineData[aData->shine[i].time] = &aData->shine[i];
	}
}
void Animation::setSpeed(int spd) { speed = spd; }
int Animation::getSpeed() { return speed; }

void Animation::start() { 
	count = frame = 0; playing = true;
}
void Animation::play() { playing = true; }
void Animation::pause() { playing = false;}
void Animation::stop() {
	count = 0;frame = -1; playing = false; 
	setFrameRect(Rect(0, 0, 0, 0));
}

void Animation::setLoop(bool l) { loop = l; }
void Animation::setReverse(bool r) { reverse = r; }

void Animation::setAutoRotate(bool r) { autoRotate = r; }
bool Animation::isAutoRotate() { return autoRotate; }

bool Animation::isPlaying() { return playing; }
bool Animation::isEnded() { return frame==-1; }


int Animation::getFrameRectX() {
	// 计算每一帧的X坐标
	return (frame % aData->xCount)*getFrameRectWidth();
}
int Animation::getFrameRectY() {
	// 计算每一帧的Y坐标
	return (frame / aData->xCount)*getFrameRectHeight();
}
int Animation::getFrameRectWidth() {
	// 计算每一帧的宽度
	return width / aData->xCount;
}
int Animation::getFrameRectHeight() {
	// 计算每一帧的高度
	return height / aData->yCount;
}
int Animation::duration() {
	// 总时长
	return aData->maxFrame * speed;
}
void Animation::updateOthers() {
	Sprite::updateOthers();
	updateAnimationFrame();
	updateShineData();
}
void Animation::updateAnimationFrame() {
	// 播放处理
	if (playing) {
		frame = animationFunction(++count);
		if (frame >= aData->maxFrame)
			if (loop)
				frame = animationFunction(count = reverse ? 1-duration() : 0);
			else stop();
		// 实时更新 frameRect，改变切图范围，从而实现帧动画
			if (frame >= 0) updateFrameRect();
	}
}
void Animation::updateShineData() {
	if (lastFrame != frame && frame>=0) {
		lastFrame = frame;
		if (shineData[frame] != NULL) {
			ShineData* dt = shineData[frame];
			if(!dt->screen)
				dynamic_cast<Sprite*>(parent)->shine(dt->c, dt->last*speed);
			else
				ScreenManager::currentScene()->shine(dt->c, dt->last*speed);
		}
	}
}
void Animation::updateFrameRect() {
	// 设置要渲染的在Bitmap中的矩形位置（即切图）
	setFrameRect(Rect(getFrameRectX(), getFrameRectY(), getFrameRectWidth(), getFrameRectHeight()));
}
int Animation::animationFunction(int c) {
	return abs(c) / speed;
}

SpriteExtend::SpriteExtend():Sprite(){}
SpriteExtend::SpriteExtend(string fileName) : Sprite(fileName) {}
SpriteExtend::SpriteExtend(int width, int height) : Sprite(width, height) {}
SpriteExtend::SpriteExtend(Bitmap* bitmap) : Sprite(bitmap) {}

void SpriteExtend::addAnimation(AnimationData* ani) { addAnimation(new Animation(ani)); }
void SpriteExtend::addAnimation(Animation ani) { addAnimation(&ani); }
void SpriteExtend::addAnimation(Animation* ani) {
	animations.push_back(ani); addChild(ani);
}

void SpriteExtend::removeAnimation(Animation ani) { removeAnimation(&ani); }
void SpriteExtend::removeAnimation(Animation* ani) {
	vector<Animation*>::iterator pos;
	pos = find(animations.begin(), animations.end(), ani);
	if (pos != animations.end()) animations.erase(pos);
	removeChild(ani);
}
Animation* SpriteExtend::getAnimation(int id) {return animations[id];}

void SpriteExtend::setMirror(bool mir) {
	Sprite::setMirror(mir);
	for (int i = 0; i < animations.size(); i++) {
		Animation* ani = animations[i];
		if (!ani->isAutoRotate()) continue;
		if (mir != ani->getMirror()) {
			ani->setMirror(mir);
			ani->x = getSpriteWidth() - ani->getFrameRectWidth() - ani->x;
		}
	}
}
void SpriteExtend::updateOthers() {
	Sprite::updateOthers();
	updateAnimations();
}
void SpriteExtend::updateAnimations() {
	vector<Animation*>::iterator pos;
	for (int i = animations.size() - 1; i >= 0;i--){
		pos = animations.begin() + i;
		if ((*pos)->isEnded()) {
			delete *pos;
			removeAnimation(*pos);
			//animations.erase(pos);
		}
	}
}