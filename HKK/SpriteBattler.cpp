
#include "stdafx.h"

#include "SpriteBattler.h"
#include "ObjectBattler.h"
#include "TextDisplay.h"
#include "ScreenManager.h"

const int SpriteActor::WeaponOffsetX = -20;
const int SpriteActor::WeaponOffsetY = 0;

SpriteWeapon::SpriteWeapon() : Sprite(weaponBitmap) {
	setSpeed(aniSpeed);
	setLoop(false);
	deactivate();
}
SpriteWeapon::SpriteWeapon(int id) {
	this->SpriteWeapon::SpriteWeapon(&weapons[id]);
}
SpriteWeapon::SpriteWeapon(Weapon* weapon) : Sprite(weaponBitmap) {
	setSpeed(aniSpeed);
	setWeapon(weapon);
	setLoop(false);
	deactivate();
}
void SpriteWeapon::setLoop(bool l) { loop = l; }
void SpriteWeapon::setSpeed(int spd) { speed = spd; }
void SpriteWeapon::setType(System::WeaponType tpe) { 
	type = tpe; cnt = frame = 0; 
	updateFrameRect();
}
void SpriteWeapon::setWeapon(Weapon* wea) { 
	weapon = wea; setType(weapon->wType); 
}

int SpriteWeapon::getTypeRectX() {
	return (type / typeYCount)*getFrameRectWidth();
}
int SpriteWeapon::getTypeRectY() {
	return (type % typeYCount)*getFrameRectHeight();
}
int SpriteWeapon::getTypeRectWidth() {
	return width / typeXCount;
}
int SpriteWeapon::getTypeRectHeight() {
	return height / typeYCount;
}

int SpriteWeapon::getFrameRectX() {
	return getTypeRectX() + frame*getFrameRectWidth();
}
int SpriteWeapon::getFrameRectY() {
	return getTypeRectY();
}
int SpriteWeapon::getFrameRectWidth() {
	return getTypeRectWidth() / typeFrame;
}
int SpriteWeapon::getFrameRectHeight() {
	return getTypeRectHeight();
}
void SpriteWeapon::activate() {
	visible = active = true;
}
void SpriteWeapon::deactivate() {
	visible = active = false;
	cnt = frame = 0;
	updateFrameRect();
}
bool SpriteWeapon::isActive() {
	return active;
}

void SpriteWeapon::updateOthers() {
	Sprite::updateOthers();
	updateFrame();
}
void SpriteWeapon::updateFrame() {
	if (active) {
		if (++cnt >= speed) {
			cnt = 0; frame++;
			if (!loop && frame >= typeFrame) deactivate();
			else {
				frame %= typeFrame;
				updateFrameRect();
			}
		}
	}
}
void SpriteWeapon::updateFrameRect() {
	setFrameRect(Rect(getFrameRectX(), getFrameRectY(), getFrameRectWidth(), getFrameRectHeight()));
}


SpriteBattler::SpriteBattler() : SpriteExtend() {
	this->battler = NULL; setup(false);
}
SpriteBattler::SpriteBattler(ObjectBattler* battler) : SpriteExtend(battler->getFileName()) {
	this->battler = battler; setup();
}
SpriteBattler::~SpriteBattler() {
	delete varBar;
}
void SpriteBattler::setup(bool active) {
	varBar = new SpriteVarBar(hpDisplayBar, hpDisplayBg);
	setMotion(Idle); setSpeed(aniSpeed);
	this->active = active; lastZ = z;
	addChild(varBar);
	varBar->x = (getFrameRectWidth() - varBar->getWidth()) / 2;
	varBar->y = -varBar->getHeight() - 4;
}

void SpriteBattler::addAnimation(AnimationData* ani, int ox, int oy, bool atGround) {
	addAnimation(new Animation(ani),ox, oy, atGround);
}
void SpriteBattler::addAnimation(Animation ani, int ox, int oy, bool atGround) {
	addAnimation(&ani, ox, oy, atGround);
}
void SpriteBattler::addAnimation(AnimationRequestData* data) {
	addAnimation(data->ani, data->ox, data->oy, data->atGround);
}
void SpriteBattler::addAnimation(Animation* ani, int ox, int oy, bool atGround) {
	int ax = ox + (getSpriteWidth() - ani->getFrameRectWidth()) / 2;
	int ay = oy + (getSpriteHeight() - ani->getFrameRectHeight()) / 2;
	animations.push_back(ani);
	if (atGround) {
		ani->x = x + ax; ani->y = y + ay;
		ani->setAutoRotate(false);
		parent->addChild(ani);
	}else {
		ani->x = ax; ani->y = ay;
		addChild(ani);
	}
}void SpriteBattler::removeAnimation(Animation* ani) {
	vector<Animation*>::iterator pos;
	pos = find(animations.begin(), animations.end(), ani);
	if (pos != animations.end()) animations.erase(pos);
	parent->removeChild(ani);
	removeChild(ani);
}

SpriteBattler::MotionType SpriteBattler::getMotion() {
	return MotionType(motion);
}
void SpriteBattler::setMotion(MotionType mot) {
	if (motion == mot) return;
	motion = mot; cnt = frame = 0; 
	motionDuration = -1; motionLock = false;
	updateFrameRect();
}
void SpriteBattler::setupMotion(MotionType mot, bool loop = false) {
	setMotion(mot); motionLock = true;
	motionDuration = loop ? -1 : motionFrame;
}
void SpriteBattler::setupMotion(MotionType mot, int duration) {
	setMotion(mot); motionLock = true;
	motionDuration = duration;
}
void SpriteBattler::setSpeed(int spd) { speed = spd; }


int SpriteBattler::getBattlerX() const { return battler->getX(); }
int SpriteBattler::getBattlerY() const { return battler->getY(); }
int SpriteBattler::getBattlerZ() const { return battler->getZ(); }


void SpriteBattler::activate() {
	visible = active = true;
}
void SpriteBattler::deactivate() {
	visible = active = false;
}
bool SpriteBattler::isActive() {
	return active;
}
bool SpriteBattler::isMoved() { return moved; }

int SpriteBattler::getMotionRectX() const {
	return (motion / motionYCount)*getMotionRectWidth();
}
int SpriteBattler::getMotionRectY() const {
	return (motion % motionYCount)*getMotionRectHeight();
}
int SpriteBattler::getMotionRectWidth() const {
	return width / motionXCount;
}
int SpriteBattler::getMotionRectHeight() const {
	return height / motionYCount;
}

int SpriteBattler::getFrameRectX() const {
	int f = frame;
	if (motion == Walking && f == motionFrame) f = motionFrame>>1;
	return getMotionRectX() + f*getFrameRectWidth();
}
int SpriteBattler::getFrameRectY() const {
	return getMotionRectY();
}
int SpriteBattler::getFrameRectWidth() const {
	return getMotionRectWidth() / motionFrame;
}
int SpriteBattler::getFrameRectHeight() const {
	return getMotionRectHeight();
}


void SpriteBattler::addPromptSprite(Damage damage) {
	DamageDisplay* dis = new DamageDisplay(damage);
	dis->x = (getFrameRectWidth() - dis->getWidth()) / 2;
	dis->y = -dis->getHeight()+2;
	damagePrompts.push_back(dis);
	addChild(dis);
}
void SpriteBattler::removePromptSprite(vector<DamageDisplay*>::iterator dit) {
	removeChild(*dit);
	delete *dit;	
	damagePrompts.erase(dit);
}

// SpriteBattler 为战斗者的精灵
// 与 ObjectBattler 相互使用，遵循数据-视图分离原则，数据储存在ObjectBattler上，视图由SpriteBattler处理
void SpriteBattler::updateOthers() {
	SpriteExtend::updateOthers();
	updatePosition();
	updateRequestAnimations();
	updateEffect();
	updateMotion();
	updateMirror();
	updatePrompt();
	updateHPDisplay();
	updateFrame();
}
void SpriteBattler::updatePosition() {
	x = battler->screenX() - getSpriteWidth() / 2;
	y = battler->screenY() - getSpriteHeight();
	z = battler->screenZ();
	if (lastZ != z || lastY != y) {
		moved = true; lastZ = z; lastY = y;
	}else moved = false;
}	

void SpriteBattler::updateRequestAnimations() {
	vector<AnimationRequestData*> rAni = battler->animationsRequested();
	if (!rAni.empty()) {
		for (int i = 0; i < rAni.size(); i++)
			addAnimation(rAni[i]);
		battler->clearRequestedAnimations();
	}
}
void SpriteBattler::updateEffect() {
	Sprite::updateEffect();
	SpriteBattler::EffectType rEff = battler->effectRequested();
	if (rEff != EffectType::NoneEffect) {
		vector<int> params = battler->effectParamsRequested();
		switch (rEff) {
		case EffectType::Shake: shake(params[0], params[1]); break;
		case EffectType::Shine: shine(params[0], params[1], params[2]); break;
		case EffectType::ScreenShine:ScreenManager::currentScene()->shine(params[0], params[1]); break;
		case EffectType::ScreenShake:ScreenManager::currentScene()->shake(params[0], params[1]); break;
		}
		battler->clearRequestedEffect();
	}
}
void SpriteBattler::updateMotion() {
	active = !battler->isFreeze();
	SpriteBattler::MotionType rMot = battler->motionRequested();
	if (rMot != MotionType::NoneMotion) {
		setupMotion(rMot, battler->motionDurationRequested());
		battler->clearRequestedMotion();
	}
	if (motionLock) return;
	if (battler->isDead()) setupMotion(Dead, true);
	else if (battler->isDefending()) setMotion(Defending);
	else if (battler->isMoving()) setMotion(Walking);
	else setMotion(Idle);
}
void SpriteBattler::updatePrompt() {
	updateRequestPrompt();
	updatePromptSprites();
}
void SpriteBattler::updateRequestPrompt() {
	Damage rProm = battler->promptRequested();
	if (rProm.getObject() != NULL) {
		addPromptSprite(rProm);
		battler->clearRequestedPrompt();
	}
}
void SpriteBattler::updatePromptSprites() {
	vector<DamageDisplay*>::iterator dit = damagePrompts.end();
	for (int i = damagePrompts.size() - 1; i >= 0; i--) {
		dit = damagePrompts.begin() + i;
		if (!(*dit)->visible) 
			removePromptSprite(dit);
	}
}
void SpriteBattler::updateMirror() {
	setMirror(battler->getDirection());
}
void SpriteBattler::updateHPDisplay() {
	varBar->setRate(battler->getHpRate());
	varBar->visible = !varBar->displayEnd();
}
void SpriteBattler::updateFrame() {
	if (active) {
		if (++cnt >= speed) {
			cnt = 0;
			if (motionDuration > 0) motionDuration--;
			if (motionDuration == 0) {
				battler->setIdle(); setMotion(Idle);
			}
			frame = (++frame) % (motion != 1 ? motionFrame : motionFrame + 1);
			updateFrameRect();
		}
	}
}
void SpriteBattler::updateFrameRect() {
	setFrameRect(Rect(getFrameRectX(), getFrameRectY(), getFrameRectWidth(), getFrameRectHeight()));
}

SpriteActor::SpriteActor() { this->actor = NULL; }
SpriteActor::SpriteActor(int id) {
	this->SpriteActor::SpriteActor(GameManager::getActor(id));
}
SpriteActor::SpriteActor(ObjectActor* actor) : SpriteBattler(actor) {
	this->actor = actor;
	createWeapon();
}
SpriteActor::~SpriteActor() {
	delete weaponSprite;
}
void SpriteActor::createWeapon() {
	weaponSprite = new SpriteWeapon();
	addBackChild(weaponSprite);
}
void SpriteActor::setMotion(MotionType mot) { 
	SpriteBattler::setMotion(mot);
	//if (motion == Attacking1) weaponSprite->activate();
	if (motion == Attacking2) weaponSprite->activate();
}
void SpriteActor::setMirror(bool mir) {
	SpriteBattler::setMirror(mir);
	weaponSprite->setMirror(mir);
	weaponSprite->x = (getFrameRectWidth() - weaponSprite->getSpriteWidth()) / 2 + 
		WeaponOffsetX * (mir ? -1 : 1);
}

void SpriteActor::updateOthers() {
	SpriteBattler::updateOthers();
	updateWeapon();
}
void SpriteActor::updateWeapon() {
	Weapon* wea = actor->getWeapon();
	if (wea != NULL && weapon != wea) {
		weapon = wea;
		weaponSprite->setWeapon(weapon);
		weaponSprite->x = (getFrameRectWidth() - weaponSprite->getSpriteWidth()) / 2 + WeaponOffsetX;
		weaponSprite->y = (getFrameRectHeight() - weaponSprite->getSpriteHeight()) / 2 + WeaponOffsetY;
	}
}

SpriteEnemy::SpriteEnemy() { this->enemy = NULL; }
SpriteEnemy::SpriteEnemy(int id) {
	this->SpriteEnemy::SpriteEnemy(BattleManager::getEnemy(id));
}
SpriteEnemy::SpriteEnemy(ObjectEnemy* enemy) : SpriteBattler(enemy) {
	this->enemy = enemy;
}
void SpriteEnemy::updateOthers() {
	SpriteBattler::updateOthers();
	//updateVisible();
}
void SpriteEnemy::updateVisible() {
	visible = !battler->isDead();
}