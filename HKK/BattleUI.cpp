#include"stdafx.h"
#include"BattleUI.h"
#include"Database.h"
#include"BattleManager.h"

const Rect BattleState::ScoreRect = Rect(51, -1, 116, 6);
const Rect BattleState::GoldRect = Rect(59, 24, 66, 6);
const Rect BattleState::HPRect = Rect(30, 10, 107, 6);
const Rect BattleState::MPRect = Rect(30, 18, 107, 6);

const int BattleSlots::SlotY = 6;
const int BattleSlots::SlotX1 = 21;
const int BattleSlots::SlotX2 = 195;
const int BattleSlots::SlotWidth = 24;
const int BattleSlots::SlotSpacing = 7;

IconExtend::IconExtend() : Icon() {
	setItem(NULL); setCoolDown(0);
}
IconExtend::IconExtend(ItemBase* item) :Icon(item->iconId){
	setItem(item); setCoolDown(0);
}
IconExtend::IconExtend(int id) : Icon(id) {
	setItem(NULL); setCoolDown(0); 
}

void IconExtend::setItem(ItemBase* item) { 
	this->item = item; setIndex(item == NULL ? -1 : item->iconId);
}
ItemBase* IconExtend::getItem() { return item; }

void IconExtend::setCoolDown(double cd) { coolDown = cd; refresh(); }

void IconExtend::refresh() {
	if (index < 0) setFrameRect(0, 0, 0, 0);
	else setFrameRect(Rect(getFrameRectX(), getFrameRectY(), 
		getFrameRectWidth(), getFrameRectHeight()*(1 - coolDown)));
}


BattleState::BattleState() : Sprite(battleStateUI) {
	actor = BattleManager::getActor();
	scoreText = new NumberDisplay(defaultFontFaceSmall);
	scoreText->x = ScoreRect.x; scoreText->y = ScoreRect.y;
	goldText = new NumberDisplay(defaultFontFaceSmall);
	goldText->x = GoldRect.x; goldText->y = GoldRect.y;
	//levelText = new NumberDisplay(defaultFontFaceSmall);
	//levelText->x = LevelRect.x; levelText->y = LevelRect.y;
	hpBar = new SpriteVarBar(battleHpBarBitmap,new Bitmap(HPRect.width, HPRect.height));
	hpBar->x = HPRect.x; hpBar->y = HPRect.y;
	mpBar = new SpriteVarBar(battleMpBarBitmap,new Bitmap(MPRect.width, MPRect.height));
	mpBar->x = MPRect.x; mpBar->y = MPRect.y;
	hpBar->visible = mpBar->visible = true;
	addChild(scoreText); addChild(goldText);
	addChild(hpBar); addChild(mpBar);
	updateData();
}
BattleState::~BattleState() {
	delete hpBar; delete mpBar;
	delete scoreText; delete goldText;
}

void BattleState::updateOthers() {
	Sprite::updateOthers();
	updateData();
}
void BattleState::updateData() {
	scoreText->setNum(BattleManager::getScore());
	goldText->setNum(GameManager::getGold());
	hpBar->setRate(actor->getHpRate());
	mpBar->setRate(actor->getMpRate());
	updateTextsPosition();
}
void BattleState::updateTextsPosition() {
	scoreText->x = ScoreRect.x + ScoreRect.width - scoreText->getWidth();
	scoreText->y = ScoreRect.y + (ScoreRect.height - scoreText->getHeight()) / 2;
	goldText->x = GoldRect.x + GoldRect.width - goldText->getWidth();
	goldText->y = GoldRect.y + (GoldRect.height - goldText->getHeight()) / 2;
}

BattleSlots::BattleSlots() : Sprite(battleSlotsUI) {
	actor = BattleManager::getActor();
	for (int i = 0; i < GameManager::SkillSlotCount + GameManager::ItemSlotCount; i++) {
		IconExtend* ico = new IconExtend();
		ico->x = getSlotX(i);
		ico->y = getSlotY(i);
		slots.push_back(ico);
		addChild(ico);
	}
}
BattleSlots::~BattleSlots() {
	for (int i = 0; i < slots.size(); i++) delete slots[i];
	slots.clear();
}
int BattleSlots::getSlotX(int index) {
	int baseX = (index >= GameManager::SkillSlotCount ? SlotX2 : SlotX1);
	if (index >= GameManager::SkillSlotCount) index -= GameManager::SkillSlotCount;
	return baseX + (SlotWidth+SlotSpacing)*index;
}
int BattleSlots::getSlotY(int index) {
	return SlotY;
}
void BattleSlots::updateOthers() {
	Sprite::updateOthers();
	updateSlots();
}
void BattleSlots::updateSlots() {
	updateSlotItems();
	updateSlotCoolDowns();
}
void BattleSlots::updateSlotItems() {
	for (int i = 0; i < GameManager::SkillSlotCount; i++) {
		slots[i]->setItem(GameManager::getSkillSlot(i));
	}
	for (int i = GameManager::SkillSlotCount; 
		i < GameManager::SkillSlotCount + GameManager::ItemSlotCount; i++) {
		slots[i]->setItem(GameManager::getItemSlot(i- GameManager::SkillSlotCount));
	}
}
void BattleSlots::updateSlotCoolDowns() {
	for (int i = 0; i < GameManager::SkillSlotCount + GameManager::ItemSlotCount; i++) {
		UsableItem* item = static_cast<UsableItem*>(slots[i]->getItem());
		slots[i]->setCoolDown(actor->getItemCoolDownRate(item));
	}
}