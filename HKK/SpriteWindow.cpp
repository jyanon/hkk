
#include "stdafx.h"
#include "SpriteWindow.h"
#include "Database.h"
#include "GameManager.h"
#include "ObjectBattler.h"
#include "ScreenManager.h"

const int WindowMap::MapPos[System::MaxMapCount][2] = {
	{ 4,20 },{ 204,20 },{ 4,95 },{ 204,95 }
};
const string WindowMap::MapPicFiles[System::MaxMapCount][2] = {
	{ "../HKK/ASCII-Battle1_small.txt","../HKK/ASCII-Battle1_name.txt" },
	{ "../HKK/ASCII-Battle2_small.txt","../HKK/ASCII-Battle2_name.txt" },
	{ "../HKK/ASCII-Battle3_small.txt","../HKK/ASCII-Battle3_name.txt" },
	{ "../HKK/ASCII-Battle4_small.txt","../HKK/ASCII-Battle4_name.txt" }
};

const int WindowMapLevel::MapPicPos[2] = { 15,28 };
const int WindowMapLevel::LevelSelectPos[2] = { 10,120 };
const int WindowMapLevel::DifficultyPos[2] = { 300,40 };
const int WindowMapLevel::HighRecordPos[2] = { 300,51 };
const int WindowMapLevel::JudgePos[2] = { 300,62 };
const string WindowMapLevel::JudgeString[8] = { "SSS","SS","S","A","B","C","D","--" };

const int WindowItem::EquipPos[System::EquipCount][2] = {
	{ 0,110 },{ 55,33 },{ 55,82 },{ 55,141 },{ 4,21 },{ 4,41 },{ 2,146 }
};
const int WindowItem::ParamsPos[System::BaseParamCount - 2][2] = {
	{ 164,142 },{ 164,149 },{ 164,157 },{ 284,142 },{ 284,149 },{ 284,157 }
};
const int WindowItem::ShortCutPos[2] = { 366,60 };
const int WindowItem::ActorNamePos[2] = { 166,134 };
const int WindowItem::GoldTextPos[2] = { 292,125 };
const int WindowItem::PageTextPos[2] = { 212,125 };

const int WindowItem::ItemBlockPos[2] = { 148, 27 };
const Rect WindowItem::HPRect = Rect(234, 0, 128, 8);
const Rect WindowItem::MPRect = Rect(234, 10, 128, 8);

const int WindowSkill::ShortCutPos[2] = { 317,43 };
const int WindowSkill::PageTextPos[2] = { 13,157 };
const int WindowSkill::SkillBlockPos[2] = { 13, 27 };
const Rect WindowSkill::HPRect = Rect(234, 0, 128, 8);
const Rect WindowSkill::MPRect = Rect(234, 10, 128, 8);


const int WindowShop::GoldTextPos[2] = { 94,156 };
const int WindowShop::PriceTextPos[2] = { 280,39 };
const int WindowShop::HoldTextPos[2] = { 280,51 };
const int WindowShop::CountTextPos[2] = { 280,63 };
const int WindowShop::SumTextPos[2] = { 280,75 };
const int WindowShop::PageTextPos[2] = { 134,24 };
const int WindowShop::GoodBlockPos[2] = { 9,35 };
const int WindowShop::AlertPos[2] = { 128,56 };
const int WindowShop::CatPosX = 268;


const int WindowFile::FilePos[DataManager::MaxSaveFileCount][2] = {
	{ 7,42 },{ 117,42 },{ 7,97 },{ 117,97 }
};
const int WindowFile::LeaderNamePos[2] = { 228,51 };
const int WindowFile::LeaderLevelPos[2] = { 228,75 };
const int WindowFile::GoldPos[2] = { 228,99 };
const int WindowFile::DateTimePos[2] = { 228,123 };

const int WindowBattleResult::TimeNeedPos[2] = { 76,17 };
const int WindowBattleResult::HurtPointPos[2] = { 76,33 };
const int WindowBattleResult::DifficultPos[2] = { 76,49 };
const int WindowBattleResult::RecoverPointPos[2] = { 261,17 };
const int WindowBattleResult::BeHurtPointPos[2] = { 261,33 };
const int WindowBattleResult::FinalScorePos[2] = { 261,49 };
const int WindowBattleResult::GoldGainPos[2] = { 121,70 };
const int WindowBattleResult::JudgePos[2] = { 273,41 };
const string WindowBattleResult::JudgeFileName[7] = {
	"../HKK/ASCII-BattleJudge_sss.txt",
	"../HKK/ASCII-BattleJudge_ss.txt",
	"../HKK/ASCII-BattleJudge_s.txt",
	"../HKK/ASCII-BattleJudge_a.txt",
	"../HKK/ASCII-BattleJudge_b.txt",
	"../HKK/ASCII-BattleJudge_c.txt",
	"../HKK/ASCII-BattleJudge_d.txt"
};
/*
SpriteWindow::SpriteWindow(Bitmap* background, Bitmap* itembg, int aniId, int firstSelect) {
	this->SpriteWindow::SpriteWindow(background, itembg, NULL, &animations[aniId], firstSelect);
}
SpriteWindow::SpriteWindow(Bitmap* background, Bitmap* itembg, Bitmap* fontFace, int aniId, int firstSelect) {
	this->SpriteWindow::SpriteWindow(background, itembg, fontFace, &animations[aniId], firstSelect);
}
SpriteWindow::SpriteWindow(Bitmap* background, Bitmap* itembg, AnimationData* data, int firstSelect) {
	this->SpriteWindow::SpriteWindow(background, itembg, NULL, data, firstSelect);
}
SpriteWindow::SpriteWindow(Bitmap* background, Bitmap* itembg, Bitmap* fontFace, AnimationData* data, int firstSelect) :
	Sprite(background) {
	itemsGroup = new SpriteCommandGroup(itembg, fontFace, data, firstSelect);
}
SpriteWindow::~SpriteWindow() {
	clearAllItems();
	delete itemsGroup;
}

int SpriteWindow::maxItems() { return data.size(); }
int SpriteWindow::maxRow() { return maxItems(); }
int SpriteWindow::maxCol() { return 1; }

int SpriteWindow::pageCol() { return maxItems(); }
int SpriteWindow::pageRow() { return 1; }

int SpriteWindow::xSpacing() { return 4; }
int SpriteWindow::ySpacing() { return 2; }

Rect SpriteWindow::itemRect(int index) {

}

void SpriteWindow::refresh() {
	clearAllItems();
	makeItemsList();
	drawAllItems();
}
void SpriteWindow::clearAllItems() {
	itemsGroup->clearCommands();
}
void SpriteWindow::makeItemsList() { clearItemsList(); }
void SpriteWindow::clearItemsList() { data.clear(); }

void SpriteWindow::drawItem(int index, int displayIndex) {

}
void SpriteWindow::clearItem(int index) {

}
void SpriteWindow::drawAllItems() {
	for (int i = 0; i < ; i++) {
		clearItem(i);
		drawItem()
	}
}
*/
SpriteWindow::SpriteWindow() {}
SpriteWindow::SpriteWindow(Bitmap* background) : Sprite(background) {
	initialize(); setup(); refresh();
}
void SpriteWindow::initialize() {}
void SpriteWindow::refresh() {}
void SpriteWindow::setup() {}

bool SpriteWindow::isActive() { return active; }

void SpriteWindow::activate() {
	refresh(); active = true; keyCd = 10;
}
void SpriteWindow::deactivate() {
	active = false;
}

void SpriteWindow::processOK() {}
void SpriteWindow::processBack() {
	deactivate();
}

void SpriteWindow::updateOthers() {
	Sprite::updateOthers();
	updateControl();
}
void SpriteWindow::updateControl() {
	if (--keyCd > 0 || !isActive()) return;
	if (InputManager::isKeyPressWithCD(InputManager::OK)) {
		processOK(); //keyCd = 5;
	}
	if (InputManager::isKeyPressWithCD(InputManager::Back)) {
		processBack(); //keyCd = 5;
	}
}



WindowMap::WindowMap() :SpriteWindow(menuMapBackground) {
	initialize(); setup(); refresh();
}
WindowMap::~WindowMap() {
	delete mapNames;
	delete levelSelectWindow;
	for (int i = 0; i < System::MaxMapCount; i++)
		delete mapCommands[i];
}
void WindowMap::initialize() {
	SpriteWindow::initialize();
	initializeMapCommands();
	initializeMapLevelSelect();
}
void WindowMap::setup() {
	SpriteWindow::setup();
	index = 0;
	select(0);
	deactivate();
}
void WindowMap::initializeMapCommands() {
	for (int i = 0; i < System::MaxMapCount; i++) {
		mapCommands[i] = new SpriteCommand(Bitmap::getBitmap(MapPicFiles[i][0]), 14, new Bitmap(0, 0));
		mapCommands[i]->x = MapPos[i][0]; mapCommands[i]->y = MapPos[i][1];
		addChild(mapCommands[i]);
	}
	mapNames = new Sprite();
	mapNames->z = 50;
	addChild(mapNames);
}
void WindowMap::initializeMapLevelSelect() {
	levelSelectWindow = new WindowMapLevel();
	levelSelectWindow->z = 10000;
	addChild(levelSelectWindow);
}
void WindowMap::activate() {
	SpriteWindow::activate(); mapCommands[index]->activate();
}
void WindowMap::deactivate() {
	SpriteWindow::deactivate(); mapCommands[index]->deactivate();
}
void WindowMap::select(int id) {
	deselect(); index = id; 
	if (System::MaxMapCount > id) 
		mapCommands[id]->select();
	updateNameDisplay();
}
void WindowMap::deselect() { deselect(index); }
void WindowMap::deselect(int id) { if (System::MaxMapCount > id) mapCommands[id]->deselect(); }

void WindowMap::scrollUp() {
	int id = (index - 2 + System::MaxMapCount) % System::MaxMapCount;
	select(id);
}
void WindowMap::scrollDown() {
	int id = (index + 2) % System::MaxMapCount;
	select(id);
}
void WindowMap::scrollLeft() {
	int id = (index - 1 + System::MaxMapCount) % System::MaxMapCount;
	select(id);
}
void WindowMap::scrollRight() {
	int id = (index + 1) % System::MaxMapCount;
	select(id);
}

void WindowMap::processOK() {
	SpriteWindow::processOK();
	levelSelectWindow->activate();
	levelSelectWindow->setMap(index);
	//ScreenManager::pushScene(new SceneBattle(index));
}
void WindowMap::updateNameDisplay() {
	mapNames->setBitmap(Bitmap::getBitmap(MapPicFiles[index][1]));
	mapNames->x = mapCommands[index]->x;
	mapNames->y = mapCommands[index]->y +
		mapCommands[index]->getSpriteHeight() -
		mapNames->getSpriteHeight();
}

void WindowMap::updateOthers() {
	SpriteWindow::updateOthers();
	updateControl();
}
void WindowMap::updateControl() {
	SpriteWindow::updateControl();
	if (levelSelectWindow->isActive()) return;
	if (keyCd > 0 || !isActive()) return;
	int ud = InputManager::UD();
	int lr = InputManager::LR();
	if (ud != 0) { keyCd = 5;
		ud > 0 ? scrollDown() : scrollUp();
	}
	if (lr != 0) { keyCd = 5;
		lr > 0 ? scrollRight() : scrollLeft();
	}
}



WindowMapLevel::WindowMapLevel() :SpriteWindow(menuMapLevelBackground) {
	initialize(); setup(); setMap(NULL); //refresh();
}
WindowMapLevel::~WindowMapLevel() {
	delete mapPic, mapNames, judgeRecord;
	delete difficulty, scoreRecord;
	delete levelCommandGroup;
}
void WindowMapLevel::initialize() {
	SpriteWindow::initialize();
	initializeMapInfo();
	initializeLevelCommands();
	initializeMapPic();
}
void WindowMapLevel::setup() {
	SpriteWindow::setup();
	deactivate();
}
void WindowMapLevel::setMap(int i) { 
	setMap(&battles[i]); 
}
void WindowMapLevel::setMap(Battle* battle) { 
	if (map != battle) { map = battle; refresh();}
}

void WindowMapLevel::initializeMapInfo() {
	difficulty = new TextDisplay(defaultFontFaceSmall);
	difficulty->x = DifficultyPos[0];
	difficulty->y = DifficultyPos[1];

	scoreRecord = new NumberDisplay(defaultFontFaceSmall);
	scoreRecord->x = HighRecordPos[0];
	scoreRecord->y = HighRecordPos[1];

	judgeRecord = new TextDisplay(defaultFontFaceSmall);
	judgeRecord->x = JudgePos[0];
	judgeRecord->y = JudgePos[1];

	addChild(difficulty);
	addChild(scoreRecord);
	addChild(judgeRecord);
}
void WindowMapLevel::initializeLevelCommands() {
	levelCommandGroup = new SpriteCommandGroup(menuCommandBg, 2, 0);
	SpriteCommand* cmd;

	levelCommandGroup->z = 50;
	levelCommandGroup->x = LevelSelectPos[0];
	levelCommandGroup->y = LevelSelectPos[1];

	cmd = levelCommandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-LevelCommand1.txt"));
	cmd->x = 0; cmd->y = 0;
	cmd = levelCommandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-LevelCommand2.txt"));
	cmd->x = 0; cmd->y = 20;
	cmd = levelCommandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-LevelCommand3.txt"));
	cmd->x = 256; cmd->y = 0;
	cmd = levelCommandGroup->addCommand(Bitmap::getBitmap("../HKK/ASCII-LevelCommand4.txt"));
	cmd->x = 256; cmd->y = 20;

	levelCommandGroup->setActiveShake(false);
	levelCommandGroup->setActiveShineChar('*');
	levelCommandGroup->deactivate();

	addChild(levelCommandGroup);
}
void WindowMapLevel::initializeMapPic() {
	mapPic = new Sprite();
	mapPic->x = MapPicPos[0];
	mapPic->y = MapPicPos[1];

	mapNames = new Sprite();

	addChild(mapPic);
	addChild(mapNames);
}
void WindowMapLevel::activate() {
	SpriteWindow::activate(); levelCommandGroup->activate(); visible = true;
}
void WindowMapLevel::deactivate() {
	SpriteWindow::deactivate(); levelCommandGroup->deactivate(); visible = false;
}

void WindowMapLevel::refresh() {
	SpriteWindow::refresh();
	refreshMapInfo();
	refreshMapPic();
}
void WindowMapLevel::refreshMapInfo() {
	if (map == NULL) {
		difficulty->setText("");
		judgeRecord->setText("");
		scoreRecord->setNum(-1);
	} else {
		char diff[50];
		int mid = map->index, lid = levelCommandGroup->getIndex();
		int judge = GameManager::getBattleScoreJudge(mid, lid);
		int score = GameManager::getBattleScoreRecord(mid, lid);
		if (judge == -1) judge = 7;
		sprintf_s(diff, "%.2f", map->difficluty);
		difficulty->setText(diff);
		judgeRecord->setText(JudgeString[judge]);
		scoreRecord->setNum(score);
	}
}

void WindowMapLevel::refreshMapPic() {
	int index = map->index;
	mapPic->setBitmap(Bitmap::getBitmap(WindowMap::MapPicFiles[index][0]));

	mapNames->setBitmap(Bitmap::getBitmap(WindowMap::MapPicFiles[index][1]));
	mapNames->x = mapPic->x;
	mapNames->y = mapPic->y + mapPic->getSpriteHeight() -
		mapNames->getSpriteHeight();
}

void WindowMapLevel::processOK() {
	SpriteWindow::processOK();
	SpriteWindow::processBack();
	ScreenManager::pushScene(new SceneBattle(map->index, levelCommandGroup->getIndex()));
}

void WindowMapLevel::updateOthers() {
	SpriteWindow::updateOthers();
	updateControl();
}
void WindowMapLevel::updateControl() {
	SpriteWindow::updateControl();
	if (levelCommandGroup->isIndexChanged()) refreshMapInfo();
}



WindowItem::WindowItem() :SpriteWindow(menuItemBackground) {
	initialize(); setup(); refresh();
}
WindowItem::~WindowItem() {
	delete hpBar, mpBar;
	delete itemsGroup, equipsGroup;
	delete nameText, pageText, goldText;
	delete tipsDisplay;
	for (int i = 0; i < System::BaseParamCount - 2; i++)
		delete paramsText[i];
	for (int i = 0; i < GameManager::ItemSlotCount; i++)
		delete shortItems[i];
}
void WindowItem::setup() {
	SpriteWindow::setup();
	actor = GameManager::getLeader();
	changePage(0);
	deactivate();
	keyCd = 0;
}
void WindowItem::initialize() {
	SpriteWindow::initialize();
	initializeMHPBlock();
	initializeItemBlock();
	initializeParamsBlock();
	initializeEquipBlock();
	initializeShortItemsBlock();
	initializeTips();
}
void WindowItem::initializeMHPBlock() {
	hpBar = new SpriteVarBar(hpBarBitmap, new Bitmap(HPRect.width, HPRect.height));
	hpBar->x = HPRect.x; hpBar->y = HPRect.y;
	mpBar = new SpriteVarBar(mpBarBitmap, new Bitmap(MPRect.width, MPRect.height));
	mpBar->x = MPRect.x; mpBar->y = MPRect.y;
	hpBar->visible = mpBar->visible = true;
	addChild(hpBar); addChild(mpBar);
}
void WindowItem::initializeItemBlock() {
	itemsGroup = new SpriteCommandGroup(menuItemCommandBg, 3);
	itemsGroup->x = ItemBlockPos[0];
	itemsGroup->y = ItemBlockPos[1];
	SpriteCommand* cmd;
	for (int i = 0; i < pageItemCount(); i++) {
		cmd = itemsGroup->addCommand(new Bitmap(0, 0));
		cmd->y = i*(cmd->getHeight() + itemYSpacing());
	}
	itemsGroup->setActiveShake(false);
	itemsGroup->setActiveShine(false);

	pageText = new TextDisplay(defaultFontFaceSmall);
	pageText->x = PageTextPos[0];
	pageText->y = PageTextPos[1];
	addChild(itemsGroup); 
	addChild(pageText);
}
void WindowItem::initializeParamsBlock() {
	nameText = new TextDisplay(defaultFontFaceSmall);
	nameText->x = ActorNamePos[0];
	nameText->y = ActorNamePos[1];

	goldText = new NumberDisplay(defaultFontFaceSmall);
	goldText->x = GoldTextPos[0];
	goldText->y = GoldTextPos[1];

	for (int i = 0; i < System::BaseParamCount - 2; i++) {
		paramsText[i] = new NumberDisplay(defaultFontFaceSmall);
		paramsText[i]->x = ParamsPos[i][0];
		paramsText[i]->y = ParamsPos[i][1];
		addChild(paramsText[i]);
	}
	addChild(nameText); addChild(goldText);
}
void WindowItem::initializeEquipBlock() {
	equipsGroup = new SpriteCommandGroup(menuEquipCommandBg, 4);
	SpriteCommand* cmd;
	for (int i = 0; i < System::EquipCount; i++) {
		cmd = equipsGroup->addCommand(new Bitmap(0, 0));
		cmd->x = EquipPos[i][0];
		cmd->y = EquipPos[i][1];
	}
	equipsGroup->setActiveShake(false);
	equipsGroup->setActiveShine(false);
	addChild(equipsGroup);
}
void WindowItem::initializeShortItemsBlock() {
	for (int i = 0; i < GameManager::ItemSlotCount; i++) {
		shortItems[i] = new Sprite(new Bitmap(36, 18));
		shortItems[i]->x = ShortCutPos[0] + 6;
		shortItems[i]->y = ShortCutPos[1] + 3;
		shortItems[i]->y += i*(shortItems[i]->
			getHeight() + itemYSpacing());
		addChild(shortItems[i]);
	}
}
void WindowItem::initializeTips() {
	tipsDisplay = new Sprite(Bitmap::getBitmap("../HKK/ASCII-MenuItemTips.txt"));
	tipsDisplay->y = (getSpriteHeight() - tipsDisplay->getSpriteHeight()) / 2;
	tipsDisplay->z = 100;
	tipsDisplay->visible = false;
	addChild(tipsDisplay);
}

int WindowItem::pageItemCount() { return 5; }
int WindowItem::itemYSpacing() { return 2; }

void WindowItem::changePage(int index) {
	if (pageIndex != index) {
		pageIndex = index;
		refreshItemBlock();
	}
}
int WindowItem::maxPage() { return max(1,pageData.size()); }
bool WindowItem::isEquipSelecting() { return equipFrame; }

void WindowItem::activate() {
	SpriteWindow::activate();
	GameManager::newEquip = false;
	itemsGroup->activate();
	equipsGroup->deactivate();
	equipFrame = false;
}
void WindowItem::deactivate() {
	SpriteWindow::deactivate();
	itemsGroup->deactivate();
	equipsGroup->deactivate();
}

void WindowItem::refresh() {
	SpriteWindow::refresh();
	refreshMHPBlock();
	refreshItemBlock();
	refreshPrarmsBlock();
	refreshEquipBlock();
	refreshShortItemsBlock();
}
void WindowItem::refreshMHPBlock() {
	hpBar->setRate(actor->getHpRate());
	mpBar->setRate(actor->getMpRate());
}
void WindowItem::refreshItemBlock() {
	clearItemsList();
	makeItemsList();
	drawAllItems();
	drawPageCount();
}
void WindowItem::refreshEquipBlock() {
	SpriteCommand* cmd;
	for (int i = 0; i < System::EquipCount; i++) {
		Equip* equip = actor->getEquips()[i];
		cmd = equipsGroup->getCommand(i);
		if(equip == NULL)
			cmd->setupContents(1, new Bitmap(0, 0));
		else
			//cmd->setupContents(1, Bitmap::getIconWithoutCache(equip->iconId, cmd->getWidth(), cmd->getHeight()));
			cmd->setupContents(1, Bitmap::getIcon(equip->iconId));
	}
}
void WindowItem::refreshPrarmsBlock() {
	char info_c[50]; 
	string info = actor->getName();
	sprintf_s(info_c, "    Lv. %d", actor->getLevel());
	nameText->setText(info+info_c);

	goldText->setNum(GameManager::getGold());

	int paramsOrder[6] = { 2,3,6,4,5,7 };
	for (int i = 0; i < System::BaseParamCount - 2; i++) 
		paramsText[i]->setNum(actor->param(paramsOrder[i]));
}
void WindowItem::refreshShortItemsBlock() {
	for (int i = 0; i < GameManager::ItemSlotCount; i++) {
		Item* item = GameManager::getItemSlot(i);
		if (item == NULL)
			shortItems[i]->setBitmap(new Bitmap(36, 18));
		else
			shortItems[i]->setBitmap(Bitmap::getIcon(item->iconId));
	}
}
void WindowItem::makeItemsList() {
	vector<ItemBase*> data = GameManager::getItems();
	vector<ItemBase*> page; int pi = 0;
	for (int i = 0; i < data.size(); i++) {
		page.push_back(data[i]);
		if (++pi >= pageItemCount()) {
			pageData.push_back(page);
			page.clear(); pi = 0;
		}
	}
	if(page.size() > 0) pageData.push_back(page);
}
void WindowItem::clearItemsList() { pageData.clear(); }

void WindowItem::drawItem(int index) {
	clearItem(index);
	if (pageData.size() <= pageIndex) return;
	if (pageData[pageIndex].size() <= index) return;
	ItemBase* item = pageData[pageIndex][index];
	itemsGroup->getCommand(index)->setupContents(1, Bitmap::getBitmap(item->fileName));
}
void WindowItem::clearItem(int index) {
	itemsGroup->getCommand(index)->setupContents(1, new Bitmap(0, 0));
}

void WindowItem::drawPageCount() {
	char info_c[50];
	sprintf_s(info_c, "%d/%d", pageIndex + 1, maxPage());
	pageText->setText(info_c);
}

void WindowItem::drawAllItems() {
	for (int i = 0; i < pageItemCount(); i++) drawItem(i);
}
void WindowItem::clearAllItems() {
	for (int i = 0; i < pageItemCount(); i++) clearItem(i);
}
void WindowItem::processOK() {
	int index;
	if (isEquipSelecting()) {
		index = equipsGroup->getIndex();
		actor->unequipToItem(index);
	}else {
		index = itemsGroup->getIndex();
		if (pageIndex >= pageData.size() ||
			index >= pageData[pageIndex].size()) return;
		ItemBase* item = pageData[pageIndex][index];
		if (item == NULL) return;
		if (item->type >= 2) { // Equip(2:Weapon  3:Armor)
			actor->equipWithItem(static_cast<Equip*>(item));
		}
		if (item->type == 0) { // UseableItem
			actor->useItem(static_cast<Item*>(item));
		}
	}
	refresh();
}
void WindowItem::processBind(int sIndex) {
	if (isEquipSelecting()) return;
	int index = itemsGroup->getIndex();
	if (pageIndex >= pageData.size() ||
		index >= pageData[pageIndex].size()) 
		GameManager::clearItemSlot(sIndex);
	else {
		ItemBase* item = pageData[pageIndex][index];
		if (item == NULL) GameManager::clearItemSlot(sIndex);
		else if(item->type == 0) GameManager::switchItemSlot(static_cast<Item*>(item), sIndex);
	}
	refresh();
}
void WindowItem::switchGroup() {
	if (itemsGroup->isActive()) {
		itemsGroup->deactivate();
		equipsGroup->activate();
		equipFrame = true;
	}else {
		itemsGroup->activate();
		equipsGroup->deactivate();
		equipFrame = false;
	}
}
void WindowItem::scrollLeft() {
	int id = max(0, pageIndex - 1);
	changePage(id);
}
void WindowItem::scrollRight() {
	int id = min(maxPage() - 1, pageIndex + 1);
	changePage(id);
}

void WindowItem::updateControl() {
	SpriteWindow::updateControl();
	if (keyCd > 0 || !isActive()) return;
	bool enterFlag = false;
	int lr = InputManager::LR();
	if (!enterFlag && lr != 0) {
		lr > 0 ? scrollRight() : scrollLeft(); enterFlag = true;
	}
	tipsDisplay->visible = InputManager::isKeyPress(InputManager::Help);

	if (!enterFlag && InputManager::isKeyPressWithCD(InputManager::Switch)) {
		switchGroup(); enterFlag = true;
	}
	if (!enterFlag) 
		for (int i = 0; i < GameManager::ItemSlotCount; i++) {
			if (InputManager::isKeyPressWithCD(GameManager::itemSlotKeys[i])) {
				processBind(i); enterFlag = true; break;
			}
		}
	//if (enterFlag) keyCd = 5;
}



WindowSkill::WindowSkill() :SpriteWindow(menuSkillBackground) {
	initialize(); setup(); refresh();
}
WindowSkill::~WindowSkill() {
	delete hpBar, mpBar;
	delete pageText;
	delete tipsDisplay;
	for (int i = 0; i < GameManager::SkillSlotCount; i++)
		delete shortSkills[i];
}
void WindowSkill::setup() {
	SpriteWindow::setup();
	actor = GameManager::getLeader();
	changePage(0);
	deactivate();
	keyCd = 0;
}
void WindowSkill::initialize() {
	SpriteWindow::initialize();
	initializeMHPBlock();
	initializeSkillBlock();
	initializeShortSkillsBlock();
	initializeTips();
}
void WindowSkill::initializeMHPBlock() {
	hpBar = new SpriteVarBar(hpBarBitmap, new Bitmap(HPRect.width, HPRect.height));
	hpBar->x = HPRect.x; hpBar->y = HPRect.y;
	mpBar = new SpriteVarBar(mpBarBitmap, new Bitmap(MPRect.width, MPRect.height));
	mpBar->x = MPRect.x; mpBar->y = MPRect.y;
	hpBar->visible = mpBar->visible = true;
	addChild(hpBar); addChild(mpBar);
}
void WindowSkill::initializeSkillBlock() {
	skillsGroup = new SpriteCommandGroup(menuSkillCommandBg, 5);
	skillsGroup->x = SkillBlockPos[0];
	skillsGroup->y = SkillBlockPos[1];
	SpriteCommand* cmd;
	for (int i = 0; i < pageItemCount(); i++) {
		cmd = skillsGroup->addCommand(new Bitmap(0, 0));
		cmd->y = i*(cmd->getHeight() + itemYSpacing());
	}
	skillsGroup->setActiveShake(false);
	skillsGroup->setActiveShine(false);

	pageText = new TextDisplay(defaultFontFaceSmall);
	pageText->x = PageTextPos[0];
	pageText->y = PageTextPos[1];
	addChild(skillsGroup);
	addChild(pageText);
}
void WindowSkill::initializeShortSkillsBlock() {
	for (int i = 0; i < GameManager::SkillSlotCount; i++) {
		shortSkills[i] = new Sprite(new Bitmap(36, 18));
		shortSkills[i]->x = ShortCutPos[0] + 6;
		shortSkills[i]->y = ShortCutPos[1] + 3;
		shortSkills[i]->y += i*(shortSkills[i]->
			getHeight() + itemYSpacing());
		addChild(shortSkills[i]);
	}
}
void WindowSkill::initializeTips() {
	tipsDisplay = new Sprite(Bitmap::getBitmap("../HKK/ASCII-MenuSkillTips.txt"));
	tipsDisplay->y = (getSpriteHeight() - tipsDisplay->getSpriteHeight()) / 2;
	tipsDisplay->z = 100;
	tipsDisplay->visible = false;
	addChild(tipsDisplay);
}
int WindowSkill::pageItemCount() { return 6; }
int WindowSkill::itemYSpacing() { return 2; }
void WindowSkill::changePage(int index) {
	if (pageIndex != index) {
		pageIndex = index;
		refreshSkillBlock();
	}
}
int WindowSkill::maxPage() { return max(1, pageData.size()); }

void WindowSkill::activate() {
	SpriteWindow::activate();
	skillsGroup->activate();
	GameManager::newSkill = false;
}
void WindowSkill::deactivate() {
	SpriteWindow::deactivate();
	skillsGroup->deactivate();
}

void WindowSkill::refresh() {
	SpriteWindow::refresh();
	refreshMHPBlock();
	refreshSkillBlock();
	refreshShortSkillsBlock();
}
void WindowSkill::refreshMHPBlock() {
	hpBar->setRate(actor->getHpRate());
	mpBar->setRate(actor->getMpRate());
}
void WindowSkill::refreshSkillBlock() {
	clearItemsList();
	makeItemsList();
	drawAllItems();
	drawPageCount();
}
void WindowSkill::refreshShortSkillsBlock() {
	for (int i = 0; i < GameManager::SkillSlotCount; i++) {
		Skill* item = GameManager::getSkillSlot(i);
		if (item == NULL)
			shortSkills[i]->setBitmap(new Bitmap(36, 18));
		else
			shortSkills[i]->setBitmap(Bitmap::getIcon(item->iconId));
	}
}
void WindowSkill::makeItemsList() {
	vector<Skill*> data = actor->getSkills();
	vector<Skill*> page; int pi = 0;
	for (int i = 0; i < data.size(); i++) {
		page.push_back(data[i]);
		if (++pi >= pageItemCount()) {
			pageData.push_back(page);
			page.clear(); pi = 0;
		}
	}
	if (page.size() > 0) pageData.push_back(page);
}
void WindowSkill::clearItemsList() { pageData.clear(); }

void WindowSkill::drawItem(int index) {
	clearItem(index);
	if (pageData.size() <= pageIndex) return;
	if (pageData[pageIndex].size() <= index) return;
	Skill* item = pageData[pageIndex][index];
	skillsGroup->getCommand(index)->setupContents(1, Bitmap::getBitmap(item->fileName));
}
void WindowSkill::clearItem(int index) {
	skillsGroup->getCommand(index)->setupContents(1, new Bitmap(0, 0));
}

void WindowSkill::drawPageCount() {
	char info_c[50];
	sprintf_s(info_c, "%d/%d", pageIndex + 1, maxPage());
	pageText->setText(info_c);
}

void WindowSkill::drawAllItems() {
	for (int i = 0; i < pageItemCount(); i++) drawItem(i);
}
void WindowSkill::clearAllItems() {
	for (int i = 0; i < pageItemCount(); i++) clearItem(i);
}

void WindowSkill::processOK() {
	int index = skillsGroup->getIndex();
	if (pageIndex >= pageData.size() ||
		index >= pageData[pageIndex].size()) return;
	Skill* skill = pageData[pageIndex][index];
	if (skill == NULL) return;
	actor->useSkill(skill);
	refresh();
}
void WindowSkill::processBind(int sIndex) {
	int index = skillsGroup->getIndex();
	if (pageIndex >= pageData.size() ||
		index >= pageData[pageIndex].size())
		GameManager::clearSkillSlot(sIndex);
	else {
		Skill* skill = pageData[pageIndex][index];
		if (skill == NULL) GameManager::clearSkillSlot(sIndex);
		else GameManager::switchSkillSlot(skill, sIndex);
	}
	refresh();
}
void WindowSkill::scrollLeft() {
	int id = max(0, pageIndex - 1);
	changePage(id);
}
void WindowSkill::scrollRight() {
	int id = min(maxPage() - 1, pageIndex + 1);
	changePage(id);
}

void WindowSkill::updateControl() {
	SpriteWindow::updateControl();
	if (keyCd > 0 || !isActive()) return;
	bool enterFlag = false;
	int lr = InputManager::LR();
	if (!enterFlag && lr != 0) {
		lr > 0 ? scrollRight() : scrollLeft(); enterFlag = true;
	}
	tipsDisplay->visible = InputManager::isKeyPress(InputManager::Help);

	if (!enterFlag)
		for (int i = 0; i < GameManager::SkillSlotCount; i++) {
			if (InputManager::isKeyPressWithCD(GameManager::skillSlotKeys[i])) {
				processBind(i); enterFlag = true; break;
			}
		}
	//if (enterFlag) keyCd = 5;
}

WindowShop::WindowShop() {
	this->WindowShop::WindowShop(getAutoSelectShop());
}
WindowShop::WindowShop(int id) {
	this->WindowShop::WindowShop(&shops[id]);
}
WindowShop::WindowShop(Shop* shop) : SpriteWindow(menuShopBackground), shop(shop) {
	initialize(); setup(); refresh();
}
WindowShop::~WindowShop() {
	delete pageText, goldText;
	delete priceText, holdText, sumText;
	delete countText;
	delete tipsDisplay;
	delete goodsGroup;
}
int WindowShop::getAutoSelectShop() {
	ObjectActor* actor = GameManager::getLeader();
	// return 6;
	int level = actor->getLevel();
	if (rand() % 200 <= level) return 5;
	if (level <= 20) return 0;
	if (level <= 40) return 1;
	if (level <= 70) return 2;
	if (level <= 99) return 3;
	return 4;
}

void WindowShop::setShop(int id) { setShop(&shops[id]); }
void WindowShop::setShop(Shop* shop) {
	this->shop = shop; refresh();
}
void WindowShop::setup() {
	SpriteWindow::setup();
	changePage(0);
	deactivate();
	keyCd = 0;
}
void WindowShop::initialize() {
	SpriteWindow::initialize();
	initializeGoodBlock();
	initializeGoodInfoBlock();
	initializeGoldBlock();
	initializeCat();
	initializeTips();
}
void WindowShop::initializeGoodBlock() {
	goodsGroup = new SpriteCommandGroup(menuItemCommandBg, 3);
	goodsGroup->x = GoodBlockPos[0];
	goodsGroup->y = GoodBlockPos[1];
	SpriteCommand* cmd;
	for (int i = 0; i < pageItemCount(); i++) {
		cmd = goodsGroup->addCommand(new Bitmap(0, 0));
		cmd->y = i*(cmd->getHeight() + itemYSpacing());
	}
	goodsGroup->setActiveShake(false);
	goodsGroup->setActiveShine(false);

	pageText = new TextDisplay(defaultFontFaceSmall);
	pageText->x = PageTextPos[0];
	pageText->y = PageTextPos[1];
	addChild(goodsGroup);
	addChild(pageText);
}
void WindowShop::initializeGoodInfoBlock() {
	priceText = new NumberDisplay(defaultFontFaceSmall);
	priceText->x = PriceTextPos[0];
	priceText->y = PriceTextPos[1];

	holdText = new NumberDisplay(defaultFontFaceSmall);
	holdText->x = HoldTextPos[0];
	holdText->y = HoldTextPos[1];

	countText = new TypableNumberDisplay(defaultFontFaceSmall,99,1);
	countText->x = CountTextPos[0];
	countText->y = CountTextPos[1];
	countText->deactivate();

	sumText = new NumberDisplay(defaultFontFaceSmall);
	sumText->x = SumTextPos[0];
	sumText->y = SumTextPos[1];

	addChild(priceText); addChild(holdText);
	addChild(countText); addChild(sumText);
}
void WindowShop::initializeGoldBlock() {
	goldText = new NumberDisplay(defaultFontFaceSmall);
	goldText->x = GoldTextPos[0];
	goldText->y = GoldTextPos[1];
	addChild(goldText);
}
void WindowShop::initializeCat() {
	cat = new Sprite(Bitmap::getBitmap("../HKK/ASCII-MenuShopCat1.txt"));
	cat->x = CatPosX; cat->y = getSpriteHeight() - cat->getSpriteHeight();

	alert = new Sprite();
	alert->x = AlertPos[0];
	alert->y = AlertPos[1];
	alert->z = cat->z = 25;
	alert->visible = false;
	addChild(alert);
	addChild(cat);
}
void WindowShop::initializeTips() {
	tipsDisplay = new Sprite(Bitmap::getBitmap("../HKK/ASCII-MenuShopTips.txt"));
	tipsDisplay->y = (getSpriteHeight() - tipsDisplay->getSpriteHeight()) / 2;
	tipsDisplay->visible = false;
	tipsDisplay->z = 100;
	addChild(tipsDisplay);
}
int WindowShop::pageItemCount() { return 6; }
int WindowShop::itemYSpacing() { return 2; }
void WindowShop::changePage(int index) {
	if (pageIndex != index) {
		pageIndex = index;
		countText->setNum(1);
		refresh();
	}
}
int WindowShop::maxPage() { return max(1, pageData.size()); }

void WindowShop::setCatPattern(Bitmap* catPattern) {
	cat->setBitmap(catPattern);
}
void WindowShop::alertMsg(Bitmap* alertContents) {
	alert->setBitmap(alertContents);	
	alert->visible = true;
	setCatPattern(Bitmap::getBitmap("../HKK/ASCII-MenuShopCat3.txt"));
	cat->shake(5, 5);
	count = 0;
}
void WindowShop::closeAlertMsg() {
	alert->visible = false;
	setCatPattern(Bitmap::getBitmap("../HKK/ASCII-MenuShopCat1.txt"));
}

ItemBase* WindowShop::getItem() {
	int index = goodsGroup->getIndex();
	if (pageIndex >= pageData.size() ||
		index >= pageData[pageIndex].size())
		return NULL;
	return pageData[pageIndex][index];
}

void WindowShop::activate() {
	SpriteWindow::activate();
	setShop(getAutoSelectShop());
	goodsGroup->activate(); 
	closeAlertMsg();
	buyFlag = false;
}
void WindowShop::deactivate() {
	SpriteWindow::deactivate();
	goodsGroup->deactivate();
}

void WindowShop::refresh() {
	SpriteWindow::refresh();
	refreshGoodBlock();
	refreshGoodInfoBlock();
	refreshGoldBlock();
}
void WindowShop::refreshGoodBlock() {
	clearItemsList();
	makeItemsList();
	drawAllItems();
	drawPageCount();
}
void WindowShop::refreshGoodInfoBlock() {
	ItemBase* item = getItem();
	if (item == NULL) {
		priceText->setNum(0);
		holdText->setNum(0);
		countText->setNum(0);
		sumText->setNum(0);
	}else {
		priceText->setNum(item->price);
		holdText->setNum(GameManager::itemNum(item));
		sumText->setNum(countText->getNum()*item->price);
	}
}
void WindowShop::refreshGoldBlock() {
	goldText->setNum(GameManager::getGold());
}
void WindowShop::makeItemsList() {
	vector<ItemBase*> data = shop->list;
	vector<ItemBase*> page; int pi = 0;
	for (int i = 0; i < data.size(); i++) {
		if (data[i]->type >= 2 && GameManager::hasItem(data[i])) continue;
		page.push_back(data[i]);
		if (++pi >= pageItemCount()) {
			pageData.push_back(page);
			page.clear(); pi = 0;
		}
	}
	if (page.size() > 0) pageData.push_back(page);
}
void WindowShop::clearItemsList() { pageData.clear(); }

void WindowShop::drawItem(int index) {
	clearItem(index);
	if (pageData.size() <= pageIndex) return;
	if (pageData[pageIndex].size() <= index) return;
	ItemBase* item = pageData[pageIndex][index];
	goodsGroup->getCommand(index)->setupContents(1, Bitmap::getBitmap(item->fileName));
}
void WindowShop::clearItem(int index) {
	goodsGroup->getCommand(index)->setupContents(1, new Bitmap(0, 0));
}

void WindowShop::drawPageCount() {
	char info_c[50];
	sprintf_s(info_c, "%d/%d", pageIndex + 1, maxPage());
	pageText->setText(info_c);
}

void WindowShop::drawAllItems() {
	for (int i = 0; i < pageItemCount(); i++) drawItem(i);
}
void WindowShop::clearAllItems() {
	for (int i = 0; i < pageItemCount(); i++) clearItem(i);
}

void WindowShop::processOK() {
	if (alert->visible) { closeAlertMsg(); return; }
	ItemBase* item = getItem();
	if (item == NULL) return;
	if (!countText->isActive()) {
		goodsGroup->deactivate();
		countText->activate();
	}else {
		refreshGoodInfoBlock();
		int gold = GameManager::getGold();
		int price = sumText->getNum();
		if (gold >= price) {
			alertMsg(Bitmap::getBitmap("../HKK/ASCII-AlertMsg2.txt")); buyFlag = true;
			GameManager::loseGold(price);
			GameManager::gainItem(item, countText->getNum());
			countText->deactivate();
			goodsGroup->activate();
		}else
			alertMsg(Bitmap::getBitmap("../HKK/ASCII-AlertMsg1.txt"));
	}
	refresh();
}
void WindowShop::processBack() {
	if (alert->visible) { closeAlertMsg(); return; }
	if (countText->isActive()) {
		countText->deactivate(); goodsGroup->activate(); 
		return; 
	}
	if (!buyFlag) setCatPattern(Bitmap::getBitmap("../HKK/ASCII-MenuShopCat2.txt"));
	SpriteWindow::processBack();
}
void WindowShop::scrollLeft() {
	int id = max(0, pageIndex - 1);
	changePage(id);
}
void WindowShop::scrollRight() {
	int id = min(maxPage() - 1, pageIndex + 1);
	changePage(id);
}

void WindowShop::updateOthers() {
	SpriteWindow::updateOthers();
	updateAlert();
}
void WindowShop::updateControl() {
	SpriteWindow::updateControl();
	if (goodsGroup->isIndexChanged()) {
		countText->setNum(1);
		refreshGoodInfoBlock();
	}
	if (countText->isChanged()) refreshGoodInfoBlock();
	if (keyCd > 0 || !isActive()) return;
	bool enterFlag = false;
	int lr = InputManager::LR();
	if (!enterFlag && lr != 0) {
		lr > 0 ? scrollRight() : scrollLeft(); enterFlag = true;
	}
	tipsDisplay->visible = InputManager::isKeyPress(InputManager::Help);
	//if (enterFlag) keyCd = 5;
}
void WindowShop::updateAlert() {
	if(alert->visible)
		if (++count == 50) {
			closeAlertMsg(); count = 0;
		}
}

WindowBattleResult::WindowBattleResult():
	SpriteWindow(Bitmap::getBitmap("../HKK/ASCII-BattleResultWindow.txt")){
	initialize();
}
WindowBattleResult::~WindowBattleResult() {
	delete timeNeed, recoverPoint;
	delete beHurtPoint, hurtPoint;
	delete difficulty, finalScore;
	delete goldGain, judge;
}
void WindowBattleResult::initialize() {
	SpriteWindow::initialize();
	popScene = setuped = false;
	initializeParams();
}
void WindowBattleResult::initializeParams() {
	timeNeed = new NumberDisplay(defaultFontFaceSmall);
	timeNeed->x = TimeNeedPos[0];
	timeNeed->y = TimeNeedPos[1];

	recoverPoint = new NumberDisplay(defaultFontFaceSmall);
	recoverPoint->x = RecoverPointPos[0];
	recoverPoint->y = RecoverPointPos[1];

	beHurtPoint = new NumberDisplay(defaultFontFaceSmall);
	beHurtPoint->x = BeHurtPointPos[0];
	beHurtPoint->y = BeHurtPointPos[1];

	hurtPoint = new NumberDisplay(defaultFontFaceSmall);
	hurtPoint->x = HurtPointPos[0];
	hurtPoint->y = HurtPointPos[1];

	finalScore = new NumberDisplay(defaultFontFaceSmall);
	finalScore->x = FinalScorePos[0];
	finalScore->y = FinalScorePos[1];

	difficulty = new TextDisplay(defaultFontFaceSmall);
	difficulty->x = DifficultPos[0];
	difficulty->y = DifficultPos[1];

	goldGain = new NumberDisplay(defaultFontFaceSmall);
	goldGain->x = GoldGainPos[0];
	goldGain->y = GoldGainPos[1];

	judge = new Sprite();
	judge->x = JudgePos[0];
	judge->y = JudgePos[1];
	judge->z = 50;

	addChild(timeNeed); addChild(recoverPoint);
	addChild(beHurtPoint); addChild(hurtPoint);
	addChild(difficulty); addChild(finalScore);
	addChild(goldGain); addChild(judge);
}

void WindowBattleResult::setup() {
	setuped = true; duration = 0;
	gold = BattleManager::getGold();
	refresh();
}
void WindowBattleResult::refresh() {
	SpriteWindow::refresh();
	refreshParams();
}
void WindowBattleResult::refreshParams() {
	char diff[10];
	sprintf_s(diff, "%.2f", BattleManager::getDifficulty());
	difficulty->setText(diff);
	timeNeed->setNum(BattleManager::getTime());
	recoverPoint->setNum(BattleManager::getRecoverPoint());
	beHurtPoint->setNum(BattleManager::getBeHurtPoint());
	hurtPoint->setNum(BattleManager::getHurtPoint());
	finalScore->setNum(BattleManager::getScore());
	judge->setBitmap(Bitmap::getBitmap(
		JudgeFileName[BattleManager::getJudge()]));
	judge->setFrameRect(0, 0, 0, judge->getHeight());
}
void WindowBattleResult::requestPopScene() { popScene = true; }
void WindowBattleResult::processOK() { requestPopScene(); }
void WindowBattleResult::processBack() { requestPopScene(); }

bool WindowBattleResult::popSceneRequest() { return popScene; }


void WindowBattleResult::updateOthers() {
	SpriteWindow::updateOthers();
	if(setuped) updateTextAni();
}
void WindowBattleResult::updateTextAni() {
	duration++;
	updateGoldAni();
	updateJudgeAni();
}
void WindowBattleResult::updateGoldAni() {
	if (duration <= GoldTextAniDuration) 
		goldGain->setNum((duration*1.0 / GoldTextAniDuration)*gold);
}
void WindowBattleResult::updateJudgeAni() {
	if (duration > GoldTextAniDuration && duration <= 1.5 * GoldTextAniDuration) {
		int w = judge->getWidth()*((duration - GoldTextAniDuration)*1.0 / (GoldTextAniDuration/2));
		judge->setFrameRect(0, 0, w, judge->getHeight());
	}
}



WindowFile::WindowFile(bool onlyLoad) :SpriteWindow(menuSaveBackground) {
	this->onlyLoad = onlyLoad;
	initialize(); setup(); refresh();
}
WindowFile::~WindowFile() {
	delete name, time;
	delete level, gold;
	for (int i = 0; i < System::MaxMapCount; i++)
		delete fileCommands[i];
}
void WindowFile::initialize() {
	SpriteWindow::initialize();
	initializeFileInfo();
	initializeFileCommands();
	initializeTips();
}
void WindowFile::setup() {
	SpriteWindow::setup();
	index = 0; select(0);
	deactivate();
}
void WindowFile::initializeFileInfo() {
	name = new TextDisplay(defaultFontFaceSmall);
	name->x = LeaderNamePos[0];
	name->y = LeaderNamePos[1];

	time = new TextDisplay(defaultFontFaceSmall);
	time->x = DateTimePos[0];
	time->y = DateTimePos[1];

	level = new NumberDisplay(defaultFontFaceSmall);
	level->x = LeaderLevelPos[0];
	level->y = LeaderLevelPos[1];

	gold = new NumberDisplay(defaultFontFaceSmall);
	gold->x = GoldPos[0];
	gold->y = GoldPos[1];

	addChild(name); addChild(time);
	addChild(level); addChild(gold);
}
void WindowFile::initializeFileCommands() {
	for (int i = 0; i < DataManager::MaxSaveFileCount; i++) {
		fileBitmaps[i] = Bitmap(menuFileCommandBg->getWidth(), menuFileCommandBg->getHeight());
		fileCommands[i] = new SpriteCommand(menuFileCommandBg, 15, &fileBitmaps[i]);
		fileCommands[i]->x = FilePos[i][0]; fileCommands[i]->y = FilePos[i][1];
		fileCommands[i]->setActiveShake(false);
		fileCommands[i]->setActiveShine(false);
		addChild(fileCommands[i]);
	}
}
void WindowFile::initializeTips() {
	tipsDisplay = new Sprite(Bitmap::getBitmap("../HKK/ASCII-MenuSaveTips.txt"));
	tipsDisplay->y = (getSpriteHeight() - tipsDisplay->getSpriteHeight()) / 2;
	tipsDisplay->visible = false;
	tipsDisplay->z = 100;
	addChild(tipsDisplay);
}
void WindowFile::refresh() {
	SpriteWindow::refresh();
	refreshFileInfo();
	refreshFileCommands();
}
void WindowFile::refreshFileInfo() {
	SaveFileData* data = DataManager::getFileData(index);
	if (data->empty) {
		name->setText("---");
		time->setText("---");
		level->setNum(0);
		gold->setNum(0);
	}else {
		name->setText(data->leaderName);
		time->setText(data->time);
		level->setNum(data->level);
		gold->setNum(data->gold);
	}
}
void WindowFile::refreshFileCommands() {
	for (int i = 0; i < DataManager::MaxSaveFileCount; i++)
		refreshFileCommandBitmap(i);
}
void WindowFile::refreshFileCommandBitmap(int id) {
	SaveFileData* data = DataManager::getFileData(id);
	fileBitmaps[id] = Bitmap(menuFileCommandBg->getWidth(),
		menuFileCommandBg->getHeight());
	if (data->empty)  return;
	Actor* actor = &actors[data->leaderId];
	Bitmap* bitmap = Bitmap::getBitmap(actor->fileName);
	Bitmap* fileBitmap = &fileBitmaps[id];

	int yCount = SpriteBattler::motionYCount;
	int xCount = SpriteBattler::motionXCount;
	int mFrame = SpriteBattler::motionFrame;

	int w = bitmap->getWidth();
	int h = bitmap->getHeight();
	int bw = fileBitmap->getWidth();
	int bh = fileBitmap->getHeight();
	int cw = w / xCount, ch = h / yCount;
	int fw = cw / mFrame, fh = ch;
	int x = (SpriteBattler::Idle / yCount)*cw + fw;
	int y = (SpriteBattler::Idle % yCount)*ch;
	int tx = (bw - fw) / 2, ty = (bh - fh) / 2;

	fileBitmap->copyFrom(bitmap, x, y, fw, ch, tx, ty);
}
bool WindowFile::isOnlyLoad() { return onlyLoad; }
void WindowFile::activate() {
	SpriteWindow::activate(); fileCommands[index]->activate();
}
void WindowFile::deactivate() {
	SpriteWindow::deactivate(); fileCommands[index]->deactivate();
}
void WindowFile::select(int id) {
	deselect(); index = id;
	if (System::MaxMapCount > id)
		fileCommands[id]->select();
	refreshFileInfo();
}
void WindowFile::deselect() { deselect(index); }
void WindowFile::deselect(int id) { 
	if (DataManager::MaxSaveFileCount > id) 
		fileCommands[id]->deselect();
}

void WindowFile::scrollUp() {
	int id = (index - 2 + DataManager::MaxSaveFileCount) % DataManager::MaxSaveFileCount;
	select(id);
}
void WindowFile::scrollDown() {
	int id = (index + 2) % DataManager::MaxSaveFileCount;
	select(id);
}
void WindowFile::scrollLeft() {
	int id = (index - 1 + DataManager::MaxSaveFileCount) % DataManager::MaxSaveFileCount;
	select(id);
}
void WindowFile::scrollRight() {
	int id = (index + 1) % DataManager::MaxSaveFileCount;
	select(id);
}

void WindowFile::deleteGame() { 
	DataManager::deleteGame(index); 
	refresh();
}
void WindowFile::loadGame() { 
	if (DataManager::getFileData(index)->empty) return;
	GameManager::setdownGame();
	DataManager::loadGame(index); 
	if (onlyLoad) ScreenManager::pushScene(new SceneMenu);
	else ScreenManager::currentScene()->requestNextScene(new SceneMenu);
	processBack();
}
void WindowFile::saveGame() { DataManager::saveGame(index); processBack(); }

void WindowFile::processOK() {
	SpriteWindow::processOK();
	if (onlyLoad) loadGame(); else saveGame();
}

void WindowFile::updateOthers() {
	SpriteWindow::updateOthers();
	updateControl();
}
void WindowFile::updateControl() {
	SpriteWindow::updateControl();
	//if (keyCd > 0 || !isActive()) return;
	if (!isActive()) return;
	int ud = InputManager::UD();
	int lr = InputManager::LR();
	if (ud != 0) {
		keyCd = 5; ud > 0 ? scrollDown() : scrollUp(); return;
	}
	if (lr != 0) {
		keyCd = 5; lr > 0 ? scrollRight() : scrollLeft(); return;
	}
	if (InputManager::isKeyPressWithCD(InputManager::Load)) {
		keyCd = 5; loadGame(); return;
	}
	if (InputManager::isKeyPressWithCD(InputManager::Delete)) {
		keyCd = 5; deleteGame(); return;
	}
	tipsDisplay->visible = InputManager::isKeyPress(InputManager::Help);
}