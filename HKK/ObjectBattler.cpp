
#include "stdafx.h"

#include<cmath>

#include"Database.h"
#include "InputManager.h"
#include "ObjectBattler.h"
#define PI 3.141592653589793238462643383

const int ObjectBattler::JumpHeight = 80;
const int ObjectBattler::JumpLast = 20;
const int ObjectBattler::WalkSpeed = 4;
const int ObjectBattler::NormalAttackSkillIdWithoutWeapon = 0;
const int ObjectBattler::NormalAttackSkillId = 1;
const int ObjectBattler::JumpAttackSkillId = 2;

const int ObjectActor::FactorA = 2;

const int ObjectEnemy::EnemyAttackDelay = 200;
const int ObjectEnemy::WalkSpeed = 2;

AnimationRequestData::AnimationRequestData(AnimationData* ani, int ox, int oy, bool atGround) :
	ani(ani), ox(ox), oy(oy), atGround(atGround) {}

ObjectBattlerBase::ObjectBattlerBase() {}
ObjectBattlerBase::ObjectBattlerBase(Battler* battler) {
	this->battler = battler; setup();
}
void ObjectBattlerBase::setup() {
	level = 1;
	fileName = battler->fileName;
	name = battler->name;
	setupSkills();
	setupParams();
	hp = mhp();
	mp = mmp();
}
void ObjectBattlerBase::setupSkills() {
	refreshSkills();
}
void ObjectBattlerBase::refreshSkills() {}

void ObjectBattlerBase::setupParams() {
	for (int i = 0; i < System::BaseParamCount; i++) {
		params[i] = battler->params[i];
		buffs[i] = 0;
	}
	for (int i = 0; i < System::SpecialParamCount; i++) {
		specialParams[i] = battler->specialParams[i];
		specialBuffs[i] = 0;
	}
}
bool ObjectBattlerBase::isActor() { return false; }
bool ObjectBattlerBase::isEnemy() { return false; }

int ObjectBattlerBase::baseParam(int id) { return params[id]; }
int ObjectBattlerBase::plusParam(int id) { return params[id] * buffs[id]; }
int ObjectBattlerBase::param(int id) { return baseParam(id) + plusParam(id); }

double ObjectBattlerBase::baseSpecialParam(int id) { return specialParams[id]; }
double ObjectBattlerBase::plusSpecialParam(int id) { 
	double paramsEffect = 0;
	switch (id) {
		case 0: paramsEffect = 1 - exp(-agi() / 200.0 - luk() / 100.0); break;
		case 1: paramsEffect = (1 - exp(-agi() / 300.0))/4; break;
		case 2: paramsEffect = (1 - exp(-atk() / 5000.0 - luk() / 1000.0))/2; break;
	}
	return specialParams[id] * specialBuffs[id] + paramsEffect;
}
double ObjectBattlerBase::specialParam(int id) { return baseSpecialParam(id) + plusSpecialParam(id); }

void ObjectBattlerBase::registerCoolDownList(UsableItem* item) {
	if (coolDownList.count(item) > 0) coolDownList[item] += item->coolDown;
	else coolDownList[item] = item->coolDown;
}
void ObjectBattlerBase::clearCoolDownList() {
	coolDownList.clear();
}
int ObjectBattlerBase::getItemCoolDown(UsableItem* item) {
	if (coolDownList.count(item) > 0) return coolDownList[item];
	return 0;
}
double ObjectBattlerBase::getItemCoolDownRate(UsableItem* item) {
	return item==NULL ? 0 : getItemCoolDown(item)*1.0 / item->coolDown;
}

bool ObjectBattlerBase::isUsableItemUsable(UsableItem* item) {
	if (item == NULL) return false;
	if (getItemCoolDown(item)>0) return false;
	if (item->occasion == System::OccasionType::NoneOccasion) return false;
	if (item->occasion == System::OccasionType::Battle && !GameManager::inBattle) return false;
	if (item->occasion == System::OccasionType::Menu && GameManager::inBattle) return false;
	if (!GameManager::inBattle && !(item->target == System::TargetType::Self ||
			item->target == System::TargetType::Friends)) return false;
	return true;
}
bool ObjectBattlerBase::isItemUsable(Item* item) {
	if (!isUsableItemUsable(item)) return false;
	if (level < item->minLevel) return false;
	if (level > item->maxLevel) return false;
	if (GameManager::itemNum(item) <= 0) return false;
	return true;
}
bool ObjectBattlerBase::isSkillUsable(Skill* item) {
	if (!isUsableItemUsable(item)) return false;
	if (mp < item->mpCost) return false;
	return true;
}
bool ObjectBattlerBase::useSkill(int id) { return useSkill(&skills[id]);}
bool ObjectBattlerBase::useItem(int id) { return useItem(&items[id]); }
bool ObjectBattlerBase::useSkill(Skill* skill) {
	if (!isSkillUsable(skill)) return false;
	addMp(-skill->mpCost);
	if (GameManager::inBattle) useSkillInBattle(skill);
	else useSkillInMenu(skill);
	return true;
}
bool ObjectBattlerBase::useItem(Item* item) {
	if (!isItemUsable(item)) return false;
	if (item->consumable)GameManager::loseItem(item, 1);
	if (GameManager::inBattle) useItemInBattle(item);
	else useItemInMenu(item);
	return true;
}
void ObjectBattlerBase::useSkillInBattle(Skill* skill) {}
void ObjectBattlerBase::useSkillInMenu(Skill* skill) {
	if (skill->target == System::TargetType::Self ||
		skill->target == System::TargetType::Friends) {
		Damage damage(this, skill, this, 1.0);
		dealDamage(damage);
	}
}
void ObjectBattlerBase::useItemInBattle(Item* item) {
	if (item->iType == System::ItemType::LevelBall) levelUp();
	if (item->target == System::TargetType::Self ||
		item->target == System::TargetType::Friends) {
		Damage damage(this, item, this, 1.0);
		if (this->isActor()) {
			BattleManager::addRecover(damage.getDamageValue(Damage::DamageType::HPRecover));
			BattleManager::addRecover(damage.getDamageValue(Damage::DamageType::MPRecover));
		}
		dealDamage(damage);
	}
}
void ObjectBattlerBase::useItemInMenu(Item* item) {
	if (item->iType == System::ItemType::LevelBall) levelUp();
	if (item->target == System::TargetType::Self ||
		item->target == System::TargetType::Friends) {
		Damage damage(this, item, this, 1.0);
		dealDamage(damage);
	}
}

double ObjectBattlerBase::hitRate() {
	return specialParam(0);
}
double ObjectBattlerBase::missRate() { 
	return specialParam(1); 
}
double ObjectBattlerBase::critical() {
	return specialParam(2);
}
bool ObjectBattlerBase::isDead() { return hp <= 0; }

void ObjectBattlerBase::dealDamage(Damage damage) {
	if (damage.getHitResult() == Damage::HitType::Miss) return;
	int deltaHp = 0, deltaMp = 0;
	deltaHp -= damage.getDamageValue(Damage::DamageType::HPDamage);
	deltaMp -= damage.getDamageValue(Damage::DamageType::MPDamage);
	deltaHp -= damage.getDamageValue(Damage::DamageType::HPAbsorb);
	deltaMp -= damage.getDamageValue(Damage::DamageType::MPAbsorb);
	deltaHp += damage.getDamageValue(Damage::DamageType::HPRecover);
	deltaMp += damage.getDamageValue(Damage::DamageType::MPRecover);

	addHp(deltaHp); addMp(deltaMp);
}

void ObjectBattlerBase::addHp(int hp) { setHp(this->hp + hp); }
void ObjectBattlerBase::addMp(int mp) { setMp(this->mp + mp); }
void ObjectBattlerBase::setHp(int hp) { this->hp = hp; refresh(); }
void ObjectBattlerBase::setMp(int mp) { this->mp = mp; refresh(); }

void ObjectBattlerBase::recoverAll() {
	setHp(mhp()); setMp(mmp());
}

int ObjectBattlerBase::getLevel() { return level; }
void ObjectBattlerBase::levelUp() { setLevel(level + 1); }
void ObjectBattlerBase::levelDown() { setLevel(level - 1); }
void ObjectBattlerBase::setLevel(int lv) { 
	if (lv > level) recoverAll(); level = lv; 
}

vector<Skill*> ObjectBattlerBase::getSkills() { return learnedSkills; }
bool ObjectBattlerBase::isSkillLearned(int id) { return isSkillLearned(&skills[id]); }
bool ObjectBattlerBase::isSkillLearned(Skill* skill) {
	return find(learnedSkills.begin(), learnedSkills.end(), skill) != learnedSkills.end();
}
void ObjectBattlerBase::learnSkill(int id) { learnSkill(&skills[id]); }
void ObjectBattlerBase::learnSkill(Skill* skill) {
	if (isSkillLearned(skill)) return;
	learnedSkills.push_back(skill);
}
void ObjectBattlerBase::forgetSkill(int id) { forgetSkill(&skills[id]); }
void ObjectBattlerBase::forgetSkill(Skill* skill) {
	vector<Skill*>::iterator sit = find(learnedSkills.begin(), learnedSkills.end(), skill);
	if (sit != learnedSkills.end()) learnedSkills.erase(sit);
}

void ObjectBattlerBase::refresh() {
	hp = max(0, min(hp, mhp()));
	mp = max(0, min(mp, mmp()));
	refreshSkills();
}
void ObjectBattlerBase::update() { updateCoolDown(); }
void ObjectBattlerBase::updateCoolDown() { 
	map<UsableItem*, int>::iterator cit;
	for (cit = coolDownList.begin(); cit != coolDownList.end(); cit++) {
		if((*cit).second >0) (*cit).second--;
	}
}

string ObjectBattlerBase::getFileName() { return fileName; }
string ObjectBattlerBase::getName() { return name; }

string ObjectBattlerBase::makeSaveData() {
	// 不能有換行！
	string data = ""; char data_c[50];
	sprintf_s(data_c, "%d ", battler->index); data += data_c;
	sprintf_s(data_c, "%d ", level); data += data_c;
	sprintf_s(data_c, "%d %d ", hp, mp); data += data_c;

	sprintf_s(data_c, "%d ", learnedSkills.size()); data += data_c;

	for (int i = 0; i < learnedSkills.size(); i++) {
		sprintf_s(data_c, "%d ", learnedSkills[i]->index); data += data_c;
	}

	for (int i = 0; i < System::BaseParamCount; i++) {
		sprintf_s(data_c, "%d ", buffs[i]); data += data_c;
	}
	for (int i = 0; i < System::SpecialParamCount; i++) {
		sprintf_s(data_c, "%d ", specialBuffs[i]); data += data_c;
	}

	return data;
}
void ObjectBattlerBase::loadSaveData(string data) {
	// 不能有換行！
	char data_c[50];
	int index, vsize;
	int l = 1;
	vector<string> dataV = split(data, " ");
	sscanf_s(dataV[l++].c_str(), "%d", &level);
	sscanf_s(dataV[l++].c_str(), "%d", &hp);
	sscanf_s(dataV[l++].c_str(), "%d", &mp);
	sscanf_s(dataV[l++].c_str(), "%d", &vsize);
	for (int i = 0; i < vsize; i++) {
		sscanf_s(dataV[l++].c_str(), "%d", &index);
		learnSkill(index);
	}
	for (int i = 0; i < System::BaseParamCount; i++)
		sscanf_s(dataV[l++].c_str(), "%d", &buffs[i]);
	for (int i = 0; i < System::SpecialParamCount; i++)
		sscanf_s(dataV[l++].c_str(), "%d", &specialBuffs[i]);
}

ObjectBattler::ObjectBattler() {}
ObjectBattler::ObjectBattler(Battler* battler) {
	this->battler = battler; setup();
}
void ObjectBattler::setup() {
	ObjectBattlerBase::setup();
	resetSpeed();
	setupPosition();
	clearRequests();
	setupState();
}
void ObjectBattler::resetSpeed() {
	setSpeed(WalkSpeed);
}
void ObjectBattler::setupPosition() {
	transferTo(0, 0); z = 0;
}
void ObjectBattler::setupState() {
	setFreeze(false);
	setVisible(true);
	setDirection(false);
	setFixedDirection(false);
	setIdle();
}
void ObjectBattler::clearRequests() {
	clearRequestedSkill();
	clearRequestedOpers();
	clearRequestedAnimations();
	clearRequestedAnimationsAtTarget();
	clearRequestedEffect();
	clearRequestedMotion();
	clearRequestedDamage();
	clearRequestedPrompt();
	clearRequestedToTarget();
	clearRequestAny();
}

void ObjectBattler::dealDamage(Damage damage, bool show) {
	ObjectBattlerBase::dealDamage(damage);
	if(show) requestPrompt(damage);
}
void ObjectBattler::wait(int duration) { waitDuration = duration; }
void ObjectBattler::delay(int duration) { delayDuration = duration; }

void ObjectBattler::clearTargetPosition() { if(forceMovable) return; tx = x; ty = y; }
void ObjectBattler::clearBusyFlags() { defending = attacking = false; }

void ObjectBattler::transfer(int x, int y) { transferTo(this->x + x, this->y + y); }
void ObjectBattler::transferTo(int x, int y) { tx = this->x = x; ty = this->y = y; }
void ObjectBattler::transferToward(int dir) { transfer(dirX[dir], dirY[dir]); }

void ObjectBattler::move(int x, int y) { moveTo(this->x + x, this->y + y); }
void ObjectBattler::moveTo(int x, int y) { if (BattleManager::passable(x, y)) { tx = x; ty = y; } }
void ObjectBattler::moveToward(int dir) { move(dirX[dir] * (getSpeed()), dirY[dir] * (getSpeed())); }

void ObjectBattler::faceTo(ObjectBattler* battler) { faceTo(battler->getX()); }
void ObjectBattler::faceTo(int x) { setDirection(this->x == x ? direction : (this->x < x));}

void ObjectBattler::jump(int height = JumpHeight, int duration = JumpLast) {
	jumpDuration = duration; freezing = true;
	calcJumpFactorA(height);
	calcJumpFactorB();
}
void ObjectBattler::clearJump() {
	jumpDuration = -1; freezing = false;
}
void ObjectBattler::calcJumpFactorA(double h) {
	jumpFactorA = -4 * h / (jumpDuration*jumpDuration);
}
void ObjectBattler::calcJumpFactorB() {
	jumpFactorB = -jumpFactorA * jumpDuration;
}

void ObjectBattler::changeZ(int targetZ, int duration) {
	tz = targetZ; zDuration = duration;
	if (duration == 0) z = tz;
	else zSpeed = (tz - z) / duration;
	clearJump();
}

void ObjectBattler::setIdle() { clearTargetPosition(); clearBusyFlags(); }
void ObjectBattler::setDefend() { setIdle(); defending = true; }
void ObjectBattler::setAttack() { setIdle(); attacking = true; }
void ObjectBattler::setFreeze(bool fre) { freezing = fre; }
void ObjectBattler::setVisible(bool vis) { hidden = !vis; }

void ObjectBattler::setSpeed(int spd) { speed = spd; }
int ObjectBattler::getSpeed() { return isDefending() ? 1 : speed; }

void ObjectBattler::setDirection(bool dir) { direction = dir; }
bool ObjectBattler::getDirection() { return direction; }

int ObjectBattler::getForwardDirection() { return direction ? 0 : 4; }

int ObjectBattler::getReverseDirection(int dir) { return (dir + 4)%8; }
int ObjectBattler::getReverseDirection() { return getReverseDirection(direction ? 0 : 4); }

int ObjectBattler::getRightDirection(int dir) { return dir + 2 % 8; }
int ObjectBattler::getRightDirection() { return getRightDirection(direction ? 0 : 4); }

int ObjectBattler::getLeftDirection(int dir) { return dir - 2 + 8 % 8; }
int ObjectBattler::getLeftDirection() { return getLeftDirection(direction ? 0 : 4); }

void ObjectBattler::setFixedDirection(bool fixed) { fixedDir = fixed; }
bool ObjectBattler::isFixedDirection() { return fixedDir; }
/*
void ObjectBattler::useSkill(int id) {
	useSkill(&skills[id]);
}
void ObjectBattler::useSkill(Skill* skill) {
	if (GameManager::inBattle) {

	}else {

	}
}
void ObjectBattler::useItem(int id) {
	useItem(&items[id]);
}
void ObjectBattler::useItem(Item* item) {

}
*/
bool ObjectBattler::isUsableItemUsable(UsableItem* item) {
	if (!ObjectBattlerBase::isUsableItemUsable(item)) return false;
	if (isDelaying()) return false;
	return true;
}
void ObjectBattler::useSkillInBattle(Skill* skill) {
	//if (!isSkillUsable(skill)) return;
	ObjectBattlerBase::useSkillInBattle(skill);
	requestSkill(skill);
	delay(skill->delay);
	registerCoolDownList(skill);
}
void ObjectBattler::useItemInBattle(Item* item) {
	//if (!isItemUsable(item)) return;
	delay(item->delay);
	registerCoolDownList(item);
	requestOpers(makeOpersForItem(item));
	ObjectBattlerBase::useItemInBattle(item);
}
vector<OperatorData> ObjectBattler::makeOpersForItem(Item* item) {
	vector<OperatorData> res;
	res.push_back(OperatorData(0, OperatorData::OperatorType::Freeze));
	res.push_back(OperatorData(0, OperatorData::OperatorType::ChangeMotion));
	res[1].params.push_back(SpriteBattler::MotionType::UsingItem);
	res[1].params.push_back(3);
	if (item->usingAnimationId >= 0) {
		res.push_back(OperatorData(3, OperatorData::OperatorType::PlayAnimation));
		res[2].params.push_back(item->usingAnimationId);
	}
	res.push_back(OperatorData(5, OperatorData::OperatorType::Unfreeze));
	return res;
}


Skill* ObjectBattler::getOperingSkill() { 
	return operingSkill; 
}

void ObjectBattler::requestAny() { anyRequested = true; }
void ObjectBattler::clearRequestAny() { anyRequested = false; }
bool ObjectBattler::isAnyRequested() { return anyRequested; }

void ObjectBattler::requestSkill(int id) { usingSkill = &skills[id]; setAttack(); requestAny(); }
void ObjectBattler::requestSkill(Skill* skill) { usingSkill = skill; setAttack(); requestAny(); }
void ObjectBattler::requestOpers(vector<OperatorData> opers) {
	requestOpers(opers, operingSkill);
}
void ObjectBattler::requestOpers(vector<OperatorData> opers, Skill* skill) { operingSkill = skill; operDuration = 0; this->opers = opers; }
void ObjectBattler::requestAnimation(int id) { requestAnimation(&animations[id]); }
void ObjectBattler::requestAnimation(AnimationData* ani, int ox, int oy, bool atGround) {
	requestAnimation(new AnimationRequestData(ani, ox, oy, atGround));
}
void ObjectBattler::requestAnimation(AnimationRequestData* data) {
	requestedAnimations.push_back(data); requestAny();
}
void ObjectBattler::requestAnimationAtTarget(AnimationData* ani, int ox, int oy, bool atGround) { 
	requestedAnimationsAtTarget.push_back(
		new AnimationRequestData(ani, ox, oy, atGround));
	requestAny();
}
void ObjectBattler::requestMotion(SpriteBattler::MotionType mot, int duration = 0) { requestedMotion = mot; motionDuration = duration; }
void ObjectBattler::requestEffect(SpriteBattler::EffectType eff, vector<int> params = vector<int>()) {
	requestedEffect = eff; effectParams = params;
}
void ObjectBattler::requestDamage(int rate) { damageRate = rate; requestAny();}
void ObjectBattler::requestPrompt(Damage damage) { damagePrompt = damage;}
void ObjectBattler::requestMoveToTarget() { requestToTarget = 1; requestAny(); }
void ObjectBattler::requestTransferToTarget() { requestToTarget = 2; requestAny(); }

void ObjectBattler::clearRequestedSkill() { usingSkill = NULL; }
void ObjectBattler::clearRequestedOpers() { opers.clear(); operDuration = 0; operingSkill = NULL; }
void ObjectBattler::clearRequestedAnimations() { requestedAnimations.clear(); }
void ObjectBattler::clearRequestedAnimationsAtTarget() { requestedAnimationsAtTarget.clear(); }
void ObjectBattler::clearRequestedMotion() { requestMotion(SpriteBattler::MotionType::NoneMotion); }
void ObjectBattler::clearRequestedEffect() { requestEffect(SpriteBattler::EffectType::NoneEffect); }
void ObjectBattler::clearRequestedDamage() { damageRate = -1; }
void ObjectBattler::clearRequestedPrompt() { damagePrompt = Damage(); }
void ObjectBattler::clearRequestedToTarget() { requestToTarget = 0; }

Skill* ObjectBattler::skillRequested() { return usingSkill; }
vector<AnimationRequestData*> ObjectBattler::animationsRequested() { return requestedAnimations; }
vector<AnimationRequestData*> ObjectBattler::animationsAtTargetRequested() { return requestedAnimationsAtTarget; }
SpriteBattler::MotionType ObjectBattler::motionRequested() { return requestedMotion; }
int ObjectBattler::motionDurationRequested() { return motionDuration; }
SpriteBattler::EffectType ObjectBattler::effectRequested() { return requestedEffect; }
vector<int> ObjectBattler::effectParamsRequested() { return effectParams; }
int ObjectBattler::damageRequested() { return damageRate; }
Damage ObjectBattler::promptRequested() { return damagePrompt; }
int ObjectBattler::requestedToTarget() { return requestToTarget; }

bool ObjectBattler::isFreeze() { return freezing; }
bool ObjectBattler::isMoving() { return !freezing && (tx != x || ty != y); }
bool ObjectBattler::isJumping() { return jumpDuration>0; }
bool ObjectBattler::isDefending() { return defending; }
bool ObjectBattler::isAttacking() { return attacking; }
bool ObjectBattler::isWaiting() { return waitDuration>0; }
bool ObjectBattler::isDelaying() { return delayDuration>0; }
bool ObjectBattler::isOpering() { 
	return operDuration>0 && !opers.empty(); 
}
bool ObjectBattler::isHidden() { return hidden; }
bool ObjectBattler::isBusying() { 
	return isAttacking() || isDefending() || isWaiting() || isOpering(); 
}

int ObjectBattler::screenX() { return x; }
int ObjectBattler::screenY() { return y-z; }
int ObjectBattler::screenZ() { return z; }

void ObjectBattler::update() {
	ObjectBattlerBase::update();
	if (updateWait()) return;
	updateDelay();
	updateOper();
	updateMove();
	updateJump();
	updateZ();
}
bool ObjectBattler::updateWait() {
	if (waitDuration > 0) {
		waitDuration--; return true;
	}
	return false;
}
void ObjectBattler::updateDelay() {
	if (delayDuration > 0) 
		delayDuration--;
}
void ObjectBattler::updateOper() {
	//if (opers.empty() && operDuration > 0)
	//	clearRequestedOpers();
	while (!opers.empty() && opers.begin()->timing <= operDuration) {
		processOper(*opers.begin());
		opers.erase(opers.begin());
	}
	if(!opers.empty()) operDuration++;
	//else wait(5);
}
void ObjectBattler::updateMove() {
	if (tx != x || ty != y) {
		double dx = tx - x, dy = ty - y;
		double dist = sqrt(dx*dx + dy*dy);
		double spd = min(double(getSpeed()), dist);
		double rad;

		if (dx == 0) rad = (dy > 0 ? PI / 2 : -PI / 2);
		else if (dy == 0) rad = (dx > 0 ? 0 : PI);
		else rad = atan(dy / dx);
		if (dy != 0 && dx < 0) rad += PI;

		dx = spd*cos(rad);
		dy = spd*sin(rad);
		x += dx; y += dy;
	}
}
void ObjectBattler::updateZ() {
	if (zDuration > 0) { z += zSpeed; zDuration--;}
}
void ObjectBattler::updateJump() {
	if (jumpDuration >= 0) {
		int d = jumpDuration--;
		z = jumpFactorA*d*d + jumpFactorB*d;
		if (jumpDuration <= 0) {
			freezing = false; z = 0;
		}
	}
}
void ObjectBattler::processOper(OperatorData oper) {
	vector<int> params = oper.params;
	switch (oper.oType) {
	case OperatorData::OperatorType::Freeze: setFreeze(true); break;
	case OperatorData::OperatorType::Unfreeze: setFreeze(false); break;
	case OperatorData::OperatorType::FixedDir: setFixedDirection(true); break;
	case OperatorData::OperatorType::AutoDir: setFixedDirection(false); break;
	case OperatorData::OperatorType::ChangeDir: setDirection(params[0]); break;
	case OperatorData::OperatorType::SetVisible: setVisible(params[0]); break;

	case OperatorData::OperatorType::ForceMove: forceMovable = params[0]; break;

	case OperatorData::OperatorType::Move: move(params[0], params[1]); break;
	case OperatorData::OperatorType::MoveTo: moveTo(params[0], params[1]); break;
	case OperatorData::OperatorType::MoveForward: moveToward(getForwardDirection()); break;
	case OperatorData::OperatorType::MoveBackward: moveToward(getReverseDirection()); break;
	case OperatorData::OperatorType::MoveLeftward: moveToward(getLeftDirection()); break;
	case OperatorData::OperatorType::MoveRightward: moveToward(getRightDirection()); break;
	case OperatorData::OperatorType::MoveToTarget: requestMoveToTarget(); break;

	case OperatorData::OperatorType::Transfer: transfer(params[0], params[1]); break;
	case OperatorData::OperatorType::TransferTo: transferTo(params[0], params[1]); break;
	case OperatorData::OperatorType::TransferForward: transferToward(getForwardDirection()); break;
	case OperatorData::OperatorType::TransferBackward: transferToward(getReverseDirection()); break;
	case OperatorData::OperatorType::TransferLeftward: transferToward(getLeftDirection()); break;
	case OperatorData::OperatorType::TransferRightward: transferToward(getRightDirection()); break;
	case OperatorData::OperatorType::TransferToTarget: requestTransferToTarget(); break;

	case OperatorData::OperatorType::Jump: jump(params[0], params[1]); break;
	case OperatorData::OperatorType::ClearJump: clearJump(); break;
	case OperatorData::OperatorType::ChangeZ: changeZ(params[0], params[1]); break;

	case OperatorData::OperatorType::ChangeSpeed: setSpeed(params[0]); break;
	case OperatorData::OperatorType::ResetSpeed: resetSpeed(); break;

	case OperatorData::OperatorType::Shake: requestEffect(SpriteBattler::EffectType::Shake, params); break;
	case OperatorData::OperatorType::ScreenShake: requestEffect(SpriteBattler::EffectType::ScreenShake, params); break;
	case OperatorData::OperatorType::Shine: requestEffect(SpriteBattler::EffectType::Shine, params); break;
	case OperatorData::OperatorType::ScreenShine: requestEffect(SpriteBattler::EffectType::ScreenShine, params); break;
	case OperatorData::OperatorType::SubSprite: requestEffect(SpriteBattler::EffectType::SubSprite, params); break;

	case OperatorData::OperatorType::PlayAnimation:
		switch (params.size()) {
		case 1:requestAnimation(&animations[params[0]]); break;
		case 2:requestAnimation(&animations[params[0]], 0, 0, params[1]); break;
		case 3:requestAnimation(&animations[params[0]], params[1], params[2]); break;
		case 4:requestAnimation(&animations[params[0]], params[1], params[2], params[3]); break;
		}
		break;
	case OperatorData::OperatorType::PlayAnimationAtTarget:
		switch (params.size()) {
		case 1:requestAnimationAtTarget(&animations[params[0]]); break;
		case 2:requestAnimationAtTarget(&animations[params[0]], 0, 0, params[1]); break;
		case 3:requestAnimationAtTarget(&animations[params[0]], params[1], params[2]); break;
		case 4:requestAnimationAtTarget(&animations[params[0]], params[1], params[2], params[3]); break;
		}
		break; 
	case OperatorData::OperatorType::ChangeMotion: requestMotion(SpriteBattler::MotionType(params[0]), params[1]); break;
	case OperatorData::OperatorType::Wait: wait(params[0]); break;
	case OperatorData::OperatorType::Delay: delay(params[0]); break;
	case OperatorData::OperatorType::Damage: requestDamage(params[0]); break;
	}
}


ObjectActor::ObjectActor() {}
ObjectActor::ObjectActor(int id) {
	this->ObjectActor::ObjectActor(&actors[id]);
}
ObjectActor::ObjectActor(Actor* actor): ObjectBattler(actor) {
	this->actor = actor; setup();
}
bool ObjectActor::isSkillUsable(Skill* item) {
	if (!ObjectBattlerBase::isSkillUsable(item)) return false;
	if (item->weaponRequirements.size() > 0){
		Weapon* wea = getWeapon();
		if (wea != NULL) {
			for (int i = 0; i < item->weaponRequirements.size(); i++) {
				System::WeaponType req = item->weaponRequirements[i];
				if (wea->wType == req) return true;
			}
		}
		return false;
	}
	return true;
}

void ObjectActor::setup() {
	initEquips(); setupParams();	
	bigFace = actor->bigFace;
	hp = mhp(); mp = mmp();
	refresh();
}
void ObjectActor::setupParams() {
	for (int i = 0; i < System::BaseParamCount; i++)
		paramsGrowth[i] = actor->paramsGrowth[i];
}
void ObjectActor::initEquips() {
	equips = actor->initEquips;
}
void ObjectActor::refreshSkills() {
	for (int i = 0; i < battler->learnedSkills.size(); i++) {
		Skill* skill = battler->learnedSkills[i];
		if (!isSkillLearned(skill) && level >= skill->learnedLevel) {
			GameManager::newSkill = true; learnSkill(skill);
		}
	}
}

int ObjectActor::getEquipSlotIndex(Equip* equip) {
	if (equip == NULL) return -1;
	for (int i = 0; i < equips.size(); i++)
		if (equips[i].eType == equip->eType) return i;
}
void ObjectActor::equip(Equip* equip) {
	if (equip == NULL) return;
	int sid = getEquipSlotIndex(equip);
	equips[sid].equip = equip;
	refresh();
}
void ObjectActor::equipWithItem(Equip* equip) {
	if (equip == NULL) return;
	int sid = getEquipSlotIndex(equip);
	if (equips[sid].equip != NULL){}
		unequipToItem(sid);
	GameManager::loseItem(equip, 1);
	equips[sid].equip = equip;
	refresh();
}

void ObjectActor::unequip(Equip* equip) {
	if (equip == NULL) return;
	for (int i = 0; i < equips.size(); i++)
		if (equips[i].equip == equip) {
			unequip(i);break;
		}
}
void ObjectActor::unequip(int id) {
	equips[id].equip = NULL;
	refresh();
}
void ObjectActor::unequipToItem(Equip* equip) {
	if (equip == NULL) return;
	unequip(equip); GameManager::gainItem(equip, 1, false);
}
void ObjectActor::unequipToItem(int id) {
	if (equips[id].equip == NULL) return;
	GameManager::gainItem(equips[id].equip, 1, false);
	unequip(id); 
}

string ObjectActor::getBigFaceName() { return bigFace; }

Weapon* ObjectActor::getWeapon() {
	for (int i = 0; i < equips.size(); i++) {
		if (equips[i].eType == System::EquipType::Weapon &&
			equips[i].equip != NULL)
			return static_cast<Weapon*>(equips[i].equip);
	}
	return NULL;
}
vector<Equip*> ObjectActor::getEquips() {
	vector<Equip*> res;
	for (int i = 0; i < equips.size(); i++) {
		res.push_back(equips[i].equip);
	}
	return res;
}
vector<Equip*> ObjectActor::getEquipsWithoutNULL() {
	vector<Equip*> res;
	for (int i = 0; i < equips.size(); i++) {
		if(equips[i].equip!=NULL) res.push_back(equips[i].equip);
	}
	return res;
}
int ObjectActor::equipParam(int id) {
	int sum = 0;
	vector<Equip*> equips = getEquipsWithoutNULL();

	for (int i = 0; i < equips.size(); i++)
		sum += equips[i]->params[id];
	return sum;
}

double ObjectActor::equipSpecialParam(int id) {
	int sum = 0;
	vector<Equip*> equips = getEquipsWithoutNULL();

	for (int i = 0; i < equips.size(); i++)
		sum += equips[i]->specialParams[id];
	return sum;
}
int ObjectActor::levelParam(int id) {
	return params[id]*(pow(1+paramsGrowth[id],(level-1)/FactorA)-1);
}
int ObjectActor::plusParam(int id) { return ObjectBattler::plusParam(id) + levelParam(id) + equipParam(id); }
double ObjectActor::plusSpecialParam(int id) { 
	return ObjectBattler::plusSpecialParam(id) + equipSpecialParam(id); 
}

bool ObjectActor::isActor() { return true; }

void ObjectActor::update() {
	ObjectBattler::update();
	updateControl();
}
void ObjectActor::updateControl() {
	if (isDead() || isDelaying()) return;
	int dir = InputManager::dir8();
	if (dir >= 0) 
		moveToward(dir);
	if (InputManager::isKeyPress(InputManager::KeyMap::Jump) && !isJumping()) jump();
	if (InputManager::isKeyPress(InputManager::KeyMap::Defend)) { if (!defending) setDefend(); }
	else if (!updateSkillControl() && isBusying()) setIdle();
}
bool ObjectActor::updateSkillControl() {
	bool skillFlag = false; // 是否按键
	if (InputManager::isKeyPress(InputManager::KeyMap::Attack)) {
		skillFlag = true;
		if (!isAttacking()) {
			if (getWeapon() == NULL) requestSkill(NormalAttackSkillIdWithoutWeapon);
			else {
				if (isJumping() && z >= 15) 
					useSkill(JumpAttackSkillId);
				else
					useSkill(NormalAttackSkillId);
			}
		}
	}else {
		for (int i = 0; i < GameManager::SkillSlotCount; i++) {
			Skill* skill = GameManager::getSkillSlot(i);
			if (InputManager::isKeyPress(GameManager::skillSlotKeys[i])) {
				useSkill(skill); skillFlag = true; break;
			}
		}
	}
	if (!skillFlag)
		for (int i = 0; i < GameManager::ItemSlotCount; i++) {
			Item* item = GameManager::getItemSlot(i);
			if (InputManager::isKeyPress(GameManager::itemSlotKeys[i])) {
				useItem(item); skillFlag = true; break;
			}
		}
	return skillFlag;
}
string ObjectActor::makeSaveData() {
	string data = ObjectBattlerBase::makeSaveData();
	// 不能有換行！
	char data_c[50];
	sprintf_s(data_c, "%d ", equips.size()); data += data_c;

	for (int i = 0; i < equips.size(); i++) {
		Equip* equip = equips[i].equip;
		if (equip == NULL) sprintf_s(data_c, "-1 -1 ");
		else sprintf_s(data_c, "%d %d ", equip->type, equip->index); 
		data += data_c;
	}
	return data;
}
void ObjectActor::loadSaveData(string data) {
	// 不能有換行！
	// 不能有換行！
	char data_c[50]; int l = 0;
	int index, vsize, type;
	vector<string> dataV = split(data, " ");
	sscanf_s(dataV[l++].c_str(), "%d", &index);
	this->ObjectActor::ObjectActor(&actors[index]);

	sscanf_s(dataV[l++].c_str(), "%d", &level);
	sscanf_s(dataV[l++].c_str(), "%d", &hp);
	sscanf_s(dataV[l++].c_str(), "%d", &mp);
	sscanf_s(dataV[l++].c_str(), "%d", &vsize);
	for (int i = 0; i < vsize; i++) {
		sscanf_s(dataV[l++].c_str(), "%d", &index);
		learnSkill(index);
	}
	for (int i = 0; i < System::BaseParamCount; i++)
		sscanf_s(dataV[l++].c_str(), "%d", &buffs[i]);
	for (int i = 0; i < System::SpecialParamCount; i++)
		sscanf_s(dataV[l++].c_str(), "%d", &specialBuffs[i]);

	sscanf_s(dataV[l++].c_str(), "%d", &vsize);
	for (int i = 0; i < vsize; i++) {
		sscanf_s(dataV[l++].c_str(), "%d", &type);
		sscanf_s(dataV[l++].c_str(), "%d", &index);
		if (type != -1 && index != -1) {
			Equip* eq;
			if (type == 2) eq = &weapons[index];
			if (type == 3) eq = &armors[index]; 
			equips[i].equip = eq;
		}else equips[i].equip = NULL;
	}
}

ObjectEnemy::ObjectEnemy() {this->difficulty = 1;}
ObjectEnemy::ObjectEnemy(int id, int difficulty) {
	this->ObjectEnemy::ObjectEnemy(&enemies[id], difficulty);
}
ObjectEnemy::ObjectEnemy(Enemy* enemy, int difficulty) : ObjectBattler(enemy) {
	this->enemy = enemy; this->difficulty = difficulty; setup(); resetSpeed();
}
bool ObjectEnemy::isEnemy() { return true; }

void ObjectEnemy::resetSpeed() {
	setSpeed(WalkSpeed);
}
void ObjectEnemy::refreshSkills() {
	sumRate = enemy->normalAttackRate;
	for (int i = 0; i < battler->learnedSkills.size(); i++) {
		learnSkill(battler->learnedSkills[i]);
		skillRate.push_back(enemy->skillRate[i]);
		sumRate += enemy->skillRate[i];
	}
}
Skill* ObjectEnemy::getRandomSkill() {
	int r = rand() % sumRate;
	int size = learnedSkills.size();
	for (int i = 0; i <= size; i++) {
		r -= (i == size ? enemy->normalAttackRate : skillRate[i]);
		if (r < 0) {
			if (i < size) return learnedSkills[i];
			else if (isJumping() && z >= 15)
				return &skills[JumpAttackSkillId];
			else
				return &skills[NormalAttackSkillId];
		}
	}
	return NULL;
}
void ObjectEnemy::useSkillInBattle(Skill* skill) {
	ObjectBattler::useSkillInBattle(skill);
	delay(max(EnemyAttackDelay, skill->delay));
}
vector<pair<Skill*, int>> ObjectEnemy::getUsableSkills() {
	vector<pair<Skill*,int>> res;
	for (int i = 0; i < learnedSkills.size(); i++) {
		Skill* skill = learnedSkills[i];
		if (isSkillUsable(skill)) res.push_back(make_pair(skill,skillRate[i]));
	}
	return res;
}
int ObjectEnemy::baseParam(int id) { 
	int level = GameManager::getLeader()->getLevel();
	return ObjectBattlerBase::baseParam(id)*(difficulty==4 ? 
		max(difficulty,level*level/200): difficulty);
}
int ObjectEnemy::getLevel() {
	return enemy->level<<(difficulty-1);
}
int ObjectEnemy::getGold() {
	int alv = GameManager::getLeader()->getLevel();
	int elv = getLevel();
	double rate = 1 + min(10, max(elv - alv, -5)) / 10.0;
	return enemy->gold * rate;
}
Enemy* ObjectEnemy::getEnemy() { return enemy; }

void ObjectEnemy::updateMove() {
	if (tx != x) direction = tx > x;
	ObjectBattler::updateMove();
}
void ObjectEnemy::updateRandomMove() {
	if (isMoving()) return;
	int tx = x + rand() % 11 - 5;
	int ty = y + rand() % 11 - 5;
	moveTo(tx, ty);
}
void ObjectEnemy::update() {
	ObjectBattler::update();
	if (isDead() && !dead) {
		GameManager::gainGold(getGold()); dead = true;
	}
}