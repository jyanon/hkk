#pragma once

#include<vector>

#include "Rect.h"
#include "DisplayObject.h"
using namespace std;
class DisplayObject;

class Stage {
public:
	Stage();
	Stage(int width, int height);
	Stage(int x,int y,int width, int height);
	void addChild(DisplayObject obj);
	void addChild(DisplayObject* obj);
	void removeChild(DisplayObject obj);
	void removeChild(DisplayObject* obj);

	virtual void render();
	virtual void render(char** data);
	virtual void quickRender(char** &data);

	void paint(int x, int y, char c);
	char getData(int x, int y);
	vector<vector<char> > getData();

protected:
	void createContents();
	void clearContents();

	int x, y, z;
	int width, height;
	vector<DisplayObject*> children;
	vector<vector<char> > data;
	vector<vector<char> > emptyData;
};