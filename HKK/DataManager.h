#pragma once
#include <vector>
#include "GameManager.h"
using namespace std;
vector<string> split(const string& str, const string& delim);
struct SaveFileData {
	bool empty = true;
	int leaderId;
	string leaderName;
	int level, gold;
	string time;
	string dataToString();
	void stringToData(string str);
};
static class DataManager {
public:
	static const int MaxSaveFileCount = 4;
	static const string SaveFileDataPath;
	static const char* GameSaveFormat;
	static void saveGame(int id);
	static void loadGame(int id);
	static void deleteGame(int id);

	static void saveSaveFiles();
	static void loadSaveFiles();

	static SaveFileData* getFileData(int id);
private:
	static string getSaveGameData();
	static string getSaveGameFileData();

	static void updateGameFile(int id);
	static void deleteGameFile(int id);

	static SaveFileData fileDatas[MaxSaveFileCount];
};