#pragma once

#include<vector>

#include"Stage.h"
#include"Sprite.h"
#include"SpriteBattler.h"
#include"SpriteMap.h"
#include"SpriteCommand.h"
#include"BattleUI.h"
#include"SpriteWindow.h"

using namespace std;

struct Actor;

class Stage;
class Scene : public Stage {
public:
	Scene();

	void shake(int power, int duration);
	void shine(char c, int duration);

	virtual void update();

	virtual void render();
	virtual void render(char** data);
	virtual void quickRender(char** &data);

	virtual void requestPopScene();
	virtual void requestNextScene(Scene* scene);
	virtual bool isRequestPopScene();
	virtual Scene* getRequestNextScene();
protected:
	void initialiazeEffect();

	void updateChildren();
	void updatePopScene();
	virtual void updateOthers();
	virtual void updateEffect();
	void updateShine();
	void updateShake();
	virtual void createSprites();

	char shineChar;
	int shakePower;
	int shineDuration;
	int shakeDuration;

	bool requestPop;
	Scene* requestScene;
};
/*
class SceneTest : public Scene {
public:
	SceneTest();
private:
	int cnt;
	SpriteExtend *test1;
	Sprite *test2, *test3;
	Sprite *background1, *background2;
	Animation *testAni1, *testAni2, *testAni3;
	SpriteActor *actor1;
protected:
	void updateOthers();
	void createSprites();

	void createTestSprite();
};
*/
class SceneTitle : public Scene {
public:
	SceneTitle();
private:
	bool okEnable();
	void processOK(int index);

	int cnt;
	Sprite *title1, *title2;
	Sprite *background1, *background2, *background3;

	WindowFile* loadWindow;

	SpriteCommandGroup* commandGroup;
protected:
	void updateOthers();
	void updateSprites();
	void updateControl();

	void createSprites();

	void createBackgroundSprites();
	void createCommandSprites();
	void createLoadWindow();
};

class SceneStart : public Scene {
public:
	SceneStart();

	void setupGame();

	void select(int id);
	void setActor(int id);
	void setActor(Actor* actor);
	Actor* getActor();

	void drawParams(Actor* actor);

	int getCount();

	void refresh();
private:
	void drawActorParams();
	void scrollLeft();
	void scrollRight();

	int index, keyCd;

	Sprite *foreground,*background;
	Sprite* actorBigFace;
	Sprite *leftArrow, *rightArrow;
	Sprite* paramsDisplay;

	TextDisplay* textDisplay;

	Actor* actor;
protected:
	void updateOthers();
	void updateSprites();
	void updateControl();

	void createSprites();

	void createGroundSprites();
	void createArrowsSprites();
	void createBigFaceSprites();
	void createParamsSprites();
};

class SceneMenu :public Scene {
public:
	SceneMenu();

	bool anyWindowActive();
	SpriteWindow* getActiveWindow();

private:
	bool okEnable();
	void processOK(int index);
	//void processBack();

	Sprite* actorBigFace;
	Sprite* background;

	Sprite* newEquip;
	Sprite* newSkill;
	Sprite* newer;

	SpriteCommandGroup* commandGroup;

	WindowMap* mapWindow;
	WindowItem* itemWindow;
	WindowSkill* skillWindow;
	WindowShop* shopWindow;
	WindowFile* saveWindow;
protected:
	void createSprites();

	void createBackgroundSprites();
	void createBigFaceSprites();
	void createCommandSprites();
	void createWindowsSprites();
	void createNewPromptsSprites();

	void createMapWindow();
	void createItemWindow();
	void createSkillWindow();
	void createShopWindow();
	void createSaveWindow();

protected:
	void updateOthers();
	void updateSprites();
	void updateControl();

};

class SceneBattle : public Scene {
public:
	SceneBattle(int id, int level);

	void addEnemy(ObjectEnemy* enemy);
private:
	void battleEnd();

	int cnt, keyCd;
	bool pause;

	Sprite* battleResult;
	Sprite* battlePause;
	WindowBattleResult* battleResultWindow;

	SpriteMap *battleField;
	BattleState *battleState;
	BattleSlots *battleSlots;

	SpriteActor *actorSprite;
	ObjectActor *actorObject;
	vector<SpriteEnemy*> enemySprites;
	vector<ObjectEnemy*> enemyObjects;
	Battle* battle;

protected:
	void update();

	void updateOthers();
	void updateBattleProcess();
	void updateBattleResult();
	void updateBattleResultAni();
	void updateBattlePause();

	void createSprites();

	void createBattleResult();
	void createBattlePause();

	void createBattleField();
	void createBattlers();
	void createBattleUI();
	void createActor();
	void createEnemies();

	void setupBattleResult(BattleManager::BattleResult result);
};