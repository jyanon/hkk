#pragma once

#include<vector>
#include<fstream>
#include<map>

#include "Rect.h"
using namespace std;

class Bitmap {
public:
	static Bitmap* getBitmap(string str);
	static Bitmap* getIcon(int id);
	static Bitmap getIconWithoutCache(int id);
	static Bitmap* getIconWithoutCache(int id, int width, int height);
	static void clearCache();

	Bitmap();
	Bitmap(string fileName);
	Bitmap(int width, int height);

	int getWidth();
	int getHeight();
	char** getData();
	char getData(int x,int y);

	void drawRect(int x, int y, int width, int height, char c);
	void drawRect(Rect rect, char c);
	void clearRect(int x, int y, int width, int height);
	void clearRect(Rect rect);
	void paint(int x, int y, char c);

	Bitmap clip(int x, int y, int width, int height);

	void copyTo(Bitmap* bitmap, int x, int y, int width, int height, int tx = 0, int ty = 0, bool ignore = true);
	void copyFrom(Bitmap* bitmap, int x, int y, int width, int height, int tx = 0, int ty = 0, bool ignore = true);

private:
	void readFromFile(string fileName);
	void createEmpty(int width, int height);

	string url;
	int width, height;
	char** data;

	static map<string, Bitmap> bitmapCache;
	static map<int, Bitmap> iconCache;
	static vector<Bitmap> otherBitmaps;
};
