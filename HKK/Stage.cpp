
#include "stdafx.h"

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<vector>
#include<algorithm>

#include<Windows.h>

#include "Stage.h"
#include "DisplayObject.h"
#include "ScreenManager.h"


Stage::Stage() {
	this->Stage::Stage(0, 0, ScreenManager::Width, ScreenManager::Height);
}
Stage::Stage(int width, int height) {
	this->Stage::Stage(0, 0, width, height);
}
Stage::Stage(int x, int y, int width, int height) {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	createContents();
}

void Stage::createContents() {
	data.clear();
	vector<char> line;
	for (int x = 0; x < width; x++)
		line.push_back(' ');
	for (int y = 0; y < height; y++)
		data.push_back(line);
	emptyData = data;
}

void Stage::clearContents() {
	data = emptyData;/*
	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++)
			data[y][x] = ' ';*/
}

void Stage::addChild(DisplayObject obj) {addChild(&obj);}
void Stage::addChild(DisplayObject* obj) {
	children.push_back(obj);
	sort(children.begin(), children.end(), DisplayObjectComp);
}

void Stage::removeChild(DisplayObject obj) {removeChild(&obj);}
void Stage::removeChild(DisplayObject* obj) {
	vector<DisplayObject*>::iterator pos;
	pos = find(children.begin(), children.end(), obj);
	if (pos != children.end()) children.erase(pos);
}
void Stage::render() {
	clearContents();
	for (int i = 0; i < children.size(); i++) {
		DisplayObject* obj = children[i];
		obj->render(this, x+obj->x, y+obj->y);
	}
}
void Stage::render(char** data) {
	for (int i = 0; i < children.size(); i++) {
		DisplayObject* obj = children[i];
		obj->render(data, x+obj->x, y+obj->y);
	}
}
void Stage::quickRender(char** &data) {
	for (int i = 0; i < children.size(); i++) {
		DisplayObject* obj = children[i];
		obj->quickRender(data, x+obj->x, y+obj->y);
	}
}
void Stage::paint(int x, int y, char c) {
	if (x >= 0 && x<width && y >= 0 && y<height)
		data[y][x] = c;
}
char Stage::getData(int x, int y) {
	if (x >= 0 && x < width && y >= 0 && y < height)
		return data[y][x];
	else
		return ' ';
}
vector<vector<char> > Stage::getData() {return data;}