#pragma once

#include <windows.h> 
#include <conio.h> 
#define KEY_DOWN(VK_NONAME) ((GetAsyncKeyState(VK_NONAME) & 0x8000) ? 1:0) //必要的，我是背下来的 

static class InputManager {
public:
	static const int StandardKeyCoolDown = 5;
	static bool isAnyKeyPress();
	static char getInputKey();
	static char getInputKeyInUppercase();
	static bool isKeyPress(char key);
	static bool isKeyPressWithCD(char key);

	static int dir8();
	static int UD();
	static int LR();

	static void update();

	static enum KeyMap {
		Attack = 'J',
		Jump = 'K',
		Defend = 'L',
		Pause = 'P',
		PauseVisable = 'V',
		Skill1 = 'U',
		Skill2 = 'I',
		Skill3 = 'O',
		Skill4 = 'Q',
		Skill5 = 'E',
		Item1 = '1',
		Item2 = '2',
		Item3 = '3',
		Switch = 'E',
		Help = 'H',
		Up = 'W',
		Left = 'A',
		Down = 'S',
		Right = 'D',
		Load = 'L',
		Delete = 'T',
		OK = 13,
		Back = 27,
		Cancel = 27,
	};
private:
	static int keyCd;
	static char hit;
};