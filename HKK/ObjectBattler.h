#pragma once
#include<map>
#include"SpriteBattler.h"
#include"GameManager.h"
				   // 0  1  2  3  4  5  6  7
const int dirX[8] = { 1, 1, 0,-1,-1,-1, 0, 1 };
const int dirY[8] = { 0, 1, 1, 1, 0,-1,-1,-1 };

struct UsableItem;
struct AnimationRequestData {
	AnimationData* ani;
	bool atGround = false;
	int ox = 0, oy = 0;
	AnimationRequestData(AnimationData* ani, int ox, int oy, bool atGround);
};

class ObjectBattlerBase {
public:
	ObjectBattlerBase();
	ObjectBattlerBase(Battler* battler);

	virtual bool isActor();
	virtual bool isEnemy();

	virtual int baseParam(int id);
	virtual int plusParam(int id);
	virtual int param(int id);

	virtual double baseSpecialParam(int id);
	virtual double plusSpecialParam(int id);
	virtual double specialParam(int id);

	virtual void registerCoolDownList(UsableItem* item);
	virtual void clearCoolDownList();
	virtual int getItemCoolDown(UsableItem* item);
	virtual double getItemCoolDownRate(UsableItem* item);

	virtual bool isUsableItemUsable(UsableItem* item);
	virtual bool isItemUsable(Item* item);
	virtual bool isSkillUsable(Skill* item);

	virtual bool useSkill(Skill* skill);
	virtual bool useSkill(int id);
	virtual bool useItem(Item* item);
	virtual bool useItem(int id);

	double hitRate();
	double missRate();
	double critical();

	bool isDead();

	virtual void dealDamage(Damage damage);

	void addHp(int hp);
	void addMp(int mp);
	void setHp(int hp);
	void setMp(int mp);

	int getHp() { return hp; }
	int getMp() { return mp; }

	double getHpRate() { return hp*1.0 / mhp(); }
	double getMpRate() { return mp*1.0 / mmp(); }

	void recoverAll();

	int getLevel();
	void levelUp(); void levelDown();
	void setLevel(int lv);

	vector<Skill*> getSkills();
	bool isSkillLearned(int id);
	bool isSkillLearned(Skill* skill);

	void learnSkill(int id);
	void learnSkill(Skill* skill);
	void forgetSkill(int id);
	void forgetSkill(Skill* skill);

	int mhp() { return param(0); }
	int mmp() { return param(1); }

	int atk() { return param(2); }
	int def() { return param(3); }

	int mat() { return param(4); }
	int mdf() { return param(5); }

	int agi() { return param(6); }
	int luk() { return param(7); }

	virtual void update();
	virtual void refresh();
	string getFileName();
	string getName();

	virtual string makeSaveData();
	virtual void loadSaveData(string data);
protected:
	virtual void setup();
	virtual void setupSkills();
	virtual void setupParams();

	virtual void refreshSkills();

	virtual void useSkillInBattle(Skill* skill);
	virtual void useSkillInMenu(Skill* skill);

	virtual void useItemInBattle(Item* item);
	virtual void useItemInMenu(Item* item);

	virtual void updateCoolDown();

	Battler* battler;
	string name, fileName;
	int hp, mp;
	int level;
	int params[System::BaseParamCount];
	double specialParams[System::SpecialParamCount];
	double buffs[System::BaseParamCount], specialBuffs[System::SpecialParamCount];

	map<UsableItem*, int> coolDownList;
	vector<Skill*> learnedSkills;
};

class ObjectBattler : public ObjectBattlerBase {
public:
	static const int JumpHeight;
	static const int JumpLast;
	static const int WalkSpeed;

	static const int NormalAttackSkillIdWithoutWeapon;
	static const int NormalAttackSkillId;
	static const int JumpAttackSkillId;

	ObjectBattler();
	ObjectBattler(Battler* battler);

	virtual void update();

	void wait(int duration);
	void delay(int duration);

	void clearTargetPosition(); 
	void clearBusyFlags();

	void transfer(int x, int y);
	void transferTo(int x, int y);
	void transferToward(int dir);
	void move(int x, int y);
	void moveTo(int x, int y);
	void moveToward(int dir);

	void faceTo(ObjectBattler* battler);
	void faceTo(int x);

	void jump(int height, int duration);
	void clearJump();

	void changeZ(int targetZ, int duration);

	int screenX();
	int screenY();
	int screenZ();

	int getX() { return x; }
	int getY() { return y; }
	int getZ() { return z; }

	void setSpeed(int spd);
	int getSpeed();

	virtual void resetSpeed();

	void setFixedDirection(bool fixed);
	bool isFixedDirection();

	void setDirection(bool dir);
	bool getDirection();

	int getForwardDirection();
	int getReverseDirection();
	int getLeftDirection();
	int getRightDirection();
	int getReverseDirection(int dir);
	int getLeftDirection(int dir);
	int getRightDirection(int dir);

	void setIdle();
	void setDefend();
	void setAttack();

	void setVisible(bool vis);
	void setFreeze(bool fre);

	virtual bool isUsableItemUsable(UsableItem* item);

	Skill* getOperingSkill();

	virtual void dealDamage(Damage damage, bool show = true);

	void requestAny();

	void requestSkill(Skill* skill);
	void requestSkill(int id);
	void requestOpers(vector<OperatorData> opers);
	void requestOpers(vector<OperatorData> opers,Skill* skill);
	void requestAnimation(int id);
	void requestAnimation(AnimationRequestData* data);
	void requestAnimation(AnimationData* ani, int ox = 0, int oy = 0, bool atGround = false);
	void requestAnimationAtTarget(AnimationData* ani, int ox = 0, int oy = 0, bool atGround = false);
	void requestMotion(SpriteBattler::MotionType mot,int duration);
	void requestEffect(SpriteBattler::EffectType eff, vector<int> params);
	void requestDamage(int rate);
	void requestPrompt(Damage damage);
	void requestMoveToTarget();
	void requestTransferToTarget();

	void clearRequestAny();

	void clearRequests();
	void clearRequestedSkill();
	void clearRequestedOpers();
	void clearRequestedAnimations();
	void clearRequestedAnimationsAtTarget();
	void clearRequestedMotion();
	void clearRequestedEffect();
	void clearRequestedDamage();
	void clearRequestedPrompt();
	void clearRequestedToTarget();

	bool isAnyRequested();

	Skill* skillRequested();
	SpriteBattler::MotionType motionRequested();
	int motionDurationRequested();
	vector<AnimationRequestData*> animationsRequested();
	vector<AnimationRequestData*> animationsAtTargetRequested();
	SpriteBattler::EffectType effectRequested();
	vector<int> effectParamsRequested();
	int damageRequested();
	Damage promptRequested();
	int requestedToTarget();

	bool isFreeze();
	bool isMoving();
	bool isJumping();
	bool isDefending();
	bool isAttacking();
	bool isOpering();
	bool isWaiting();
	bool isHidden();
	bool isDelaying();

	bool isBusying();
private:
	vector<OperatorData> makeOpersForItem(Item* item);
	void calcJumpFactorA(double h);
	void calcJumpFactorB();

	double jumpFactorA, jumpFactorB;
protected:
	virtual void setup();
	virtual void setupPosition();
	virtual void setupState();

	virtual void useSkillInBattle(Skill* skill);

	virtual void useItemInBattle(Item* item);

	bool updateWait();
	void updateDelay();
	void updateOper();
	virtual void updateMove();
	void updateZ();
	void updateJump();

	void processOper(OperatorData oper);

	bool forceMovable;

	int x, y; double z;
	int tx, ty; double tz; // target position
	int speed; double zSpeed;
	int zDuration;
	int waitDuration;
	int delayDuration;
	int jumpDuration;
	int operDuration;

	bool direction, fixedDir; // false: ������true: ������
	bool freezing, defending, attacking;
	bool hidden;

	Skill* usingSkill;

	vector<OperatorData> opers;
	Skill* operingSkill;
	vector<AnimationRequestData*> requestedAnimations;
	vector<AnimationRequestData*> requestedAnimationsAtTarget;
	SpriteBattler::MotionType requestedMotion;
	int motionDuration;
	SpriteBattler::EffectType requestedEffect;
	vector<int> effectParams;
	Damage damagePrompt;

	int damageRate;

	int requestToTarget; // 0: none, 1: move, 2: transfer

	bool anyRequested;
};


class ObjectActor : public ObjectBattler {
public:
	static const int FactorA;
	ObjectActor();
	ObjectActor(int id);
	ObjectActor(Actor* actor);

	bool isActor();

	bool isSkillUsable(Skill* item);

	virtual void update();

	int plusParam(int id);
	double plusSpecialParam(int id);

	string getBigFaceName();

	Weapon* getWeapon();
	vector<Equip*> getEquips();
	vector<Equip*> getEquipsWithoutNULL();

	int getEquipSlotIndex(Equip* equip);
	void equip(Equip* equip);
	void equipWithItem(Equip* equip);
	void unequip(int id);
	void unequip(Equip* equip);
	void unequipToItem(int id);
	void unequipToItem(Equip* equip);

	virtual string makeSaveData();
	virtual void loadSaveData(string data);
private:
	virtual void setup();
	virtual void setupParams();

	virtual void refreshSkills();
	
	void updateControl();
	bool updateSkillControl();

	int levelParam(int id);
	int equipParam(int id);
	double equipSpecialParam(int id);

	void initEquips();

	double paramsGrowth[System::BaseParamCount];
	
	string bigFace;
	Actor* actor;
	vector<EquipSlot> equips;
};
class ObjectEnemy : public ObjectBattler{
public:
	static const int EnemyAttackDelay;
	static const int WalkSpeed;
	ObjectEnemy();
	ObjectEnemy(int id, int difficulty = 1);
	ObjectEnemy(Enemy* enemy, int difficulty = 1);

	void update();

	int baseParam(int id);
	int getLevel();
	int getGold();

	virtual void resetSpeed();

	Enemy* getEnemy();
	bool isEnemy();
	void updateRandomMove();
	Skill* getRandomSkill();
	vector<pair<Skill*, int>> getUsableSkills();
private:
	vector<int> skillRate;
	int sumRate;
	int difficulty;
	Enemy* enemy;

protected:
	virtual void refreshSkills();

	void useSkillInBattle(Skill* skill);

	void updateMove();

	bool dead = false;
};