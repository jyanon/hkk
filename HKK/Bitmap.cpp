
#include "stdafx.h"

#include"Bitmap.h"
#include"Database.h"
using namespace std;

map<string, Bitmap> Bitmap::bitmapCache;
map<int, Bitmap> Bitmap::iconCache; 
vector<Bitmap> Bitmap::otherBitmaps;

Bitmap* Bitmap::getBitmap(string str) {
	//return new Bitmap(str);
	if (bitmapCache.count(str) <= 0) bitmapCache[str] = Bitmap(str);
	return &bitmapCache[str];
}
Bitmap* Bitmap::getIcon(int id) {
	//return new Bitmap(str);
	if (iconCache.count(id) <= 0) iconCache[id] = Bitmap::getIconWithoutCache(id);
	return &iconCache[id];
}
Bitmap Bitmap::getIconWithoutCache(int id) {
	int iWidth = iconSet->getWidth();
	int iHeight = iconSet->getHeight();
	int xCount = System::IconSetXCount;
	int yCount = System::IconSetYCount;
	int width = iWidth / xCount;
	int height = iHeight / yCount;
	int x = (id % xCount)*width;
	int y = (id / xCount)*height;
	return iconSet->clip(x, y, width, height);
}
Bitmap* Bitmap::getIconWithoutCache(int id, int width, int height) {
	Bitmap res(width, height);
	int iWidth = iconSet->getWidth();
	int iHeight = iconSet->getHeight();
	int xCount = System::IconSetXCount;
	int yCount = System::IconSetYCount;
	int rWidth = iWidth / xCount;
	int rHeight = iHeight / yCount;
	int x = (id % xCount)*rWidth;
	int y = (id / xCount)*rHeight;
	int tx = (width - rWidth) / 2;
	int ty = (height - rHeight) / 2;
	//res.drawRect(tx, ty, rWidth, rHeight, ' ');
	res.copyFrom(iconSet, x, y, rWidth, rHeight, tx, ty);//,false);
	otherBitmaps.push_back(res);
	return &otherBitmaps[otherBitmaps.size()-1];
}

void Bitmap::clearCache() {
	map<string, Bitmap>::iterator bit;
	map<int, Bitmap>::iterator iit;
	for (bit = bitmapCache.begin(); bit != bitmapCache.end(); bit++) {
		delete &(*bit).second;
	}
	for (iit = iconCache.begin(); iit != iconCache.end(); iit++) {
		delete &(*iit).second;
	}
	for (int i = 0; i< otherBitmaps.size(); i++) {
		delete &otherBitmaps[i];
	}
	bitmapCache.clear();
	iconCache.clear();
	otherBitmaps.clear();
}

int Bitmap::getWidth() {
	return width;
}
int Bitmap::getHeight() { return height; }

Bitmap::Bitmap() {
	width = height = 0;
}
Bitmap::Bitmap(string fileName) {
	url = fileName;
	readFromFile(url);
}
Bitmap::Bitmap(int width, int height) {
	url = "custom";
	createEmpty(width, height);
}
void Bitmap::drawRect(int x, int y, int width, int height, char c) {
	drawRect(Rect(x, y, width, height), c);
}
void Bitmap::drawRect(Rect rect, char c) {
	for (int y = rect.y; y < rect.y + rect.height; y++)
		for (int x = rect.x; x < rect.x + rect.width; x++)
			data[y][x] = c;
}
void Bitmap::clearRect(int x, int y, int width, int height) {
	clearRect(Rect(x, y, width, height));
}
void Bitmap::clearRect(Rect rect) {
	for (int y = rect.y; y < rect.y + rect.height; y++)
		for (int x = rect.x; x < rect.x + rect.width; x++)
			data[y][x] = 'T';
}
void Bitmap::paint(int x, int y, char c) {
	if (x >= 0 && x < width && y >= 0 && y < height) 
		data[y][x] = c;
}

void Bitmap::readFromFile(string fileName) {
	string fileData = "";
	height = 0; width = 0;
	fstream file;
	file.open(fileName, ios::in); // 打开文件 逐字读取
	char c; 
	file >> noskipws;
	while (!file.eof()) {
		file >> c; 
		if (c == '\n') height++; 
		else { if(height==0) width++; fileData += c;}
	}
	data = new char*[++height];
	for (int i = 0, y = 0; y < height; y++) {
		data[y] = new char[width];
		for (int x = 0; x < width; x++) data[y][x] = fileData[i++];
	}
}
void Bitmap::createEmpty(int width, int height) {
	this->width = width;
	this->height = height;

	data = new char *[height];
	for (int y = 0; y < height; y++) {
		data[y] = new char[width];
		for (int x = 0; x < width; x++) data[y][x] = 'T';
	}
}

Bitmap Bitmap::clip(int x, int y, int width, int height) {
	Bitmap res(width, height);
	for (int dy = 0; dy < height; dy++)
		for (int dx = 0; dx < width; dx++)
			res.paint(dx, dy, data[y + dy][x + dx]);
	return res;
}
void Bitmap::copyTo(Bitmap* bitmap, int x, int y, int width, int height, int tx, int ty, bool ignore) {
	for (int dy = 0; dy < height; dy++)
		for (int dx = 0; dx < width; dx++)
			if(ignore || data[y + dy][x + dx]!='T') bitmap->paint(tx+dx, ty+dy, data[y + dy][x + dx]);
}
void Bitmap::copyFrom(Bitmap* bitmap, int x, int y, int width, int height, int tx, int ty, bool ignore) {
	bitmap->copyTo(this, x, y, width, height, tx, ty, ignore);
}

char** Bitmap::getData() {return data;}
char Bitmap::getData(int x,int y) {
	if (x >= 0 && x < width && y >= 0 && y < height) return data[y][x];
	else return 'T';
}